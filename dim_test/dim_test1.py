import logging
import sys
from taf.sw.nrdim import nrdim
# from loguru import logger
# logger.add("debug.log", 
#            format="{time:YYYY-MM-DD at HH:mm:ss} | {level} | {message}",
#            level="DEBUG",
#            rotation="500 MB")

# logger.add(sys.stderr, 
#            format="{time} {name} - {level}: {message}", 
#            filter="my_module",
#            level="INFO")
# logger.add(sys.stdout, format="{message}", level="DEBUG", filter="nrdim")
# logger.bind(module='nrdim')
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s",
    datefmt="%a, %d %b %Y %H:%M:%S",
    filename="debug.log",
    filemode="w",
)

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
console.setFormatter(formatter)
logging.getLogger("").addHandler(console)

default_url = "http://5gtables.eecloud.dynamic.nsn-net.net/"
taf_url = "http://nrdim2.5ghzsyve.hz.nsn-rdnet.net/"
NRDIM = nrdim()
NRDIM.setup(
    scf_file_path="SCF.xml",
    cell_distname="MRBTS-579/NRBTS-579/NRCELL-1",
    cellgroup_distname="MRBTS-579/NRBTS-579/NRCELLGRP-0",
    url=default_url,
)
tp = NRDIM.calculate_expected_throughput()

print(tp)
