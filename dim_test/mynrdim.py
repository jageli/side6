import logging
import re
from taf.sw.nrdim import nrdim
from taf.sw.nrdim.nrdim_calculator import NrdimCalculator
from taf.internal.aliasing import aliasing_scope


class MyNrdimCalculator(NrdimCalculator):
    def __init__(self, specified_nrdim_params, url):
        super().__init__(specified_nrdim_params, url)

    def calculate_throughput(self, **kwargs):
        result = self._get_calculated_result(**kwargs)
        result = str(result)
        return self._extract_throughput(result)

    def _extract_throughput(self, result):
        logging.debug("calculated throughput response={}".format(result))
        downlink_throughput = re.search(r"DL tput: ([\d.]*)Mbps", result).group(1)
        uplink_throughput = re.search(r"UL tput: ([\d.]*)Mbps", result).group(1)
        # code = re.search(r"Save the file (.*?)[<]br[>]", result).group(1)
        code = re.search(r"co=([\w\d=]+)", result).group(1)
        return {
            "downlink": float(downlink_throughput),
            "uplink": float(uplink_throughput),
            "code": code,
        }


class Mynrdim(nrdim):
    ROBOT_LIBRARY_SCOPE = "TEST SUITE"

    """
    .. code-block:: robotframework

    *** Settings ***
    Library    taf.sw.nrdim    WITH NAME    NRDIM


    *** Test Cases ***
    Test Case
        NRDIM.Setup    scf_file_path=SCF_11.800.xml
        ...            cell_distname=MRBTS-318/NRBTS-318/NRCELL-2
        ...            cellgroup_distname=MRBTS-318/NRBTS-318/NRCELLGRP-0
        ...            alias=Test

        NRDIM.Set Parameter    name=frameStructureType   value=${1}    calculator=Test
        NRDIM.Set Parameter    name=ulDlDataSlotRatio    value=${0}    calculator=Test
        NRDIM.Set Parameter    name=guardPeriodLength    value=${2}    calculator=Test

        ${calculated throughput}=    NRDIM.Calculate Expected Throughput    calculator=Test
        Log    ${calculated throughput}

    """

    lookup_handler, configured_nrdim_calculator = aliasing_scope(
        name=__name__, lookup_arg="calculator", lookup_type=MyNrdimCalculator
    )

    def setup(
        self,
        scf_file_path=None,
        cell_distname=None,
        cellgroup_distname=None,
        scf_mediation=None,
        alias=None,
        url=None,
        **nrdim_parameters
    ):
        """
        Setup a NRDIM calculator for gNB

        :param scf_file_path: gNB scf file path.
        :type scf_file_path: str
        :param cell_distname: specified cell distname.
        :type cell_distname: str
        :param cellgroup_distname: specified cell group distname.
        :type cellgroup_distname: str
        :param scf_mediation: specified offline scf mediation table.
        :type scf_mediation: dict
        :param alias: calculator alias name.
        :type alias: str
        :param url: specified local NRDIM website url, default use official website.
        :type url: str
        :param nrdim_parameters: Specified default NRDIM parameters,
            notice: if the parameter is existed in SCF, will use SCF value finally.
            e.g csirsTrackingPeriod=1
        :type nrdim_parameters: dict
        :return: nrdim calculator instance
        :rtype: NrdimCalculator
        """
        nrdim_calculator = MyNrdimCalculator(
            specified_nrdim_params=nrdim_parameters, url=url
        )
        nrdim_calculator.convert_scf_to_nrdim_params(
            scf_file_path=scf_file_path,
            cell_distname=cell_distname,
            cellgroup_distname=cellgroup_distname,
            scf_mediation=scf_mediation,
        )
        self.configured_nrdim_calculator.add(nrdim_calculator, alias)
        return nrdim_calculator

    @lookup_handler.implicit_lookup
    def set_parameter(self, name, value, calculator=None):
        """
        Set NRDIM parameter value, nrdim config parameters table.
        :param name: parameter name.
        :type name: str
        :param value: parameter value.
        :type value: int
        :param calculator: calculator alias name.
        :type calculator: str
        """
        calculator.set_parameter(name, value)

    @lookup_handler.implicit_lookup
    def get_parameter(self, name=None, calculator=None):
        """
        Get NRDIM parameter value, nrdim config parameters table.
        :param name: parameter name.
        :type name: str
        :param calculator: calculator alias name.
        :type calculator: str
        :return: parameter value.
        :rtype: int
        """
        return calculator.get_parameter(name)

    @lookup_handler.implicit_lookup
    def calculate_expected_throughput(self, calculator=None, **kwargs):
        """
        Get calculated expected throughput value.
        :param calculator: calculator alias name.
        :type calculator: str
        :return: throughput downlink and uplink values, unit is "Mbps". e.g {'downlink': 1301.929, 'uplink': 117.1586}
        :rtype: dict
        """
        return calculator.calculate_throughput(**kwargs)


if __name__ == "__main__":
    # logging.basicConfig(
    #     level=logging.DEBUG,
    #     format="%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s",
    #     datefmt="%a, %d %b %Y %H:%M:%S",
    #     filename="debug.log",
    #     filemode="w",
    # )

    # console = logging.StreamHandler()
    # console.setLevel(logging.DEBUG)
    # formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
    # console.setFormatter(formatter)
    # logging.getLogger("").addHandler(console)

    default_url = "http://5gtables.eecloud.dynamic.nsn-net.net/"
    NRDIM = Mynrdim()
    NRDIM.setup(
        scf_file_path="SCF.xml",
        cell_distname="MRBTS-579/NRBTS-579/NRCELL-1",
        cellgroup_distname="MRBTS-579/NRBTS-579/NRCELLGRP-0",
        url=default_url,
    )
    tp = NRDIM.calculate_expected_throughput()

    print(tp)
