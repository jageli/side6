from qtawesome import icon_browser
import qtawesome as qta
from PySide6.QtWidgets import QApplication
from PySide6.QtGui import QColor


def my_run():
    icon_browser.run()


def export():
    app = QApplication([])
    # icon = qta.icon("fa5s.feather-alt", color="red")
    # icon = qta.icon("fa.feed", color=QColor(187, 188, 190))
    icon = qta.icon("mdi.help", color=QColor(187, 188, 190))
    pixmap = icon.pixmap(16, 16)
    pixmap.save("help.png", quality=100)


# export()
my_run()
