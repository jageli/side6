from typing import Optional
from PySide6.QtWidgets import (
    QApplication,
    QStyle,
    QVBoxLayout,
    QPushButton,
    QGridLayout,
    QWidget,
)
from PySide6.QtCore import QSize


class MyWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("图标")
        self.resize(800, 600)
        self.mainlayout = QVBoxLayout()
        self.btnlayout = QGridLayout()

        currentRow = 0
        rowCount = 4
        for index, each in enumerate(QStyle.StandardPixmap):
            currentCol = index % rowCount

            btn = QPushButton()
            btn.setText(f"{str(index)}-{str(each)}")
            btn.setIcon(self.style().standardIcon(each))
            btn.setIconSize(QSize(24, 24))

            if currentCol == 0:
                currentRow += 1
            self.btnlayout.addWidget(btn, currentRow, currentCol)

        self.mainlayout.addLayout(self.btnlayout)
        self.setLayout(self.mainlayout)


if __name__ == "__main__":
    app = QApplication([])
    window = MyWindow()
    window.show()
    app.exec()
