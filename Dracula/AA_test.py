import sys
from PySide6.QtWidgets import QApplication
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWebEngineCore import QWebEnginePage
from PySide6.QtCore import QUrl, QFile, QIODevice
from PySide6.QtWebEngineCore import QWebEngineScript

file = QFile("SCF.xml")
file.open(QIODevice.ReadOnly)
content = file.readAll().data().decode("utf-8")
file.close()

javascript = f"""
function hide_div() {{
  var element = document.getElementById('NRDim_Header');
  element.style.display = 'none';
}}

function upload() {{
  var SCFFullFile = SCFImport_GUI_uploadFile();
  SCFImport_updateInfo(SCFFullFile);
}}

function sleep(milliseconds) {{
  return new Promise((resolve) => {{
    setTimeout(resolve, milliseconds);
  }});
}}

function simulateChooseFile() {{
  const content = `{content}`; 
	const file = new File([content], "SCF.xml",{{type: 'text/xml'}});
	const fileList = new DataTransfer();
	fileList.items.add(file);
	const input = document.getElementById('SCFFile');
	input.files = fileList.files;
	console.log(input.files[0]);
}}

async function start_run() {{
  console.log('Start');
  hide_div();
  await sleep(6000);
  simulateChooseFile();
  //await sleep(2000);
  console.log('End');
  upload();
}}
start_run();
"""

app = QApplication(sys.argv)
browser = QWebEngineView()
browser.load(QUrl("http://5gtables.eecloud.dynamic.nsn-net.net/?tool=SCFImport"))


# script = QWebEngineScript()
# script.setSourceCode(javascript)


def js_finished():
    # html = page.toHtml(javascript_callback)
    # print(html)
    print("hello")


def on_load_finished(finished):
    if finished:
        page = browser.page()
        page.runJavaScript(javascript)
        print("hello11")
        # page.loadFinished.connect(js_finished)
        js_finished()

def javascript_callback(result):
    print(result)


browser.loadFinished.connect(on_load_finished)

# browser.page().toHtml(javascript_callback)

# def get_html(ok):
#     html = browser.page().toHtml(javascript_callback)
#     print(html)

# browser.loadFinished.connect(get_html)


browser.resize(1200, 600)
browser.setWindowTitle("NR Dim")
browser.show()
sys.exit(app.exec())

# app = QApplication(sys.argv)
# view = QWebEngineView()
# view.load(QUrl("http://5gtables.eecloud.dynamic.nsn-net.net/?tool=SCFImport"))
# page = view.page()

# def on_load_finished(finished):
#     if finished:
#         page = view.page()
#         page.runJavaScript("document.getElementById('fileInput').click()")


# view.loadFinished.connect(on_load_finished)


# def on_file_selected():
#     file_path="C:/log/scf.xml"
#     url = QUrl.fromLocalFile(file_path)
#     file_urls = [url.toString()]
#     file_path = file_urls[0][7:]  # 获取本地文件路径
#     file = QFile(file_path)

#     # 打开文件并获取内容
#     file.open(QFile.ReadOnly)
#     content = file.readAll()
#     file.close()

#     # 异步上传
#     page.uploadFile(QUrl(), os.path.basename(file_path), content)


# view.loadFinished.connect(on_load_finished)

# page.chooseFiles.connect(on_file_selected)

# view.show()
# sys.exit(app.exec())


# def on_load_finished(finished):
#     if finished:
#         page = browser.page()
#         page.runJavaScript("document.getElementById('SCFFile').click()")


# browser = QWebEngineView()
# # browser.loadFinished.connect(lambda x: test(x, browser))
# browser.load(QUrl("http://5gtables.eecloud.dynamic.nsn-net.net/?tool=SCFImport"))
# browser.loadFinished.connect(on_load_finished)


# def test(x, browser: QWebEngineView):
#     print(x)

#     file_path = r"SCF.xml"
#     file_base_name = os.path.basename(file_path)
#     print(file_base_name)

#     javascript = f"""
#     const file = new File(["foo"], "SCF.xml");
#     const fileList = new DataTransfer();
#     fileList.items.add(file);
#     const input = document.getElementById('SCFFile');
#     input.files = fileList.files;
#     input.setAttribute('value', 'SCF.xml');
#     const button = document.getElementById('SCFImport_uploadButton');
#     button.click();

#     """
#     XX = """
#     const file = new File(["foo"], "SCF.xml");
#     const fileList = new DataTransfer();
#     fileList.items.add(file);
#     const input = document.getElementById('SCFFile');
#     input.files = fileList.files;
#     input.setAttribute('value', 'SCF.xml');
#     const button = document.getElementById('SCFImport_uploadButton');
#     button.click();
#     """
#     # search_text = "Python Qt"
#     # javascript = """
#     # document.getElementById("kw").value = "{}";
#     # document.getElementById("su").click();
#     # """.format(search_text)

#     # javascript = """
#     # document.getElementById("kw").value = "{}";
#     # document.getElementById("su").click();
#     # """.format(search_text)

#     result = browser.page().runJavaScript(javascript)
#     print(result)
#     browser.page().toHtml(javascript_callback)


# app = QApplication(sys.argv)


# def javascript_callback(result):
#     print(result)


# view.show()
# sys.exit(app.exec())
