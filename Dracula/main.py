# ///////////////////////////////////////////////////////////////
#
# BY: WANDERSON M.PIMENTA
# PROJECT MADE WITH: Qt Designer and PySide6
# V: 1.0.0
#
# This project can be used freely for all uses, as long as they maintain the
# respective credits only in the Python scripts, any information in the visual
# interface (GUI) can be modified without any implication.
#
# There are limitations on Qt licenses if you want to use your products
# commercially, I recommend reading them on the official website:
# https://doc.qt.io/qtforpython/licenses.html
#
# ///////////////////////////////////////////////////////////////

import sys
import os
import platform

# IMPORT / GUI AND MODULES AND WIDGETS
# ///////////////////////////////////////////////////////////////
from modules import *
from widgets import *

os.environ["QT_FONT_DPI"] = "96"  # FIX Problem for High DPI and Scale above 100%

# SET AS GLOBAL WIDGETS
# ///////////////////////////////////////////////////////////////
widgets = None


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        # SET AS GLOBAL WIDGETS
        # ///////////////////////////////////////////////////////////////
        self.dragPos = None
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        global widgets
        widgets = self.ui
        self.resize(1500, 900)

        # Load config
        # ///////////////////////////////////////////////////////////////
        MyOwnFuntions.init_setup_config(self)

        # USE CUSTOM TITLE BAR | USE AS "False" FOR MAC OR LINUX
        # ///////////////////////////////////////////////////////////////
        Settings.ENABLE_CUSTOM_TITLE_BAR = True

        # APP VERSION
        # ///////////////////////////////////////////////////////////////
        current_version = "V.20231205"
        MY_GLOBAL_DICT.update({"current_version": current_version})
        self.ui.version.setText(current_version)
        check_version_thread = CheckVersion(current_version=current_version)
        check_version_thread.message.connect(lambda x: self.ui.version.setText(x))
        check_version_thread.message.connect(
            lambda: self.ui.version.setStyleSheet("color: rgb(255, 121, 198)")
        )
        check_version_thread.start()

        self.ui.btn_upgrade.clicked.connect(lambda: upgrade_app(self))

        # APP NAME
        # ///////////////////////////////////////////////////////////////
        title = "Dracula - 5GRAN - GUI"
        description = "Dracula - 5GRAN"
        # APPLY TEXTS
        self.setWindowTitle(title)
        widgets.titleRightInfo.setText(description)

        # TOGGLE MENU
        # ///////////////////////////////////////////////////////////////
        widgets.toggleButton.clicked.connect(lambda: UIFunctions.toggleMenu(self, True))

        # SET UI DEFINITIONS
        # ///////////////////////////////////////////////////////////////
        UIFunctions.uiDefinitions(self)

        # QTableWidget PARAMETERS
        # ///////////////////////////////////////////////////////////////
        widgets.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        # PUBLIC THREAD
        # ///////////////////////////////////////////////////////////////
        t = PublicThreadStartUp()
        t.import_mode_signal.connect(lambda x: self.ui.Label_import_progress.setText(x))
        t.start()

        # TPUT PAGE
        # ///////////////////////////////////////////////////////////////
        TputPage.init_setup(self)

        # BUTTONS CLICK
        # ///////////////////////////////////////////////////////////////

        # LEFT MENUS
        widgets.btn_home.clicked.connect(self.buttonClick)
        widgets.btn_rf.clicked.connect(self.buttonClick)
        widgets.btn_tput.clicked.connect(self.buttonClick)

        # widgets.btn_save.clicked.connect(self.buttonClick)
        widgets.btn_save.setVisible(False)
        widgets.btn_exit.setVisible(False)

        widgets.btn_change_theme.clicked.connect(self.theme_btn_click)

        # extraTopMenu
        widgets.pushButton_save.clicked.connect(
            lambda: MyOwnFuntions.pushButton_save_passed_slot(self)
        )

        widgets.comboBox_testline_name.activated.connect(
            lambda x: MyOwnFuntions.update_comboBox_testline_name_list(
                self, current_testline=widgets.comboBox_testline_name.itemText(x)
            )
        )
        widgets.comboBox_testline_name.installEventFilter(self)

        widgets.comboBox_site_type.activated.connect(
            lambda x: MyOwnFuntions.web_information_show(
                self, current_type=widgets.comboBox_site_type.itemText(x)
            )
        )

        # EXTRA LEFT BOX
        def openCloseLeftBox():
            UIFunctions.toggleLeftBox(self, True)

        widgets.toggleLeftBox.clicked.connect(openCloseLeftBox)
        widgets.extraCloseColumnBtn.clicked.connect(openCloseLeftBox)

        # EXTRA RIGHT BOX
        def openCloseRightBox():
            UIFunctions.toggleRightBox(self, True)

        widgets.settingsTopBtn.clicked.connect(openCloseRightBox)

        # SHOW APP
        # ///////////////////////////////////////////////////////////////
        self.show()

        # SET CUSTOM THEME
        # ///////////////////////////////////////////////////////////////
        absPath = None
        if getattr(sys, "frozen", False):
            absPath = os.path.dirname(os.path.abspath(sys.executable))
        elif __file__:
            absPath = os.path.dirname(os.path.abspath(__file__))

        useCustomTheme = MY_GLOBAL_DICT.get("theme")
        self.useCustomTheme = useCustomTheme
        self.absPath = absPath
        themeFile = "themes/py_dracula_light.qss"
        self.selectMenu_btn = "btn_home"

        # SET MY OWN HACKS
        MyOwnStyle.dark_style(self)

        # SET THEME AND HACKS
        if useCustomTheme:
            # LOAD AND APPLY STYLE
            UIFunctions.theme(self, themeFile, True)

            # SET HACKS
            AppFunctions.setThemeHack(self)

            # SET MY OWN HACKS
            MyOwnStyle.light_style(self)

        # SET HOME PAGE AND SELECT MENU
        # ///////////////////////////////////////////////////////////////
        # widgets.stackedWidget.setCurrentWidget(widgets.home)
        # widgets.btn_home.setStyleSheet(
        #     UIFunctions.selectMenu(widgets.btn_home.styleSheet())
        # )

        widgets.stackedWidget.setCurrentWidget(widgets.tput_page)
        widgets.btn_tput.setStyleSheet(
            UIFunctions.selectMenu(widgets.btn_tput.styleSheet())
        )

    def theme_btn_click(self):
        MyOwnFuntions.change_theme_fun(self)
        selectMenu_btn_object = self.ui.topMenu.findChild(QObject, self.selectMenu_btn)

        # 删除已选按钮的样式
        if self.useCustomTheme:
            sty = Settings.MENU_SELECTED_STYLESHEET_DRAK
        else:
            sty = Settings.MENU_SELECTED_STYLESHEET_LIGHT

        UIFunctions.clear_selectMenu(self, self.selectMenu_btn, sty)
        selectMenu_btn_object.setStyleSheet(
            UIFunctions.selectMenu(selectMenu_btn_object.styleSheet())
        )

    # BUTTONS CLICK
    # Post here your functions for clicked buttons
    # ///////////////////////////////////////////////////////////////
    def buttonClick(self):
        # GET BUTTON CLICKED
        btn = self.sender()
        btnName = btn.objectName()

        Parent = btn.parent().objectName()
        if Parent == "topMenu":
            self.selectMenu_btn = btnName
        else:
            self.selectMenu_btn = "btn_home"

        # SHOW HOME PAGE
        if btnName == "btn_home":
            self.ui.stackedWidget.setCurrentWidget(self.ui.home)
            UIFunctions.resetStyle(self, btnName)
            btn.setStyleSheet(UIFunctions.selectMenu(btn.styleSheet()))
            # print(self.ui.btn_home.styleSheet())

        # SHOW RF PAGE
        if btnName == "btn_rf":
            self.ui.stackedWidget.setCurrentWidget(self.ui.rf_page)
            UIFunctions.resetStyle(self, btnName)
            btn.setStyleSheet(UIFunctions.selectMenu(btn.styleSheet()))

        # SHOW TPUT PAGE
        if btnName == "btn_tput":
            self.ui.stackedWidget.setCurrentWidget(self.ui.tput_page)  # SET PAGE
            UIFunctions.resetStyle(self, btnName)  # RESET OTHERS BUTTONS SELECTED
            btn.setStyleSheet(UIFunctions.selectMenu(btn.styleSheet()))  # SELECT MENU

        if btnName == "btn_save":
            print("Save BTN clicked!")

        # PRINT BTN NAME
        # print(f'Button "{btnName}" pressed!')

    # RESIZE EVENTS
    # ///////////////////////////////////////////////////////////////
    def resizeEvent(self, event):
        # Update Size Grips
        UIFunctions.resize_grips(self)

    # MOUSE CLICK EVENTS
    # ///////////////////////////////////////////////////////////////
    def mousePressEvent(self, event):
        # SET DRAG POS WINDOW
        self.dragPos = QCursor.pos()
        # PRINT MOUSE EVENTS
        # if event.buttons() == Qt.LeftButton:
        #     print("Mouse click: LEFT CLICK")
        # if event.buttons() == Qt.RightButton:
        #     print("Mouse click: RIGHT CLICK")

    # close EVENTS
    # ///////////////////////////////////////////////////////////////
    def handleButtonClick(self):
        self.close()

    def closeEvent(self, event):
        reply = QMessageBox.question(
            self,
            "Message",
            "Are you sure to quit?",
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No,
        )
        if reply == QMessageBox.Yes:
            # stop_thread(MY_GLOBAL_DICT.get("_Thread_monitoring"))
            event.accept()
            os._exit(0)
        else:
            event.ignore()

    def eventFilter(self, source, event):
        if (
            event.type() == QEvent.MouseButtonPress
            and event.button() == Qt.RightButton
            and source is self.ui.comboBox_testline_name
        ):
            # menu = CustomMenu()
            # menu.exec(QCursor.pos())
            MyOwnFuntions.comboBox_testline_name_right_event(self)
            return True
        return super().eventFilter(source, event)


if __name__ == "__main__":
    """
    pyinstaller -w -i themes\icon.ico main.py --hidden-import=matplotlib.backends.backend_qt5agg --name=Dracula
    """
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon("themes\icon.ico"))
    splash = SplashPanel()
    window = MainWindow()
    splash.finish(window)
    splash.deleteLater()
    # Thread_monitor()
    sys.exit(app.exec())
