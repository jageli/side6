from PySide6.QtWidgets import QFrame
from PySide6.QtGui import QIcon
from modules.my_thread.thread_open_tputlog import OpenTputLog
from ui.ui_TputLog import Ui_Form


class Tputlog_windows(QFrame, Ui_Form):
    def __init__(self, parent=None, FileName=None):
        super(Tputlog_windows, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(":/images/images/images/PyDracula.png"))
        self.FileName = FileName
        self.setWindowTitle(self.FileName)
        self.prepare_data()

    def prepare_data(self):
        self.worker_open_tput_log = OpenTputLog(self.FileName)
        # self.worker_open_tput_log.signal_clear_textBrowser.connect(
        #     lambda: self._clear_textBrowser()
        # )
        self.worker_open_tput_log.signal_textBextBrowser_DL.connect(
            lambda x: self._slot_update_textBrowser_DL(x)
        )
        self.worker_open_tput_log.signal_textBextBrowser_UL.connect(
            lambda x: self._slot_update_textBrowser_UL(x)
        )
        self.worker_open_tput_log.signal_message.connect(
            lambda x: self._slot_update_textBrowser(x)
        )
        self.worker_open_tput_log.start()

    # def _clear_textBrowser(self):
    #     self.textBrowser_DL.clear()
    #     self.textBrowser_UL.clear()

    def _slot_update_textBrowser_DL(self, x):
        self.textBrowser_DL.append(x)

    def _slot_update_textBrowser_UL(self, x):
        self.textBrowser_UL.append(x)

    def _slot_update_textBrowser(self, x):
        self.textBrowser_DL.append(x)
        self.textBrowser_UL.append(x)
