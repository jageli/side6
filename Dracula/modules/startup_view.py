import time
from PySide6.QtWidgets import QSplashScreen
from PySide6.QtGui import QPixmap, QFont
from PySide6.QtCore import Qt


class SplashPanel(QSplashScreen):
    # 启动画面
    def __init__(self):
        super(SplashPanel, self).__init__()
        message_font = QFont()
        message_font.setBold(True)
        message_font.setPointSize(14)
        self.setFont(message_font)
        pixmap = QPixmap(":/images/images/images/nokia-refreshed-logo-3_0.jpg")
        self.setPixmap(pixmap)
        self.show()
        for i in range(1, 5):
            self.showMessage(
                "loading {}".format("." * i), alignment=Qt.AlignBottom, color=Qt.black
            )
            time.sleep(0.15)

    def mousePressEvent(self, evt):
        pass
        # 重写鼠标点击事件，阻止点击后消失

    def mouseDoubleClickEvent(self, *args, **kwargs):
        pass
        # 重写鼠标移动事件，阻止出现卡顿现象

    def enterEvent(self, *args, **kwargs):
        pass
        # 重写鼠标移动事件，阻止出现卡顿现象

    def mouseMoveEvent(self, *args, **kwargs):
        pass
        # 重写鼠标移动事件，阻止出现卡顿现象
