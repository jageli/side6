import time
from modules.my_thread.thread_check_tput import CheckTput_show
from PySide6.QtCore import Signal


class OpenTputLog(CheckTput_show):
    signal_clear_textBrowser = Signal(int)

    def __init__(self, FileName):
        super().__init__()
        self.FileName = FileName

    def run(self):
        with open(self.FileName, "r") as f:
            lines = f.readlines()
        if lines:
            self.signal_clear_textBrowser.emit(len(lines))
        else:
            return
        self.show(lines=lines)

    def show(self, lines):
        for line in lines:
            if "UlCounterUpdate" in line:
                try:
                    self.ul_tp(line)
                except Exception as _:
                    pass
                continue
            
            if "L2_LO_TB_BU" and "DataPdu" in line:
                try:
                    self.dl_tp(line)
                except Exception as _:
                    pass
                continue
            
            self.MSG(line)
