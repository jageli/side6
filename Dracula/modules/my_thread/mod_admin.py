from modules.public_thread import color_text, timeS
from modules.my_global_dict import MY_GLOBAL_DICT
from PySide6.QtCore import Signal, QObject


class GetScf(QObject):
    signal_message_err = Signal(str)

    def __init__(self):
        QObject.__init__(self)

    def get_scf(self) -> str:
        """_summary_
        get SCF to logs/SCF dir
        Returns:
            str: _description_
            logs/SCF/XXXX.xml
        """

        testline_name = MY_GLOBAL_DICT.get("current_testline").get("testline_name")
        site_type = MY_GLOBAL_DICT.get("current_testline").get("site_type")
        filename = f"{testline_name}_SCF_{timeS()}.xml"
        local_dir_path = "logs/SCF/"

        if site_type == "classical":
            # from taf.sbts.oam.coam.admin import admin
            admin = MY_GLOBAL_DICT.get("admin")
            if admin is None:
                self.signal_message_err.emit(
                    color_text(text="admin is loading,please waiting!")
                )
                return
            node_ip = MY_GLOBAL_DICT.get("current_testline").get("node_ip")
            target_file_name = f"{local_dir_path}{filename}"
            api = admin()
            connection = api.connect_to(
                bts_host=node_ip, admin_api_log_dir="logs/admin_api"
            )

            api.get_current_scf_file(
                local_dir_path=local_dir_path, file_name=filename, connection=connection
            )
            api.teardown_admin()
            target_file_name = f"{local_dir_path}{filename}"
        else:
            vdu = MY_GLOBAL_DICT.get("vdu")
            if vdu is None:
                self.signal_message_err.emit(
                    color_text(text="admin is loading,please waiting!")
                )
                return
            web_ip = MY_GLOBAL_DICT.get("current_testline").get("web_ip")
            web_username = MY_GLOBAL_DICT.get("current_testline").get("web_username")
            web_password = MY_GLOBAL_DICT.get("current_testline").get("web_password")
            api = vdu()
            api.connect_to(
                host=web_ip,
                port="443",
                username=web_username,
                password=web_password,
                secure=True,
            )

            api.get_current_plan_file(
                local_dir_path=local_dir_path,
                filename=filename,
                include_runtime_data=False,
            )
            api.disconnect()
            target_file_name = f"{local_dir_path}{filename}"

        if target_file_name is None:
            self.signal_message_err.emit(
                color_text(text="admin is loading,please waiting!")
            )
            return

        return target_file_name
