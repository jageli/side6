import threading
from PySide6.QtCore import Signal, QObject
from modules.my_thread.mod_admin import GetScf
from modules.my_thread.mod_SCF_parse import ParseScf
from modules.my_thread.mod_nrdim import MyNrdimRequest, Mynrdim


class GetPeakNrdim(threading.Thread, QObject):
    finished = Signal()
    signal_message = Signal(dict)
    signal_create_table = Signal(int, bool, str)
    signal_print = Signal(dict)
    signal_error = Signal(str)
    signal_messagebox_error = Signal(str)

    def __init__(self, scf_file_path):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        self.scf_file_path = scf_file_path
        self.url = "http://5gtables.eecloud.dynamic.nsn-net.net/"
        self.NRDIM = None
        self.nrgroup_nrcell_parameters: dict = None
        self.nrbts_parameters: dict = None

    def run(self):
        self.get_scf()
        if self.scf_file_path is not None:
            (
                self.nrgroup_nrcell_parameters,
                self.nrbts_parameters,
            ) = self.nrcell_parameters()
            self.calculate_peak_tput()
        
        self.finished.emit()

    def get_scf(self):
        def err_message(text):
            self.scf_file_path = None
            self.signal_messagebox_error.emit(text)

        scf_file_path = GetScf()
        scf_file_path.signal_message_err.connect(lambda x: err_message(x))
        self.scf_file_path = scf_file_path.get_scf()

    def nrcell_parameters(self) -> dict:
        Parse = ParseScf(self.scf_file_path)
        nrgroup_nrcell_parameters = Parse.nrcellgroup_nrcell_parameters()
        nrbts_parameters = Parse.get_nrbts_mode()
        return nrgroup_nrcell_parameters, nrbts_parameters

    def get_nrcell_nrcellgroup(self) -> list:
        """_summary_

        Returns:
            list: _description_
            [{'cell_distname': 'MRBTS-xxx/NRBTS-xxx/NRCELL-x', 'cellgroup_distname': 'MRBTS-xxx/NRBTS-xxx/NRCELLGRP-x'}, ...]
        """
        Parse = ParseScf(self.scf_file_path)
        NRCELLGRP_NRCELL_mapping = Parse.mapping_nrcell_nrcellgroup()
        return NRCELLGRP_NRCELL_mapping

    def calculate_peak_tput(self):
        """_summary_
        signal_message: {"cell_id": "loading","downlink": "loading","uplink": "loading","link": "loading","id": 0}
        """
        try:
            url_request = MyNrdimRequest()
            url_request._send_request(url=self.url)
        except Exception as e:
            self.signal_create_table.emit(1, False, str(e))
            return

        self.NRDIM = Mynrdim()
        NRCELLGRP_NRCELL_mapping = self.get_nrcell_nrcellgroup()
        self.signal_create_table.emit(len(NRCELLGRP_NRCELL_mapping), True, "")
        for n, i in enumerate(NRCELLGRP_NRCELL_mapping):
            temp_dict = {
                "cell_id": "loading",
                "downlink": "loading",
                "uplink": "loading",
                "link": "loading",
                "id": 0,
            }
            cell_distname = i.get("cell_distname")
            cellgroup_distname = i.get("cellgroup_distname")

            cell_id = cell_distname.split("/")[-1].split("-")[-1]
            temp_dict.update({"cell_id": cell_id, "id": n})
            self.signal_message.emit(temp_dict)

            self.NRDIM.setup(
                scf_file_path=self.scf_file_path,
                cell_distname=cell_distname,
                cellgroup_distname=cellgroup_distname,
                url=self.url,
            )

            self.revise(
                cell_distname=cell_distname,
                cellgroup_distname=cellgroup_distname,
                revise_parameters=["trsMCS", "SslotMCS", "sib1FDM"],
            )

            tp: dict = self.NRDIM.calculate_expected_throughput()
            link = "{}?code={}".format(self.url, tp.get("code"))
            tp.update({"link": link})
            temp_dict.update(tp)

            # tp = {
            #     "code": "IQWFT6qvewk6aUoENRzACAAIAAAAAATiACcQAAAJCYJKAQBAAAAAACYABIABkVAAEJ0AAAAJAAAAAAAAAkgAAAAAgABoAAAAAAAAAABAgAAAABAAUQARgYAAgAAAAAAAAAAAYBQJALAAAAAAAAAIAAO4AAAAAAAAAAABjA",
            #     "downlink": 412.52875,
            #     "link": "http://5gtables.eecloud.dynamic.nsn-net.net/?code=IQWFT6qvewk6aUoENRzACAAIAAAAAATiACcQAAAJCYJKAQBAAAAAACYABIABkVAAEJ0AAAAJAAAAAAAAAkgAAAAAgABoAAAAAAAAAABAgAAAABAAUQARgYAAgAAAAAAAAAAAYBQJALAAAAAAAAAIAAO4AAAAAAAAAAABjA",
            #     "uplink": 52.7784,
            # }
            # self.signal_print.emit(temp_dict)
            self.signal_message.emit(temp_dict)

    def revise(self, cell_distname, cellgroup_distname, revise_parameters: list = None):
        if revise_parameters is None:
            return
        if "trsMCS" in revise_parameters:
            self._revise_trsMCS(cell_distname, cellgroup_distname)
        if "SslotMCS" in revise_parameters:
            self._revise_SslotMCS()
        if "sib1FDM" in revise_parameters:
            self._revise_sib1FDM(cell_distname)

    def _revise_trsMCS(self, cell_distname, cellgroup_distname):
        """_summary_
        https://confluence.ext.net.nokia.com/pages/viewpage.action?spaceKey=5GRANHZ&title=NRDIM+Deployment+History
        Args:
            cell_distname (_type_): _description_
            cellgroup_distname (_type_): _description_
        """
        actPdschRmCsirsForTracking = (
            self.nrgroup_nrcell_parameters.get(cellgroup_distname)
            .get(cell_distname)
            .get("actPdschRmCsirsForTracking")
        )
        if actPdschRmCsirsForTracking == "true":
            DLBandwidth = self.NRDIM.get_parameter(name="DLBandwidth")
            if DLBandwidth <= 3:
                self.NRDIM.set_parameter(name="trsMCS", value=25)
            elif DLBandwidth >= 5:
                self.NRDIM.set_parameter(name="trsMCS", value=10)

    def _revise_SslotMCS(self):
        self.NRDIM.set_parameter(name="SslotMCS", value=29)

    def _revise_sib1FDM(self, cell_distname):
        nrbts_distname = cell_distname.rsplit("/", 1)[0]
        actFdmScheduling = self.nrbts_parameters.get(nrbts_distname).get(
            "actFdmScheduling"
        )
        if actFdmScheduling == "true":
            self.NRDIM.set_parameter(name="sib1FDM", value=1)
