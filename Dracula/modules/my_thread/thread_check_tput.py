import queue
import re
import threading
import time
from loguru import logger
import paramiko
from PySide6.QtCore import Signal, QObject
from modules.public_thread import color_text, timeS
from modules import MY_GLOBAL_DICT


class CheckTput(threading.Thread, QObject):
    finished = Signal()
    signal_message = Signal(str)

    def __init__(self):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        self.conn = None
        self.debug_mode = MY_GLOBAL_DICT.get("debug_mode")
        self.check_tput_mode = MY_GLOBAL_DICT.get("check_tput_mode")
        self.ns = None
        # use logging
        # testline_name = MY_GLOBAL_DICT.get("current_testline").get("testline_name")
        # tput_log_name = "logs/tputlog/{}_tputlog_{}.log".format(testline_name, timeS())
        # self.tput_log = logging.Logger("tput_log")
        # self.tput_log_handler = RotatingFileHandler(
        #     tput_log_name, maxBytes=50 * 1024 * 1024, backupCount=20
        # )
        # self.tput_log.addHandler(self.tput_log_handler)

        testline_name = MY_GLOBAL_DICT.get("current_testline").get("testline_name")
        tput_log_name = "logs/tputlog/{}_tputlog_{}.log".format(testline_name, timeS())

        file_handler_params = {
            "sink": tput_log_name,
            "filter": lambda record: record["extra"]["module"] == "tputlog",
            "enqueue": True,
            "format": "[{time}] - {message}",
        }

        self.tput_log = logger.bind(module="tputlog", configure=False)
        self.tput_log_handler_id = self.tput_log.add(**file_handler_params)
        self.tool_log: logger = MY_GLOBAL_DICT.get("tool_log")

    def run(self):
        # self.tool_log.info("start to check throughput")
        site_type = MY_GLOBAL_DICT.get("current_testline").get("site_type")
        WithBtsLog = MY_GLOBAL_DICT.get("WithBtsLog")

        debug_btslog_substring = MY_GLOBAL_DICT.get("debug_btslog_substring")
        filter_text_list = [
            "UlCounterUpdate",
            "DataPdu",
            "L2_PS_DL_UP_.*ue.rnti",
            "handlePrachPreamble",
            "Pdcch Resources updated for Ue rnti",
            "informDlOfSuccessfulMsg3Ue",
            "Store UE",
            "dl_awaitMsg4WithoutHiMsgTimerExpiry",
        ]

        if self.debug_mode and debug_btslog_substring is not None:
            # get value from textEdit_substring to list
            debug_string_list = debug_btslog_substring.strip().split("\n")
            # Remove None
            debug_string_list = list(filter(None, debug_string_list))
            # get ignore text
            debug_string_list = [
                i for i in debug_string_list if not i.startswith(r"\\") and i != ".*"
            ]

        else:
            debug_string_list = []

        if debug_string_list:
            self.signal_message.emit(color_text("open debug mode", color="yellow"))

        MY_GLOBAL_DICT.update(
            {
                "filter_text_list": filter_text_list,
                "debug_string_list": debug_string_list,
            }
        )

        filter_text = r"\|".join(filter_text_list + debug_string_list)

        self.conn = ConnectNodeB(signal=self.signal_message, tput_log=self.tput_log)

        if not self.conn.login():
            MY_GLOBAL_DICT.update({"tput_flag": False})
            self.finished.emit()
            self.close_log()
            try:
                self.conn.close_conn()
            except Exception as e:
                self.tool_log.exception(e)
            return

        if site_type == "classical" and self.check_tput_mode == "NC mode":
            self.conn.add_flag_for_classical()
            cmd = 'nc -u -w 100 -l 127.0.0.1 51000 | grep -a "{}"'.format(filter_text)
            if WithBtsLog:
                cmd = "nc -u -w 100 -l 127.0.0.1 51000"

        if site_type == "classical" and self.check_tput_mode == "TCP dump mode":
            cmd = 'tcpdump -A -i any port 51000 | grep -a "{}"'.format(filter_text)
            if WithBtsLog:
                cmd = "tcpdump -A -i any port 51000"

        if site_type == "cloud":
            try:
                self.check_ns_cloud()
            except Exception as e:
                self.signal_message.emit(color_text(f"{e}"))
                self.close_log()
                self.finished.emit()
                return
            ns = self.ns

            try:
                logservice = self.conn.get_log_server_pod()
            except Exception as e:
                self.signal_message.emit(color_text(f"{e}"))
                self.close_log()
                self.finished.emit()
                return
            try:
                syslog_dir_varn = self.conn.get_syslog_dir_varn(
                    ns=ns, logservice=logservice
                )
            except Exception as e:
                self.signal_message.emit(color_text(f"{e}"))
                self.close_log()
                self.finished.emit()
                return

            cmd = 'kubectl -n {0} exec -it {1} -c ctr-cran1-logstorageserver  -- tail -f {2} | grep -a "{3}"'.format(
                ns, logservice, syslog_dir_varn, filter_text
            )

            if WithBtsLog:
                cmd = "kubectl -n {0} exec -it {1} -c ctr-cran1-logstorageserver  -- tail -f {2}".format(
                    ns, logservice, syslog_dir_varn
                )

        timeout = 30
        if self.debug_mode:
            timeout = 1800
        self.signal_message.emit(color_text("start to check throughput", color="green"))
        try:
            self.conn.start_check_tput_cmd(cmd, timeout=timeout)
        except TimeoutError:
            self.signal_message.emit(color_text("Time out"))
            self.stop()
            self.finished.emit()

        self.tool_log.info("done")
        self.finished.emit()

    def check_ns_cloud(self):
        self.ns = MY_GLOBAL_DICT.get("current_testline").get("Namespace")
        if not self.ns:
            raise Exception(f"can't find namespace")

    def close_log(self):
        logger.remove(self.tput_log_handler_id)

    def stop(self):
        MY_GLOBAL_DICT.update({"tput_flag": False})

        try:
            logger.remove(self.tput_log_handler_id)
        except Exception as _:
            pass

        try:
            # self.conn.stop_check_tput_cmd()
            if self.check_tput_mode == "NC mode":
                self.conn.recovery_flag_for_classical()

            self.conn.close_conn()

        except Exception as e:
            self.tool_log.warning(e)


class ConnectNodeB:
    def __init__(self, signal=None, tput_log: logger = None) -> None:
        self.signal = signal
        self.tput_log = tput_log
        self.objsftp = None
        self.obj = None
        self.trans = None
        self.node_ip = MY_GLOBAL_DICT.get("current_testline").get("node_ip")
        self.node_username = MY_GLOBAL_DICT.get("current_testline").get("node_username")
        self.node_password = MY_GLOBAL_DICT.get("current_testline").get("node_password")
        self.queue_tput_log: queue.Queue = MY_GLOBAL_DICT.get("queue_tput_log")
        self.ns = MY_GLOBAL_DICT.get("current_testline").get("Namespace")
        self.tool_log: logger = MY_GLOBAL_DICT.get("tool_log")

    def login(self):
        try:
            self.trans = paramiko.Transport((self.node_ip, 22))
            self.trans.connect(username=self.node_username, password=self.node_password)
            self.obj = paramiko.SSHClient()
            self.obj._transport = self.trans
            self.objsftp = self.obj.open_sftp()
            return True
        except Exception as e:
            MY_GLOBAL_DICT.update({"tput_flag": False})
            self.signal.emit("<p style='color:red;font-weight:bold'>{}</p>".format(e))
            return False

    def close_conn(self):
        self.objsftp.close()
        self.obj.close()
        self.trans.close()

    def start_check_tput_cmd(self, cmd, timeout=20):
        _, stdout, _ = self.obj.exec_command(
            cmd, bufsize=-1, get_pty=True, timeout=timeout
        )
        try:
            for line in stdout:
                # logger.bind(module="main").info("{}".format(line.strip()))
                line = line.strip().strip(chr(0))
                self.tput_log.info("{}".format(line))

                self.queue_tput_log.put(line)
                # self.signal.emit(line.strip())
        except Exception as e:
            self.tool_log.exception(e)

    def stop_check_tput_cmd(self):
        _, stdout, _ = self.obj.exec_command(
            "ps -ef | grep 'nc -u -w 100 -l 127.0.0.1 51000' | grep -v grep | awk '{print $2}'| xargs kill -9",
            bufsize=-1,
            get_pty=True,
            timeout=5,
        )
        for line in stdout:
            # logger.bind(module="main").info("{}".format(line.strip()))
            self.tput_log.info("{}".format(line.strip()))

    def add_flag_for_classical(self):
        cmd1 = "(echo rad -mw 0x10041 5)|nc 0 15007"
        cmd2 = "(echo rad -w 0x10042 0x7F000001)|nc 0 15007"
        cmd3 = "(echo rad -w 0x10043 0xC738)|nc 0 15007"
        cmd = " && ".join((cmd1, cmd2, cmd3))
        stdin, stdout, _ = self.obj.exec_command(cmd, bufsize=-1, get_pty=True)
        stdin.flush()
        for _ in stdout:
            pass

    def recovery_flag_for_classical(self):
        cmd = "(echo rad -w 0x10042 0xC0A8FF7E)|nc 0 15007"
        stdin, stdout, _ = self.obj.exec_command(cmd, bufsize=-1, get_pty=True)
        stdin.flush()
        for _ in stdout:
            pass

    def get_log_server_pod(self) -> str:
        cmd = "oc get po -n {} | grep 'logservice' | awk '{{print $1}}'".format(self.ns)
        _, stdout, _ = self.obj.exec_command(cmd, bufsize=-1, get_pty=False, timeout=20)
        logservice_name = stdout.readline().strip()
        if not logservice_name:
            raise Exception(
                f"can't get logservice pod. please check if the namespace {self.ns} is correct"
            )

        cmd = "oc get po -n {} | grep 'logservice' | awk '{{print $3}}'".format(self.ns)
        _, stdout, _ = self.obj.exec_command(cmd, bufsize=-1, get_pty=False, timeout=20)
        logservice_status = stdout.readline().strip()

        if logservice_status != "Running":
            raise Exception(f"{logservice_name} is {logservice_status}. please check")
        return logservice_name

    def get_syslog_dir_varn(self, ns, logservice):
        dir_list = ["/var/log/localvolume/syslog", "/var/log/syslog/syslog"]
        for i in dir_list:
            cmd = "kubectl -n {1} exec -it {0} -c ctr-{1}-logstorageserver -- ls {2}".format(
                logservice, ns, i
            )
            _, stdout, _ = self.obj.exec_command(
                cmd, bufsize=-1, timeout=50, get_pty=False, environment=None
            )
            result = str(stdout.read(), encoding="utf-8").strip()

            if result != "":
                return i


class CheckTput_show(threading.Thread, QObject):
    finished = Signal()
    signal_textBextBrowser_DL = Signal(str)
    signal_textBextBrowser_UL = Signal(str)
    signal_message = Signal(str)

    def __init__(self):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        self.tool_log: logger = MY_GLOBAL_DICT.get("tool_log")

        self.queue_tput_log: queue.Queue = MY_GLOBAL_DICT.get("queue_tput_log")
        self.filter_text_list = MY_GLOBAL_DICT.get("filter_text_list")
        self.debug_string_list = MY_GLOBAL_DICT.get("debug_string_list")

        self.times_compile = re.compile(r"[<](.*?)[>]", re.S)
        self.nrCellId_compile_UL = re.compile(r"nrCellIdentity (\d+)", re.S)
        self.ul_tp_compile_UL = re.compile(r"[(](.*?) rlcPduBytes", re.S)
        self.nrCellId_compile_DL = re.compile(r"nrCellId (.*?)\[TBS", re.S)
        self.dl_tp_compile_DL = re.compile(r"DataPdu: (.*?)\]", re.S)

    def run(self):
        tput_flag = True
        while tput_flag:
            if not self.queue_tput_log.empty():
                line = self.queue_tput_log.get()
                if "UlCounterUpdate" in line:
                    try:
                        self.ul_tp(line)
                    except Exception as _:
                        pass
                    continue
                if "L2_LO_TB_BU" and "DataPdu" in line:
                    try:
                        self.dl_tp(line)
                    except Exception as _:
                        pass
                    continue

                self.MSG(line)

            else:
                time.sleep(1)
            tput_flag = MY_GLOBAL_DICT.get("tput_flag")

    def ul_tp(self, line):
        nrCellId = re.search(self.nrCellId_compile_UL, line).group(1)
        times = re.search(self.times_compile, line).group(1)
        ul_tp = re.search(self.ul_tp_compile_UL, line).group(1)
        ul_tp_M = str(round(int(ul_tp) * 8 / 1000 / 1000, 1))

        html = "{} nrCellId {} UL_TP: {} ( <span style='color:violet;font-weight:bold'>{}{}</span> )".format(
            times, nrCellId, ul_tp, ul_tp_M, "M"
        )
        self.signal_textBextBrowser_UL.emit(html)

    def dl_tp(self, line):
        times = re.search(self.times_compile, line).group(1)
        nrCellId = re.search(self.nrCellId_compile_DL, line).group(1)
        dl_tp = re.search(self.dl_tp_compile_DL, line).group(1).split("|")[-1]
        dl_tp_M = str(round(int(dl_tp) * 8 / 1000 / 1000, 1))

        html = "{} nrCellId {} DL_TP: {} ( <span style='color:violet;font-weight:bold'>{}{}</span> )".format(
            times, nrCellId, dl_tp, dl_tp_M, "M"
        )
        self.signal_textBextBrowser_DL.emit(html)

    def MSG(self, line):
        if not line:
            return

        # MSG0
        if "L2_PS_DL_UP_" in line and "ue.rnti:" in line:
            try:
                self.msg1(line=line)
            except Exception as _:
                pass
            return
        # MSG2
        if "handlePrachPreamble" in line:
            try:
                self.msg2(line=line)
            except Exception as _:
                pass
            return
        # MSG3
        if "informDlOfSuccessfulMsg3Ue" in line:
            try:
                self.msg3(line=line)
            except Exception as _:
                pass
            return
        # Store UE
        if "Store UE" in line:
            try:
                self.StoreUE(line=line)
            except Exception as _:
                pass
            return
        # MSG4
        if "dl_awaitMsg4WithoutHiMsgTimerExpiry" in line:
            try:
                self.msg4(line=line)
            except Exception as _:
                pass
            return

        try:
            self.debug_string(debug_string_list=self.debug_string_list, line=line)
        except Exception as _:
            pass

    def msg1(self, line):
        """
        001114 10.09 15:14:38.554 [10.71.213.140] cb ASC-1518-Disp_4 <2023-09-10T07:14:38.778268Z> 1E0186-EQ_L2_PS_DL_UP_INF/.uplane/L2-PS/src/pscommon/db/ue/data/CarrierSpecificDataLogic.hpp[150] ue.rnti:28935, subcellIndex:0, Ue betaOffsetConfig Index1:0, index2:0, index3:0
        """
        times = re.findall(self.times_compile, line)
        rnti_re = re.compile(r"ue[.]rnti:(.*?),", re.S)
        rnti = re.search(rnti_re, line).group(1)
        msg_text = color_text(text="msg1: ue.rnti:{}".format(rnti), color="green")

        html = "{} {}".format(times[0], msg_text)

        self.signal_message.emit(html)

    def msg2(self, line):
        """
        002712 09.09 16:36:57.220 [10.71.213.140] 5f ASC-151A-Disp_4 <2023-09-09T08:36:57.452283Z>
        16017F-EQ_L2_PS_DL_UP_ INF/.uplane/L2-PS/src/dl/sch/pre/rach/RaProcedure.cpp[283] handlePrachPreamble:
        xsfn=825.3 prachXsfn=825.1 cRnti=60201 raRnti=15 prachPreambleIndex=57 initialTa=1 timeOccasion=0
        freqOccasion=0 beamId=0 2ndBeamId=0 nrCellIdentity=9371653 subcellIndex=0
        """
        times = re.findall(self.times_compile, line)
        nrCellIdentity_re = re.compile(r"nrCellIdentity=(.*?) ", re.S)
        nrCellIdentity = re.search(nrCellIdentity_re, line).group(1)
        initialTa_re = re.compile(r"initialTa=(.*?) ", re.S)
        initialTa = re.search(initialTa_re, line).group(1)
        cRnti_re = re.compile(r"cRnti=(.*?) ", re.S)
        cRnti = re.search(cRnti_re, line).group(1)

        msg_text = color_text(
            "msg2: nrCellIdentity={},initialTa={},cRnti={}".format(
                nrCellIdentity, initialTa, cRnti
            ),
            "green",
        )

        html = "{} {}".format(times[0], msg_text)

        self.signal_message.emit(html)

    def msg3(self, line):
        times = re.findall(self.times_compile, line)
        tempRnti_re = re.compile(r"tempRnti=(.*?) ", re.S)
        tempRnti = re.search(tempRnti_re, line).group(1)

        msg_text = color_text("msg3: tempRnti={}".format(tempRnti), "green")

        html = "{} {}".format(times[0], msg_text)

        self.signal_message.emit(html)

    def StoreUE(self, line):
        times = re.findall(self.times_compile, line)
        cellId_re = re.compile(r"cellId:(.*?),", re.S)
        cellId = re.search(cellId_re, line).group(1)
        crnti_re = re.compile(r"crnti_:(.*?),", re.S)
        crnti_ = re.search(crnti_re, line).group(1)
        ueIdDu_re = re.compile(r"ueIdDu_:(.*?),", re.S)
        ueIdDu_ = re.search(ueIdDu_re, line).group(1)

        msg_text = color_text(
            "Store UE: cellId:{},crnti_:{},ueIdDu_:{}".format(cellId, crnti_, ueIdDu_),
            "green",
        )

        html = "{} {}".format(times[0], msg_text)

        self.signal_message.emit(html)

    def msg4(self, line):
        times = re.findall(self.times_compile, line)
        rnti_re = re.compile(r"rnti=(.*?) ", re.S)
        rnti = re.search(rnti_re, line).group(1)
        isRrcReject_re = re.compile(r"isRrcReject=(.*?)\n", re.S)
        isRrcReject = re.search(isRrcReject_re, line).group(1)

        msg_text = color_text(
            "msg4: rnti:{},isRrcReject:{}".format(rnti, isRrcReject), "green"
        )

        html = "{} {}".format(times[0], msg_text)

        self.signal_message.emit(html)

    def debug_string(self, debug_string_list: list, line: str):
        l1 = sum([i.split(".*") for i in debug_string_list], [])
        for i in l1:
            if i in line:
                prefix = color_text("debug log:", color="yellow", font_weight="100")
                msg_text = color_text(
                    "{}".format(line.strip().replace("<", "&lt;").replace(">", "&gt;")),
                    color="grey",
                    font_weight="100",
                )
                msg_text = "{} {}".format(prefix, msg_text)
                self.signal_message.emit(msg_text)
