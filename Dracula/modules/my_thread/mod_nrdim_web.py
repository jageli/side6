import threading
from PySide6.QtCore import Signal, QObject
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtCore import QUrl, QFile, QIODevice


class NrDimWeb(threading.Thread, QObject):
    finished = Signal()
    signal_message = Signal(dict)

    def __init__(self, scf_file="SCF.xml"):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        self.scf_file = scf_file
        self.browser = None
        self.javascript = None

    def run(self):
        self.set_browser()
        self.javascript_create()
        self.browser.loadFinished.connect(self.on_load_finished)
        self.show_browser()

    def set_browser(self):
        self.browser = QWebEngineView()
        self.browser.resize(1200, 600)
        self.browser.setWindowTitle("NR Dim")
        self.browser.load(
            QUrl("http://5gtables.eecloud.dynamic.nsn-net.net/?tool=SCFImport")
        )

    def show_browser(self):
        self.browser.show()

    def on_load_finished(self):
        page = self.browser.page()
        page.runJavaScript(self.javascript)

    def _open_scf_file(self) -> str:
        file = QFile(self.scf_file)
        file.open(QIODevice.ReadOnly)
        scf_content = file.readAll().data().decode("utf-8")
        file.close()
        return scf_content

    def javascript_create(self) -> str:
        scf_content = self._open_scf_file()
        self.javascript = f"""
function hide_div() {{
    var element = document.getElementById('NRDim_Header');
    element.style.display = 'none';
}}

function upload() {{
    var SCFFullFile = SCFImport_GUI_uploadFile();
    SCFImport_updateInfo(SCFFullFile);
}}

function sleep(milliseconds) {{
    return new Promise((resolve) => {{
    setTimeout(resolve, milliseconds);
  }});
}}

function simulateChooseFile() {{
    const content = `{scf_content}`; 
	const file = new File([content], "SCF.xml",{{type: 'text/xml'}});
	const fileList = new DataTransfer();
	fileList.items.add(file);
	const input = document.getElementById('SCFFile');
	input.files = fileList.files;
	console.log(input.files[0]);
}}

async function start_run() {{
  console.log('Start');
  hide_div();
  await sleep(6000);
  simulateChooseFile();
  //await sleep(2000);
  console.log('End');
  upload();
}}

start_run();
"""
