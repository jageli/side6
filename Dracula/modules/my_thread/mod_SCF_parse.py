# -*- coding:utf-8  -*-
import copy
import re
from lxml import etree
from collections import OrderedDict


class ParseScf:
    def __init__(self, xml_file=None):
        self.xml_tree = etree.parse(
            xml_file, parser=etree.XMLParser(remove_blank_text=True)
        )
        self.root = self.xml_tree.getroot()
        self.ns = self.root.nsmap
        self.ns_xpath = {"ns": self.ns[None]}
        self.ns_str = self.ns[None]
        self.nrbts = re.compile(r"NRBTS-(.*?)/", re.S)
        self.NRCELLGRP = re.compile(r"NRCELLGRP-(.*)", re.S)

    def nrcellgroup_nrcell_parameters(self) -> dict:
        """_summary_

        Returns:
            dict: _description_
            {"MRBTS-xxx/NRBTS-xxx/NRCELLGRP-x":{"MRBTS-xxx/NRBTS-xxx/NRCELL-x":{"***":"***","***":"***","***":[{"***":"***"},[{"***":"***"}]}}}
        """
        temp_dict = OrderedDict()
        nrcellgroup = self.get_nrcellgroup()
        for k, v in nrcellgroup.items():
            xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELL" and contains(@distName,"NRCELL-{}")]'.format(
                v.get("nrCellList")
            )
            res = self._get_all_parameters(xpath=xpath)
            # temp_dict.update({"cellgroup_distname": k}, **res)
            temp_dict.update({k: res})
        return temp_dict

    def mapping_nrcell_nrcellgroup(self) -> list:
        """_summary_

        Returns:
            list: _description_
            [{'cell_distname': 'MRBTS-xxx/NRBTS-xxx/NRCELL-x', 'cellgroup_distname': 'MRBTS-xxx/NRBTS-xxx/NRCELLGRP-x'}, ...]
        """
        return_list = []
        nrcellgroup = self.get_nrcellgroup()
        for k, v in nrcellgroup.items():
            temp_dict = OrderedDict()
            xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELL" and contains(@distName,"NRCELL-{}")]'.format(
                v.get("nrCellList")
            )

            res = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)

            temp_dict.update(
                {"cell_distname": res[0].get("distName"), "cellgroup_distname": k}
            )

            return_list.append(temp_dict)
        return return_list

    def get_nrcellgroup(self) -> dict:
        """_summary_

        Returns:
            dict: _description_
            {"MRBTS-xxx/NRBTS-xxx/NRCELL-1":{...}, "MRBTS-xxx/NRBTS-xxx/NRCELL-2":{...}, ... ...}
        """
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELLGRP"]'
        res = self._get_all_parameters(xpath=xpath)
        return res

    def get_nrcell(self) -> dict:
        """_summary_

        Returns:
            dict: _description_
            {"MRBTS-xxx/NRBTS-xxx/NRCELL-1":{...}, "MRBTS-xxx/NRBTS-xxx/NRCELL-2":{...}, ... ...}
        """
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELL"]'
        res = self._get_all_parameters(xpath=xpath)
        return res

    def get_nrbts_mode(self) -> dict:
        """_summary_

        Returns:
            dict: _description_
            {"MRBTS-xxx/NRBTS-xxx":{...}, ... ...}
        """
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRBTS"]'
        res = self._get_all_parameters(xpath=xpath)
        return res

    def _get_all_parameters(self, xpath) -> dict:
        """_summary_
        <p name="***">***</p>

        <list name="***">
            <p>***</p>
        </list>

        <list name="***">
            <item>
                <p name="***">***</p>
                <p name="***">***</p>
            </item>
        </list>

        Returns:
            dict: _description_
            {"$distName":{"***":"***","***":"***","***":[{"***":"***"},[{"***":"***"}]}}
        """

        return_dict = OrderedDict()

        res = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)

        for main_node in res:
            disname = main_node.attrib["distName"]
            temp_dict = OrderedDict()

            res: dict = self._get_p_name(node=main_node)
            temp_dict.update(res)

            res: dict = self._get_list_name(node=main_node)
            temp_dict.update(res)

            return_dict.update({disname: temp_dict})

        return return_dict

    def _get_list_name(self, node, xpath="ns:list") -> dict:
        """_summary_

        <list name="***">
            <p>***</p>
        </list>

        <list name="***">
            <item>
                <p name="***">***</p>
                <p name="***">***</p>
            </item>
        </list>

        Args:
            node (_type_): _description_
            xpath (str, optional): _description_. Defaults to "ns:list".

        Returns:
            dict: _description_
            {"***":"***","***":[{"***":"***"},[{"***":"***"}]}
        """
        temp_dict = OrderedDict()
        res: list = node.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        for node in res:
            children_node = node.getchildren()[0]
            if children_node.tag == "{{{0}}}p".format(self.ns_str):
                res: dict = self._get_p(node=node)
                temp_dict.update({node.get("name"): res})
            elif children_node.tag == "{{{0}}}item".format(self.ns_str):
                res: dict = self._get_p_name(node=children_node)
                temp_dict.update({node.get("name"): res})
        return temp_dict

    def _get_p_name(self, node, xpath="ns:p") -> dict:
        """_summary_
        <p name="***">***</p>

        Args:
            node (_type_): _description_
            xpath (str, optional): _description_. Defaults to "ns:p".

        Returns:
            dict: _description_
            {"***":"***"}
        """
        temp_dict = OrderedDict()
        res = node.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        for i in res:
            temp_dict.update({i.get("name"): i.text})
        return temp_dict

    def _get_p(self, node, xpath="ns:p") -> str:
        """_summary_
        <list name="***">
            <p>***</p>
        </list>

        Args:
            node (_type_): _description_
            xpath (str, optional): _description_. Defaults to "ns:p".

        Returns:
            str: _description_
            ***
        """
        res = node.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        if res:
            return res[0].text


class ParseSCF(object):
    def __init__(self, xml_file=None):
        self.xml_tree = etree.parse(
            xml_file, parser=etree.XMLParser(remove_blank_text=True)
        )
        self.root = self.xml_tree.getroot()
        self.ns = self.root.nsmap

        self.ns_xpath = {"ns": self.ns[None]}

        self.nrbts = re.compile(r"NRBTS-(.*?)/", re.S)
        self.NRCELLGRP = re.compile(r"NRCELLGRP-(.*)", re.S)

    def get_version(self):
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts:MRBTS"]'
        mod = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        version = mod[0].attrib["version"]
        return version

    def update_SCF(self, *args):
        lcrId = args[0]
        NRCELL_p = args[1]
        NRCELL_list_item_p = args[2]
        gNbCuType = args[3]

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELL"]/ns:p[@name="lcrId" and text()="{}"]'.format(
            lcrId
        )
        lcrId_mod = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        lcrId_mod_parent = lcrId_mod[0].getparent()
        # print(NRCELL_p)
        for k, v in NRCELL_p.items():
            xpath_child = f'ns:p[@name="{k}"]'
            child = lcrId_mod_parent.xpath(
                "{}".format(xpath_child), namespaces=self.ns_xpath
            )
            if len(child):
                child[0].text = v

        for k, v in NRCELL_list_item_p.items():
            xpath_tddFrameStructure = f'ns:list[@name="tddFrameStructure"]/ns:item'
            tddFrameStructure_root = lcrId_mod_parent.xpath(
                "{}".format(xpath_tddFrameStructure), namespaces=self.ns_xpath
            )[0]
            xpath_child = (
                f'ns:list[@name="tddFrameStructure"]/ns:item/ns:p[@name="{k}"]'
            )
            child = lcrId_mod_parent.xpath(
                "{}".format(xpath_child), namespaces=self.ns_xpath
            )
            if len(child):
                sub_root = child[0].getparent()
                # print(k,v)
                sub_root.remove(child[0])
            if v:
                # child[0].text = v
                # sub_root.remove(child[0])

                child1 = etree.Element("p")
                child1.set("name", k)
                child1.text = v
                tddFrameStructure_root.append(child1)
            # else:
            # child[0].getparent().remove(child[0])
            # print("ok")

    def _get_NRBTS_version(self):
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRBTS"]'
        NRBTS_version = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        return {"NRBTS_version": NRBTS_version[0].attrib["version"]}

    def _get_cpri_ecpri(self, xpath_NRCELL):
        # 定位NRCELL里的rModDNList
        xpath = '{}/ns:list[@name="rModDNList"]/ns:p'.format(xpath_NRCELL)
        et = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)[0].text

        # 定位CABLINK
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.eqm:CABLINK"]/ns:p[@name="secondEndpointDN" and text()="{}"]'.format(
            et
        )
        # 定位secondEndpointDN
        secondEndpointDN = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)

        # 通过secondEndpointDN定位linkSpeed（兄弟节点）
        linkSpeed = (
            secondEndpointDN[0]
            .xpath(
                './preceding-sibling::ns:p[@name="linkSpeed"]', namespaces=self.ns_xpath
            )[0]
            .text
        )

        return {"linkSpeed": linkSpeed}

    def NRCELLGRP_NRCELL_mapping(self, *args):
        NRCELL_p = args[0]
        NRCELL_list_item_p = args[1]
        NRCELLGRP_p = args[2]
        NRBTS_p = args[3]
        Other_parameter = args[4]

        NRCELLGRP_NRCELL_mapping_dict = OrderedDict()

        # NRBTS节点
        tmp_NRBTS = OrderedDict()

        for i in NRBTS_p:
            xpath = 'cmData/managedObject[@class="com.nokia.srbts.nrbts:NRBTS"]'
            NRBTS_parameter = self.root.find(
                '{}/p[@name="{}"]'.format(xpath, i), namespaces=self.ns
            )
            # print(NRBTS_parameter.text)
            tmp_NRBTS[i] = NRBTS_parameter.text

        # 通过xpath定位 NRCELLGRP节点，可能会有多个，需要遍历。目前一个cell对应一个NRCELLGRP
        xpath = 'cmData/managedObject[@class="com.nokia.srbts.nrbts:NRCELLGRP"]'
        NRCELLGRP_list = self.root.findall("{}".format(xpath), namespaces=self.ns)
        for i in NRCELLGRP_list:
            # 定位NRCELLGRP disName节点
            NRCELLGRP_distName = i.attrib["distName"]

            # 用通配符查找NRBTS字段
            NRBTS = re.search(self.nrbts, NRCELLGRP_distName).group(1)

            # 此处可以预留定位NRCELLGRP_p
            #
            #
            #

            # 定位NRCELLGRP_list_nrCellList_p节点,找到NRCELLGRP对应的cellid(目前是1对1),写成NRCELL_list是为了兼容以后的1对多
            xpath_nrCellList = 'cmData/managedObject[@distName="{}"]/list[@name="nrCellList"]/p'.format(
                NRCELLGRP_distName
            )
            NRCELL_list = self.root.findall(
                "{}".format(xpath_nrCellList), namespaces=self.ns
            )

            # 在每个NRCELLGRP里遍历cell_id，目前一个NRCELLGRP里只有一个cell_id,这样写可以兼容以后的多cell的情况
            for j in NRCELL_list:
                cell_id = j.text

                # 通过NRCELL节点和NRCELLGRP里的cell_id定位NRCELL的mod,xpath中是唯一的结果，
                xpath_NRCELL = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELL" and contains(@distName,"NRCELL-{}")]'.format(
                    cell_id
                )
                NRCELL_xpath = self.root.xpath(
                    "{}".format(xpath_NRCELL), namespaces=self.ns_xpath
                )

                # 定位到NRCELL节点
                NRCELL_MOD = NRCELL_xpath[0]
                NRCELL_distName = NRCELL_MOD.attrib["distName"]

                # 把NRCELLGRP_distName和NRCELL_distName组成元组作为字典的key字，value是一个列表和字典的嵌套，返回整个字典，由于导入NRDIM工具中
                key_x = (NRCELLGRP_distName, NRCELL_distName)
                tmp = OrderedDict()
                NRCELLGRP_NRCELL_mapping_dict[key_x] = tmp

                # 把NRBTS放入列表中
                # NRCELLGRP_NRCELL_mapping_dict[key_x].append({"NRBTS": NRBTS})
                tmp["NRBTS"] = NRBTS

                # 根据传入的参数NRCELL_p，查找NRCELL下p标签的所有属性为name的值，默认name是唯一的，用NRCELL_p_name_value[0]表示
                for NRCELL_p_name in NRCELL_p:
                    xpath_NRCELL_p_name = '{}/ns:p[@name="{}"]'.format(
                        xpath_NRCELL, NRCELL_p_name
                    )
                    NRCELL_p_name_value = self.root.xpath(
                        "{}".format(xpath_NRCELL_p_name), namespaces=self.ns_xpath
                    )
                    # NRCELLGRP_NRCELL_mapping_dict[key_x].append({NRCELL_p_name: NRCELL_p_name_value[0].text})
                    if len(NRCELL_p_name_value):
                        tmp[NRCELL_p_name] = NRCELL_p_name_value[0].text
                    else:
                        tmp[NRCELL_p_name] = ""
                # 根据传入的参数NRCELL_list_item_p，查找相关节点
                for NRCELL_list_item_p_name in NRCELL_list_item_p:
                    NRCELL_list_item_p_vlue = NRCELL_MOD.xpath(
                        './ns:list/ns:item/ns:p[@name="{}"]'.format(
                            NRCELL_list_item_p_name
                        ),
                        namespaces=self.ns_xpath,
                    )
                    if len(NRCELL_list_item_p_vlue) > 0:
                        tmp[NRCELL_list_item_p_name] = NRCELL_list_item_p_vlue[0].text

                # 判断cpri和ecpri
                linkSpeed = self._get_cpri_ecpri(xpath_NRCELL)
                NRCELLGRP_NRCELL_mapping_dict[key_x].update(linkSpeed)

                # 添加BTS_version信息
                NRBTS_version = self._get_NRBTS_version()
                tmp_NRBTS.update(NRBTS_version)

                # 把NEBTS参数和NRCELL参数合并
                # NRCELLGRP_NRCELL_mapping_dict[key_x].update(tmp_NRBTS)
                NRCELLGRP_NRCELL_mapping_dict[key_x] = {
                    **NRCELLGRP_NRCELL_mapping_dict[key_x],
                    **tmp_NRBTS,
                }

        return NRCELLGRP_NRCELL_mapping_dict

    def del_duplicates(self):
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:BWP_PROFILE"]'
        self.del_mod3(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDCCH"]'
        self.del_mod3(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDCCH_CONFIG_COMMON"]'
        self.del_mod3(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDCCH_CONFIG_DEDICATED"]'
        self.del_mod3(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDSCH"]'
        self.del_mod3(xpath)

    def del_BWP_PROFILE(self):
        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:BWP_PROFILE"]'
        self.del_mod(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDCCH"]'
        self.del_mod(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDCCH_CONFIG_COMMON"]'
        self.del_mod(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDCCH_CONFIG_DEDICATED"]'
        self.del_mod(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:PDSCH"]'
        self.del_mod(xpath)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRCELL"]/ns:list[@name="cellBwpList"]'
        self.del_mod(xpath)

    def add_BWP_PROFILE(self, tree_list, gnb_type_dict, NRCELLGRP_NRCELL_mapping_dict):
        for k, v in gnb_type_dict.items():
            dis_name = "/".join(k[1].split("/")[:-1])
        # print(dis_name)

        xpath = 'ns:cmData/ns:managedObject[@class="com.nokia.srbts.nrbts:NRBTS"]'
        NRBTS = self.root.xpath("{}".format(xpath), namespaces=self.ns_xpath)

        for v in NRCELLGRP_NRCELL_mapping_dict.values():
            version = v["NRBTS_version"]

        # print(NRBTS)
        for x in reversed(tree_list):
            # for x in tree_list:
            root = x.getroot()
            root.attrib["distName"] = "/".join(
                list([dis_name, root.attrib["distName"]])
            )
            root.attrib["version"] = version
            self.insert_after(NRBTS[-1], x.getroot())

    def add_cellBwpList(
        self, bwp_index_dict, NRCELLGRP_NRCELL_mapping_dict, gnb_type_dict
    ):
        # print(bwp_index_dict)
        # print(NRCELLGRP_NRCELL_mapping_dict)
        # print(gnb_type_dict)

        for k, v in gnb_type_dict.items():
            NRCELLGRP_NRCELL_mapping_dict[k]["gnb_type"] = v

        # print(NRCELLGRP_NRCELL_mapping_dict)
        for k, v in NRCELLGRP_NRCELL_mapping_dict.items():
            dis_name_temp = "/".join(k[1].split("/")[:-1])
            # print(dis_name_temp)
            cell_disname = k[1]
            gnb_type = v["gnb_type"]
            for k1, v1 in bwp_index_dict.items():
                if k1 == gnb_type:
                    # print(cell_disname)
                    # print(v1)
                    my_root = self.__cellBwpList_mod(v1, dis_name_temp)
                    # 找到beamSet，cellBwpList就在beamSet下一个节点(不是官方）
                    xpath = 'ns:cmData/ns:managedObject[@distName="{}"]/ns:list[@name="beamSet"]'.format(
                        cell_disname
                    )
                    cell_mod = self.root.xpath(
                        "{}".format(xpath), namespaces=self.ns_xpath
                    )
                    # print(cell_mod)
                    self.insert_after(cell_mod[0], my_root)

    def __cellBwpList_mod(self, bwp_index_list, dis_name_temp):
        root = etree.Element("list")
        root.set("name", "cellBwpList")

        for i, bwp_index in enumerate(bwp_index_list):
            # print(bwp_index)
            child1 = etree.SubElement(root, "item")
            child2 = etree.SubElement(child1, "p")
            child2.set("name", "bwpDN")
            child2.text = f"{dis_name_temp}/BWP_PROFILE-{str(bwp_index)}"
            child3 = etree.SubElement(child1, "p")
            child3.set("name", "cellBwpId")
            child3.text = str(i)
        # tree = etree.ElementTree(root)
        # print(etree.tostring(root, pretty_print=True))
        return root

    def del_mod(self, xpath):
        BWP_PROFILE_mod_list = self.root.xpath(
            "{}".format(xpath), namespaces=self.ns_xpath
        )
        for BWP_PROFILE_mod in BWP_PROFILE_mod_list:
            BWP_PROFILE_mod.getparent().remove(BWP_PROFILE_mod)

    def del_mod2(self, root, xpath):
        BWP_PROFILE_mod_list = root.xpath("{}".format(xpath), namespaces=self.ns_xpath)
        for BWP_PROFILE_mod in BWP_PROFILE_mod_list:
            BWP_PROFILE_mod.getparent().remove(BWP_PROFILE_mod)

    def del_mod3(self, xpath):
        BWP_PROFILE_mod_list = self.root.xpath(
            "{}".format(xpath), namespaces=self.ns_xpath
        )
        # print(BWP_PROFILE_mod_list)
        for BWP_PROFILE_mod in BWP_PROFILE_mod_list:
            distName = BWP_PROFILE_mod.attrib["distName"]
            xpath2 = f'ns:cmData/ns:managedObject[@distName="{distName}"]'
            mod = self.root.xpath("{}".format(xpath2), namespaces=self.ns_xpath)
            mod.pop(0)
            # print(mod)
            if len(mod):
                for sub_mod in mod:
                    sub_mod.getparent().remove(sub_mod)

    def insert_after(self, element, new_element):
        parent = element.getparent()
        parent.insert(parent.index(element) + 1, new_element)

    def addData(self):
        NRCELL_mod = self.root.findall(
            'cmData/managedObject[@class="com.nokia.srbts.nrbts:NRCELL"]',
            namespaces=self.ns,
        )
        copied_managedObject = copy.deepcopy(NRCELL_mod[0])
        NRCELL_mod[0].addnext(copied_managedObject)

    def copy_MOD(self, xpath):
        mod = self.root.findall(xpath, namespaces=self.ns)
        copied_managedObject = copy.deepcopy(mod[0])
        return copied_managedObject

    def deleteData(self, xpath, **kwargs):
        rowData = self.root.xpath(xpath, **kwargs)
        if not len(rowData) == 1:
            return False
        rowData[0].getparent().remove(rowData[0])

    def save_xml_file(self, new_golden_scf_file):
        result = etree.tostring(
            self.xml_tree,
            xml_declaration=True,
            encoding="UTF-8",
            standalone="yes",
            pretty_print=True,
        )
        with open(new_golden_scf_file, "wt") as fp:
            fp.write(result.decode("utf-8"))
