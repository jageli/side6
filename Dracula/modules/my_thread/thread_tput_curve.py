import threading
from PySide6.QtCore import QObject, Signal
import re
import datetime

from modules.my_global_dict import MY_GLOBAL_DICT


class Tput_curve_Thread(threading.Thread, QObject):
    """
    Tput curve
    """

    _signal = Signal()
    _signal_tput_list = Signal(list)

    def __init__(self):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        self.list = []

        # 正则表达式
        self.times_compile = re.compile(r"[<](.*?)[>]", re.S)
        self.nrCellId_compile_UL = re.compile(r"nrCellIdentity (\d+)", re.S)
        self.ul_tp_compile_UL = re.compile(r"[(](.*?) rlcPduBytes", re.S)
        self.nrCellId_compile_DL = re.compile(r"nrCellId (.*?)\[TBS", re.S)
        self.dl_tp_compile_DL = re.compile(r"DataPdu: (.*?)\]", re.S)

    def run(self):
        if not MY_GLOBAL_DICT.get("Tputlog_FileNames"):
            return

        for Tputlog_FileName in MY_GLOBAL_DICT.get("Tputlog_FileNames"):
            with open(Tputlog_FileName, encoding="utf-8") as f:
                for line in f:
                    if "L2_LO_UL" and "rlcPduBytes" in line:
                        # self.ul_tp(line)
                        try:
                            self.ul_tp(line)
                        except:
                            pass
                    if "L2_LO_TB_BU" and "DataPdu" in line:
                        # self.dl_tp(line)
                        try:
                            self.dl_tp(line)
                        except:
                            pass
        # print(self.list)
        # for i in self.list:
        #     print(i)
        self._signal_tput_list.emit(self.list)

    def ul_tp(self, line):
        # tp_list = line.split(',')
        # nrCellId = tp_list[3].strip().split(' ')[1]
        # nrCellId = re.compile(r'nrCellIdentity (.*?)\n', re.S)
        nrCellId = re.findall(self.nrCellId_compile_UL, line)[-1].strip()

        # times = re.compile(r'[<](.*?)[>]', re.S)
        times = re.findall(self.times_compile, line)

        # ul_tp = re.compile(r'[(](.*?) rlcPduBytes', re.S)
        ul_tp = re.search(self.ul_tp_compile_UL, line).group(1)
        # ul_tp_M1 = str(round(int(ul_tp) * 8 / 1000 / 1000, 1))
        ul_tp_M = round(int(ul_tp) * 8 / 1024 / 1000, 1)

        # ul_tp = re.compile(r'[(](.*?)[)]', re.S)
        # ul_tp = (re.findall(ul_tp, line))[0].split(' ', 2)[0]
        # ul_tp_M1 = str(round(int(ul_tp) * 8 / 1024 / 1000, 1))
        # ul_tp_M = round(int(ul_tp) * 8 / 1024 / 1000, 1)

        # 时间格式转化
        UTC_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
        utc_time = datetime.datetime.strptime(times[0], UTC_FORMAT)
        # local_time = utc_time + datetime.timedelta(hours=0)

        # prt = times[0] + ' nrCellId ' + nrCellIdentity + ' UL_TP: ' + ul_tp + ' (' + ul_tp_M + 'M)'
        # prt = times[0] + ' nrCellId ' + nrCellId + ' UL_TP: ' + ul_tp + ' (' + ul_tp_M1 + 'M)'
        # print(prt)
        self.list.append(["UL", times[0], utc_time, nrCellId, ul_tp_M])

        # print(self.list.append)

    def dl_tp(self, line):
        # tp_list = line.split(' ')

        times = re.findall(self.times_compile, line)

        nrCellId = re.findall(self.nrCellId_compile_DL, line)[-1].strip()

        dl_tp = re.search(self.dl_tp_compile_DL, line).group(1).split("|")[-1]
        dl_tp_M = round(int(dl_tp) * 8 / 1000 / 1000, 1)
        # print(nrCellId,dl_tp,dl_tp_M)

        # if tp_list[7] == 'nrCellId':
        #     nrCellId = tp_list[8].strip()
        #     dl_tp = tp_list[10].strip().split('|')[-1].replace(']', '')
        # else:
        #     nrCellId = tp_list[10].strip().split('[')[0]
        #     dl_tp = tp_list[11].strip().split('|')[-1].replace(']', '')
        #
        # # dl_tp_M = str(round(int(dl_tp) * 8 / 1024 / 1000, 1))
        # dl_tp_M = round(int(dl_tp) * 8 / 1024 / 1000, 1)

        # 时间格式转化
        UTC_FORMAT = "%Y-%m-%dT%H:%M:%S.%fZ"
        utc_time = datetime.datetime.strptime(times[0], UTC_FORMAT)
        # local_time = utc_time + datetime.timedelta(hours=0)

        self.list.append(["DL", times[0], utc_time, nrCellId, dl_tp_M])
        # print(times[0],utc_time,nrCellId,dl_tp_M)

        # print(self.list)
        # prt = times[0] + ' nrCellId ' + nrCellId + ' DL_TP: ' + dl_tp + ' (' + dl_tp_M + 'M)'
        # print(prt)
