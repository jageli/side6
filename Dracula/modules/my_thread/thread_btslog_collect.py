import threading
from PySide6.QtCore import Signal, QObject
from loguru import logger
import paramiko
from modules.my_thread.thread_check_tput import ConnectNodeB
from modules.public_thread import ValidateTestLineConfig, color_text, timeS
from modules.my_global_dict import MY_GLOBAL_DICT


class CollectBtsLogThread(threading.Thread, QObject):
    finished = Signal()
    signal_message = Signal(str)
    signal_light_status = Signal()

    def __init__(self):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        self.btslog = None
        self.btslog_handler_id = None
        self.btslog_mode = None

    def run(self):
        self.prepare_btslog()
        MY_GLOBAL_DICT.update({"signal_light_status": self.signal_light_status})

        site_type = MY_GLOBAL_DICT.get("current_testline").get("site_type")
        self.btslog_mode = MY_GLOBAL_DICT.get("btslog_mode")

        try:
            ValidateTestLineConfig.validate_node_ip()
            ValidateTestLineConfig.validate_node_username()
            ValidateTestLineConfig.validate_node_password()
        except Exception as e:
            self.signal_message.emit(color_text(e))
            self.finished.emit()
            return

        self.conn = ConnectNodeBForBtsLog(
            signal=self.signal_message, btslog=self.btslog
        )

        if not self.conn.login():
            self.finished.emit()
            try:
                self.conn.close_conn()
            except Exception as _:
                pass
            return

        if site_type == "classical":
            self.conn.add_flag_for_classical()
            if self.btslog_mode == "NC mode":
                cmd = "nc -u -w 100 -l 127.0.0.1 51000"
            elif self.btslog_mode == "TCP dump mode":
                cmd = "tcpdump -A -i any port 51000"
        elif site_type == "cloud":
            cmd = 3

        try:
            self.conn.start_collect_btslog(cmd)
        except TimeoutError:
            self.signal_message.emit(color_text("Time out"))
            self.finished.emit()

        self.finished.emit()
        self.conn.close_conn()

    def prepare_btslog(self):
        testline_name = MY_GLOBAL_DICT.get("current_testline").get("testline_name")
        bts_log_name = "logs/btslog/{}_btslog_{}.log".format(testline_name, timeS())
        file_handler_params = {
            "sink": bts_log_name,
            "filter": lambda record: record["extra"]["module"] == "btslog",
            "enqueue": True,
            "format": "[{time}] - {message}",
            "rotation": "50 MB",
            "compression": "zip",
        }
        self.btslog = logger.bind(module="btslog", configure=False)
        self.btslog_handler_id = self.btslog.add(**file_handler_params)

    def close_log(self):
        logger.remove(self.btslog_handler_id)

    def stop(self):
        try:
            self.close_log()
        except Exception as _:
            pass

        try:
            if self.btslog_mode == "NC mode":
                self.conn.recovery_flag_for_classical()
            self.conn.close_conn()

        except Exception as _:
            # self.tool_log.warning(e)
            pass


class ConnectNodeBForBtsLog(ConnectNodeB):
    def __init__(self, signal=None, btslog: logger = None):
        ConnectNodeB.__init__(self)
        self.signal: Signal = signal
        self.signal_light_status: Signal = MY_GLOBAL_DICT.get("signal_light_status")
        self.btslog = btslog
        self.objsftp = None
        self.obj = None
        self.trans = None
        self.node_ip = MY_GLOBAL_DICT.get("current_testline").get("node_ip")
        self.node_username = MY_GLOBAL_DICT.get("current_testline").get("node_username")
        self.node_password = MY_GLOBAL_DICT.get("current_testline").get("node_password")
        self.ns = MY_GLOBAL_DICT.get("current_testline").get("Namespace")

    def login(self):
        try:
            self.trans = paramiko.Transport((self.node_ip, 22))
            self.trans.connect(username=self.node_username, password=self.node_password)
            self.obj = paramiko.SSHClient()
            self.obj._transport = self.trans
            return True
        except Exception as e:
            MY_GLOBAL_DICT.update({"tput_flag": False})
            self.signal.emit("<p style='color:red;font-weight:bold'>{}</p>".format(e))
            return False

    def close_conn(self):
        self.obj.close()
        self.trans.close()

    def start_collect_btslog(self, cmd, timeout=300):
        _, stdout, _ = self.obj.exec_command(
            cmd, bufsize=-1, get_pty=True, timeout=timeout
        )
        self.signal_light_status.emit()
        for line in stdout:
            line = line.strip().strip(chr(0))
            self.btslog.info("{}".format(line))
