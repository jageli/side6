# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'main2.ui'
##
## Created by: Qt User Interface Compiler version 6.5.3
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QAbstractScrollArea, QApplication, QCheckBox,
    QComboBox, QCommandLinkButton, QFrame, QGridLayout,
    QHBoxLayout, QHeaderView, QLabel, QLayout,
    QLineEdit, QMainWindow, QPlainTextEdit, QPushButton,
    QRadioButton, QScrollArea, QScrollBar, QSizePolicy,
    QSlider, QSpacerItem, QSplitter, QStackedWidget,
    QTableWidget, QTableWidgetItem, QTextBrowser, QVBoxLayout,
    QWidget)
from . resources_rc import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1516, 637)
        MainWindow.setMinimumSize(QSize(940, 560))
        MainWindow.setStyleSheet(u"")
        self.styleSheet = QWidget(MainWindow)
        self.styleSheet.setObjectName(u"styleSheet")
        font = QFont()
        font.setFamilies([u"Segoe UI"])
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        self.styleSheet.setFont(font)
        self.styleSheet.setStyleSheet(u"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"\n"
"SET APP STYLESHEET - FULL STYLES HERE\n"
"DARK THEME - DRACULA COLOR BASED\n"
"\n"
"///////////////////////////////////////////////////////////////////////////////////////////////// */\n"
"\n"
"QWidget {\n"
"    color: rgb(221, 221, 221);\n"
"    font: 10pt \"Segoe UI\";\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Tooltip */\n"
"QToolTip {\n"
"    color: #ffffff;\n"
"    background-color: rgba(33, 37, 43, 180);\n"
"    border: 1px solid rgb(44, 49, 58);\n"
"    background-image: none;\n"
"    background-position: left center;\n"
"    background-repeat: no-repeat;\n"
"    border: none;\n"
"    border-left: 2px solid rgb(255, 121, 198);\n"
"    text-align: left;\n"
"    padding-left: 8px;\n"
"    margin: 0px;\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Bg"
                        " App */\n"
"#bgApp {\n"
"    background-color: rgb(40, 44, 52);\n"
"    border: 1px solid rgb(44, 49, 58);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Left Menu */\n"
"#leftMenuBg {\n"
"    background-color: rgb(33, 37, 43);\n"
"}\n"
"\n"
"#topLogo {\n"
"    background-color: rgb(33, 37, 43);\n"
"    background-image: url(:/images/images/images/PyDracula.png);\n"
"    background-position: centered;\n"
"    background-repeat: no-repeat;\n"
"}\n"
"\n"
"#titleLeftApp {\n"
"    font: 63 12pt \"Segoe UI Semibold\";\n"
"}\n"
"\n"
"#titleLeftDescription {\n"
"    font: 8pt \"Segoe UI\";\n"
"    color: rgb(189, 147, 249);\n"
"}\n"
"\n"
"/* MENUS */\n"
"#topMenu .QPushButton {\n"
"    background-position: left center;\n"
"    background-repeat: no-repeat;\n"
"    border: none;\n"
"    border-left: 22px solid transparent;\n"
"    background-color: transparent;\n"
"    text-align: left;\n"
"    padding-left: 44px;\n"
"}\n"
"\n"
"#topMenu .QPushBu"
                        "tton:hover {\n"
"    background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#topMenu .QPushButton:pressed {\n"
"    background-color: rgb(189, 147, 249);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"#bottomMenu .QPushButton {\n"
"    background-position: left center;\n"
"    background-repeat: no-repeat;\n"
"    border: none;\n"
"    border-left: 20px solid transparent;\n"
"    background-color: transparent;\n"
"    text-align: left;\n"
"    padding-left: 44px;\n"
"}\n"
"\n"
"#bottomMenu .QPushButton:hover {\n"
"    background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#bottomMenu .QPushButton:pressed {\n"
"    background-color: rgb(189, 147, 249);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"#leftMenuFrame {\n"
"    border-top: 3px solid rgb(44, 49, 58);\n"
"}\n"
"\n"
"/* Toggle Button */\n"
"#toggleButton {\n"
"    background-position: left center;\n"
"    background-repeat: no-repeat;\n"
"    border: none;\n"
"    border-left: 20px solid transparent;\n"
"    background-color: rgb(37, 41, 48);\n"
"    text-"
                        "align: left;\n"
"    padding-left: 44px;\n"
"    color: rgb(113, 126, 149);\n"
"}\n"
"\n"
"#toggleButton:hover {\n"
"    background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#toggleButton:pressed {\n"
"    background-color: rgb(189, 147, 249);\n"
"}\n"
"\n"
"/* Title Menu */\n"
"#titleRightInfo {\n"
"    padding-left: 10px;\n"
"}\n"
"\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Extra Tab */\n"
"#extraLeftBox {\n"
"    background-color: rgb(44, 49, 58);\n"
"}\n"
"\n"
"#extraTopBg {\n"
"    background-color: rgb(189, 147, 249)\n"
"}\n"
"\n"
"/* Icon */\n"
"#extraIcon {\n"
"    background-position: center;\n"
"    background-repeat: no-repeat;\n"
"    background-image: url(:/icons/images/icons/icon_settings.png);\n"
"}\n"
"\n"
"/* Label */\n"
"#extraLabel {\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"/* Btn Close */\n"
"#extraCloseColumnBtn {\n"
"    background-color: rgba(255, 255, 255, 0);\n"
"    border: none;\n"
"    border-radius: 5px;\n"
""
                        "}\n"
"\n"
"#extraCloseColumnBtn:hover {\n"
"    background-color: rgb(196, 161, 249);\n"
"    border-style: solid;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"#extraCloseColumnBtn:pressed {\n"
"    background-color: rgb(180, 141, 238);\n"
"    border-style: solid;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"/* Extra Content */\n"
"#extraContent {\n"
"    border-top: 3px solid rgb(40, 44, 52);\n"
"}\n"
"\n"
"/* Extra Top Menus */\n"
"/*\n"
"#extraTopMenu .QPushButton {\n"
"	background-position: left center;\n"
"    background-repeat: no-repeat;\n"
"	border: none;\n"
"	border-left: 22px solid transparent;\n"
"	background-color:transparent;\n"
"	text-align: left;\n"
"	padding-left: 44px;\n"
"}\n"
"*/\n"
"\n"
"#extraTopMenu .QPushButton {\n"
"    background-color: rgb(52, 59, 72);\n"
"}\n"
"\n"
"#extraTopMenu .QPushButton:hover {\n"
"    background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#extraTopMenu .QPushButton:pressed {\n"
"    background-color: rgb(189, 147, 249);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
""
                        "#extraTopMenu .QLabel {\n"
"    color: rgb(143, 143, 143);\n"
"    ;\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Content App */\n"
"#contentTopBg {\n"
"    background-color: rgb(33, 37, 43);\n"
"}\n"
"\n"
"#contentBottom {\n"
"    border-top: 3px solid rgb(44, 49, 58);\n"
"}\n"
"\n"
"/* Top Buttons */\n"
"#rightButtons .QPushButton {\n"
"    background-color: rgba(255, 255, 255, 0);\n"
"    border: none;\n"
"    border-radius: 5px;\n"
"}\n"
"\n"
"#rightButtons .QPushButton:hover {\n"
"    background-color: rgb(44, 49, 57);\n"
"    border-style: solid;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"#rightButtons .QPushButton:pressed {\n"
"    background-color: rgb(23, 26, 30);\n"
"    border-style: solid;\n"
"    border-radius: 4px;\n"
"}\n"
"\n"
"/* Theme Settings */\n"
"#extraRightBox {\n"
"    background-color: rgb(44, 49, 58);\n"
"}\n"
"\n"
"#themeSettingsTopDetail {\n"
"    background-color: rgb(189, 147, 249);\n"
"}\n"
"\n"
"/* Bottom"
                        " Bar */\n"
"#bottomBar {\n"
"    background-color: rgb(44, 49, 58);\n"
"}\n"
"\n"
"#bottomBar QLabel {\n"
"    font-size: 11px;\n"
"    color: rgb(113, 126, 149);\n"
"    padding-left: 10px;\n"
"    padding-right: 10px;\n"
"    padding-bottom: 2px;\n"
"}\n"
"\n"
"/* CONTENT SETTINGS */\n"
"/* MENUS */\n"
"#contentSettings .QPushButton {\n"
"    background-position: left center;\n"
"    background-repeat: no-repeat;\n"
"    border: none;\n"
"    border-left: 22px solid transparent;\n"
"    background-color: transparent;\n"
"    text-align: left;\n"
"    padding-left: 44px;\n"
"}\n"
"\n"
"#contentSettings .QPushButton:hover {\n"
"    background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#contentSettings .QPushButton:pressed {\n"
"    background-color: rgb(189, 147, 249);\n"
"    color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"QTableWidget */\n"
"QTableWidget {\n"
"    background-color: transparent;\n"
"    padding: 10p"
                        "x;\n"
"    border-radius: 5px;\n"
"    gridline-color: rgb(44, 49, 58);\n"
"    border-bottom: 1px solid rgb(44, 49, 60);\n"
"}\n"
"\n"
"QTableWidget::item {\n"
"    border-color: rgb(44, 49, 60);\n"
"    padding-left: 5px;\n"
"    padding-right: 5px;\n"
"    gridline-color: rgb(44, 49, 60);\n"
"}\n"
"\n"
"QTableWidget::item:selected {\n"
"    background-color: rgb(189, 147, 249);\n"
"}\n"
"\n"
"QHeaderView::section {\n"
"    background-color: rgb(33, 37, 43);\n"
"    max-width: 30px;\n"
"    border: 1px solid rgb(44, 49, 58);\n"
"    border-style: none;\n"
"    border-bottom: 1px solid rgb(44, 49, 60);\n"
"    border-right: 1px solid rgb(44, 49, 60);\n"
"}\n"
"\n"
"QTableWidget::horizontalHeader {\n"
"    background-color: rgb(33, 37, 43);\n"
"}\n"
"\n"
"QHeaderView::section:horizontal {\n"
"    border: 1px solid rgb(33, 37, 43);\n"
"    background-color: rgb(33, 37, 43);\n"
"    padding: 3px;\n"
"    border-top-left-radius: 7px;\n"
"    border-top-right-radius: 7px;\n"
"}\n"
"\n"
"QHeaderView::section:vertic"
                        "al {\n"
"    border: 1px solid rgb(44, 49, 60);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"LineEdit */\n"
"QLineEdit {\n"
"    background-color: rgb(33, 37, 43);\n"
"    border-radius: 5px;\n"
"    border: 2px solid rgb(33, 37, 43);\n"
"    padding-left: 10px;\n"
"    selection-color: rgb(255, 255, 255);\n"
"    selection-background-color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"QLineEdit:hover {\n"
"    border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"\n"
"QLineEdit:focus {\n"
"    border: 2px solid rgb(91, 101, 124);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"PlainTextEdit */\n"
"QPlainTextEdit {\n"
"    background-color: rgb(27, 29, 35);\n"
"    border-radius: 5px;\n"
"    padding: 10px;\n"
"    selection-color: rgb(255, 255, 255);\n"
"    selection-background-color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"QPlainTextEdit QScrollBar:vertical {\n"
"    width: 8px;\n"
"}\n"
""
                        "\n"
"QPlainTextEdit QScrollBar:horizontal {\n"
"    height: 8px;\n"
"}\n"
"\n"
"QPlainTextEdit:hover {\n"
"    border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"\n"
"QPlainTextEdit:focus {\n"
"    border: 2px solid rgb(91, 101, 124);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"TextBrowser */\n"
"QTextBrowser {\n"
"    background-color: rgb(27, 29, 35);\n"
"    border-radius: 5px;\n"
"    padding: 10px;\n"
"    selection-color: rgb(255, 255, 255);\n"
"    selection-background-color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"QTextBrowser QScrollBar:vertical {\n"
"    width: 8px;\n"
"}\n"
"\n"
"QTextBrowser QScrollBar:horizontal {\n"
"    height: 8px;\n"
"}\n"
"\n"
"QTextBrowser:hover {\n"
"    border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"\n"
"QTextBrowser:focus {\n"
"    border: 2px solid rgb(91, 101, 124);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"ScrollBars */\n"
"QScrol"
                        "lBar:horizontal {\n"
"    border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    height: 8px;\n"
"    margin: 0px 21px 0 21px;\n"
"    border-radius: 0px;\n"
"}\n"
"\n"
"QScrollBar::handle:horizontal {\n"
"    background: rgb(189, 147, 249);\n"
"    min-width: 25px;\n"
"    border-radius: 4px\n"
"}\n"
"\n"
"QScrollBar::add-line:horizontal {\n"
"    border: none;\n"
"    background: rgb(55, 63, 77);\n"
"    width: 20px;\n"
"    border-top-right-radius: 4px;\n"
"    border-bottom-right-radius: 4px;\n"
"    subcontrol-position: right;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:horizontal {\n"
"    border: none;\n"
"    background: rgb(55, 63, 77);\n"
"    width: 20px;\n"
"    border-top-left-radius: 4px;\n"
"    border-bottom-left-radius: 4px;\n"
"    subcontrol-position: left;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:horizontal,\n"
"QScrollBar::down-arrow:horizontal {\n"
"    background: none;\n"
"}\n"
"\n"
"QScrollBar::add-page:horizontal,\n"
"QScrol"
                        "lBar::sub-page:horizontal {\n"
"    background: none;\n"
"}\n"
"\n"
"QScrollBar:vertical {\n"
"    border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    width: 8px;\n"
"    margin: 21px 0 21px 0;\n"
"    border-radius: 0px;\n"
"}\n"
"\n"
"QScrollBar::handle:vertical {\n"
"    background: rgb(189, 147, 249);\n"
"    min-height: 25px;\n"
"    border-radius: 4px\n"
"}\n"
"\n"
"QScrollBar::add-line:vertical {\n"
"    border: none;\n"
"    background: rgb(55, 63, 77);\n"
"    height: 20px;\n"
"    border-bottom-left-radius: 4px;\n"
"    border-bottom-right-radius: 4px;\n"
"    subcontrol-position: bottom;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::sub-line:vertical {\n"
"    border: none;\n"
"    background: rgb(55, 63, 77);\n"
"    height: 20px;\n"
"    border-top-left-radius: 4px;\n"
"    border-top-right-radius: 4px;\n"
"    subcontrol-position: top;\n"
"    subcontrol-origin: margin;\n"
"}\n"
"\n"
"QScrollBar::up-arrow:vertical,\n"
"QScrollBar::down-arrow:vertical {\n"
"    background: n"
                        "one;\n"
"}\n"
"\n"
"QScrollBar::add-page:vertical,\n"
"QScrollBar::sub-page:vertical {\n"
"    background: none;\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"CheckBox */\n"
"QCheckBox::indicator {\n"
"    border: 3px solid rgb(52, 59, 72);\n"
"    width: 15px;\n"
"    height: 15px;\n"
"    border-radius: 10px;\n"
"    background: rgb(44, 49, 60);\n"
"}\n"
"\n"
"QCheckBox::indicator:hover {\n"
"    border: 3px solid rgb(58, 66, 81);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"    background: 3px solid rgb(52, 59, 72);\n"
"    border: 3px solid rgb(52, 59, 72);\n"
"    background-image: url(:/icons/images/icons/cil-check-alt.png);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"RadioButton */\n"
"QRadioButton::indicator {\n"
"    border: 3px solid rgb(52, 59, 72);\n"
"    width: 15px;\n"
"    height: 15px;\n"
"    border-radius: 10px;\n"
"    background: rgb(44,"
                        " 49, 60);\n"
"}\n"
"\n"
"QRadioButton::indicator:hover {\n"
"    border: 3px solid rgb(58, 66, 81);\n"
"}\n"
"\n"
"QRadioButton::indicator:checked {\n"
"    background: 3px solid rgb(94, 106, 130);\n"
"    border: 3px solid rgb(52, 59, 72);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"ComboBox */\n"
"QComboBox {\n"
"    background-color: rgb(27, 29, 35);\n"
"    border-radius: 5px;\n"
"    border: 2px solid rgb(33, 37, 43);\n"
"    padding: 5px;\n"
"    padding-left: 10px;\n"
"}\n"
"\n"
"QComboBox:hover {\n"
"    border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"\n"
"QComboBox::drop-down {\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    width: 25px;\n"
"    border-left-width: 3px;\n"
"    border-left-color: rgba(39, 44, 54, 150);\n"
"    border-left-style: solid;\n"
"    border-top-right-radius: 3px;\n"
"    border-bottom-right-radius: 3px;\n"
"    background-image: url(:/icons/images/icons/cil-arrow-botto"
                        "m.png);\n"
"    background-position: center;\n"
"    background-repeat: no-reperat;\n"
"}\n"
"\n"
"QComboBox QAbstractItemView {\n"
"    color: rgb(255, 121, 198);\n"
"    background-color: rgb(33, 37, 43);\n"
"    padding: 10px;\n"
"    selection-background-color: rgb(39, 44, 54);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Sliders */\n"
"QSlider::groove:horizontal {\n"
"    border-radius: 5px;\n"
"    height: 10px;\n"
"    margin: 0px;\n"
"    background-color: rgb(52, 59, 72);\n"
"}\n"
"\n"
"QSlider::groove:horizontal:hover {\n"
"    background-color: rgb(55, 62, 76);\n"
"}\n"
"\n"
"QSlider::handle:horizontal {\n"
"    background-color: rgb(189, 147, 249);\n"
"    border: none;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    margin: 0px;\n"
"    border-radius: 5px;\n"
"}\n"
"\n"
"QSlider::handle:horizontal:hover {\n"
"    background-color: rgb(195, 155, 255);\n"
"}\n"
"\n"
"QSlider::handle:horizontal:pressed {\n"
"    background-"
                        "color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"QSlider::groove:vertical {\n"
"    border-radius: 5px;\n"
"    width: 10px;\n"
"    margin: 0px;\n"
"    background-color: rgb(52, 59, 72);\n"
"}\n"
"\n"
"QSlider::groove:vertical:hover {\n"
"    background-color: rgb(55, 62, 76);\n"
"}\n"
"\n"
"QSlider::handle:vertical {\n"
"    background-color: rgb(189, 147, 249);\n"
"    border: none;\n"
"    height: 10px;\n"
"    width: 10px;\n"
"    margin: 0px;\n"
"    border-radius: 5px;\n"
"}\n"
"\n"
"QSlider::handle:vertical:hover {\n"
"    background-color: rgb(195, 155, 255);\n"
"}\n"
"\n"
"QSlider::handle:vertical:pressed {\n"
"    background-color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"CommandLinkButton */\n"
"QCommandLinkButton {\n"
"    color: rgb(255, 121, 198);\n"
"    border-radius: 5px;\n"
"    padding: 5px;\n"
"    color: rgb(255, 170, 255);\n"
"}\n"
"\n"
"QCommandLinkButton:hover {\n"
"    color: rgb(255, 170, 255"
                        ");\n"
"    background-color: rgb(44, 49, 60);\n"
"}\n"
"\n"
"QCommandLinkButton:pressed {\n"
"    color: rgb(189, 147, 249);\n"
"    background-color: rgb(52, 58, 71);\n"
"}\n"
"\n"
"\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"Button */\n"
"/* #pagesContainer QPushButton {\n"
"    border: 2px solid rgb(52, 59, 72);\n"
"    border-radius: 5px;\n"
"    background-color: rgb(212, 83, 182);\n"
"}\n"
"\n"
"#pagesContainer QPushButton:hover {\n"
"    background-color: rgb(57, 65, 80);\n"
"    border: 2px solid rgb(61, 70, 86);\n"
"}\n"
"\n"
"#pagesContainer QPushButton:pressed {\n"
"    background-color: rgb(213, 30, 173);\n"
"    border: 2px solid rgb(43, 50, 61);\n"
"}\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"MY ADDS */\n"
"\n"
"/* /////////////////////////////////////////////////////////////////////////////////////////////////\n"
"frame_tput */\n"
"/*\n"
"#frame_tput  .QP"
                        "ushButton {\n"
"    background-color: rgb(60, 60, 60);\n"
"}\n"
"\n"
"#frame_tput .QPushButton:hover {\n"
"    background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#frame_tput .QPushButton:pressed {\n"
"    background-color: rgb(189, 147, 249);\n"
"    color: rgb(255, 255, 255);\n"
"}")
        self.appMargins = QVBoxLayout(self.styleSheet)
        self.appMargins.setSpacing(0)
        self.appMargins.setObjectName(u"appMargins")
        self.appMargins.setContentsMargins(10, 10, 10, 10)
        self.bgApp = QFrame(self.styleSheet)
        self.bgApp.setObjectName(u"bgApp")
        self.bgApp.setStyleSheet(u"")
        self.bgApp.setFrameShape(QFrame.NoFrame)
        self.bgApp.setFrameShadow(QFrame.Raised)
        self.appLayout = QHBoxLayout(self.bgApp)
        self.appLayout.setSpacing(0)
        self.appLayout.setObjectName(u"appLayout")
        self.appLayout.setContentsMargins(0, 0, 0, 0)
        self.leftMenuBg = QFrame(self.bgApp)
        self.leftMenuBg.setObjectName(u"leftMenuBg")
        self.leftMenuBg.setMinimumSize(QSize(60, 0))
        self.leftMenuBg.setMaximumSize(QSize(60, 16777215))
        self.leftMenuBg.setFrameShape(QFrame.NoFrame)
        self.leftMenuBg.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.leftMenuBg)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.topLogoInfo = QFrame(self.leftMenuBg)
        self.topLogoInfo.setObjectName(u"topLogoInfo")
        self.topLogoInfo.setMinimumSize(QSize(0, 50))
        self.topLogoInfo.setMaximumSize(QSize(16777215, 50))
        self.topLogoInfo.setFrameShape(QFrame.NoFrame)
        self.topLogoInfo.setFrameShadow(QFrame.Raised)
        self.topLogo = QFrame(self.topLogoInfo)
        self.topLogo.setObjectName(u"topLogo")
        self.topLogo.setGeometry(QRect(10, 5, 42, 42))
        self.topLogo.setMinimumSize(QSize(42, 42))
        self.topLogo.setMaximumSize(QSize(42, 42))
        self.topLogo.setFrameShape(QFrame.NoFrame)
        self.topLogo.setFrameShadow(QFrame.Raised)
        self.titleLeftApp = QLabel(self.topLogoInfo)
        self.titleLeftApp.setObjectName(u"titleLeftApp")
        self.titleLeftApp.setGeometry(QRect(70, 8, 160, 20))
        font1 = QFont()
        font1.setFamilies([u"Segoe UI Semibold"])
        font1.setPointSize(12)
        font1.setBold(False)
        font1.setItalic(False)
        self.titleLeftApp.setFont(font1)
        self.titleLeftApp.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)
        self.titleLeftDescription = QLabel(self.topLogoInfo)
        self.titleLeftDescription.setObjectName(u"titleLeftDescription")
        self.titleLeftDescription.setGeometry(QRect(70, 27, 160, 16))
        self.titleLeftDescription.setMaximumSize(QSize(16777215, 16))
        font2 = QFont()
        font2.setFamilies([u"Segoe UI"])
        font2.setPointSize(8)
        font2.setBold(False)
        font2.setItalic(False)
        self.titleLeftDescription.setFont(font2)
        self.titleLeftDescription.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignTop)

        self.verticalLayout_3.addWidget(self.topLogoInfo)

        self.leftMenuFrame = QFrame(self.leftMenuBg)
        self.leftMenuFrame.setObjectName(u"leftMenuFrame")
        self.leftMenuFrame.setFrameShape(QFrame.NoFrame)
        self.leftMenuFrame.setFrameShadow(QFrame.Raised)
        self.verticalMenuLayout = QVBoxLayout(self.leftMenuFrame)
        self.verticalMenuLayout.setSpacing(0)
        self.verticalMenuLayout.setObjectName(u"verticalMenuLayout")
        self.verticalMenuLayout.setContentsMargins(0, 0, 0, 0)
        self.toggleBox = QFrame(self.leftMenuFrame)
        self.toggleBox.setObjectName(u"toggleBox")
        self.toggleBox.setMaximumSize(QSize(16777215, 45))
        self.toggleBox.setFrameShape(QFrame.NoFrame)
        self.toggleBox.setFrameShadow(QFrame.Raised)
        self.verticalLayout_4 = QVBoxLayout(self.toggleBox)
        self.verticalLayout_4.setSpacing(0)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.toggleButton = QPushButton(self.toggleBox)
        self.toggleButton.setObjectName(u"toggleButton")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toggleButton.sizePolicy().hasHeightForWidth())
        self.toggleButton.setSizePolicy(sizePolicy)
        self.toggleButton.setMinimumSize(QSize(0, 45))
        self.toggleButton.setFont(font)
        self.toggleButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.toggleButton.setLayoutDirection(Qt.LeftToRight)
        self.toggleButton.setStyleSheet(u"background-image: url(:/icons/images/icons/icon_menu.png);")

        self.verticalLayout_4.addWidget(self.toggleButton)


        self.verticalMenuLayout.addWidget(self.toggleBox)

        self.topMenu = QFrame(self.leftMenuFrame)
        self.topMenu.setObjectName(u"topMenu")
        self.topMenu.setFrameShape(QFrame.NoFrame)
        self.topMenu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_8 = QVBoxLayout(self.topMenu)
        self.verticalLayout_8.setSpacing(0)
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.verticalLayout_8.setContentsMargins(0, 0, 0, 0)
        self.btn_home = QPushButton(self.topMenu)
        self.btn_home.setObjectName(u"btn_home")
        sizePolicy.setHeightForWidth(self.btn_home.sizePolicy().hasHeightForWidth())
        self.btn_home.setSizePolicy(sizePolicy)
        self.btn_home.setMinimumSize(QSize(0, 45))
        self.btn_home.setFont(font)
        self.btn_home.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_home.setLayoutDirection(Qt.LeftToRight)
        self.btn_home.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-home.png);")

        self.verticalLayout_8.addWidget(self.btn_home)

        self.btn_tput = QPushButton(self.topMenu)
        self.btn_tput.setObjectName(u"btn_tput")
        sizePolicy.setHeightForWidth(self.btn_tput.sizePolicy().hasHeightForWidth())
        self.btn_tput.setSizePolicy(sizePolicy)
        self.btn_tput.setMinimumSize(QSize(0, 45))
        self.btn_tput.setFont(font)
        self.btn_tput.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_tput.setLayoutDirection(Qt.LeftToRight)
        self.btn_tput.setStyleSheet(u"background-image: url(:/icons/images/icons/feed.png);")

        self.verticalLayout_8.addWidget(self.btn_tput)

        self.btn_rf = QPushButton(self.topMenu)
        self.btn_rf.setObjectName(u"btn_rf")
        sizePolicy.setHeightForWidth(self.btn_rf.sizePolicy().hasHeightForWidth())
        self.btn_rf.setSizePolicy(sizePolicy)
        self.btn_rf.setMinimumSize(QSize(0, 45))
        self.btn_rf.setFont(font)
        self.btn_rf.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_rf.setLayoutDirection(Qt.LeftToRight)
        self.btn_rf.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-gamepad.png);")

        self.verticalLayout_8.addWidget(self.btn_rf)

        self.btn_save = QPushButton(self.topMenu)
        self.btn_save.setObjectName(u"btn_save")
        sizePolicy.setHeightForWidth(self.btn_save.sizePolicy().hasHeightForWidth())
        self.btn_save.setSizePolicy(sizePolicy)
        self.btn_save.setMinimumSize(QSize(0, 45))
        self.btn_save.setFont(font)
        self.btn_save.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_save.setLayoutDirection(Qt.LeftToRight)
        self.btn_save.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-save.png)")

        self.verticalLayout_8.addWidget(self.btn_save)

        self.btn_exit = QPushButton(self.topMenu)
        self.btn_exit.setObjectName(u"btn_exit")
        sizePolicy.setHeightForWidth(self.btn_exit.sizePolicy().hasHeightForWidth())
        self.btn_exit.setSizePolicy(sizePolicy)
        self.btn_exit.setMinimumSize(QSize(0, 45))
        self.btn_exit.setFont(font)
        self.btn_exit.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_exit.setLayoutDirection(Qt.LeftToRight)
        self.btn_exit.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-x.png);")

        self.verticalLayout_8.addWidget(self.btn_exit)


        self.verticalMenuLayout.addWidget(self.topMenu, 0, Qt.AlignTop)

        self.bottomMenu = QFrame(self.leftMenuFrame)
        self.bottomMenu.setObjectName(u"bottomMenu")
        self.bottomMenu.setFrameShape(QFrame.NoFrame)
        self.bottomMenu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_9 = QVBoxLayout(self.bottomMenu)
        self.verticalLayout_9.setSpacing(0)
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.verticalLayout_9.setContentsMargins(0, 0, 0, 0)
        self.toggleLeftBox = QPushButton(self.bottomMenu)
        self.toggleLeftBox.setObjectName(u"toggleLeftBox")
        sizePolicy.setHeightForWidth(self.toggleLeftBox.sizePolicy().hasHeightForWidth())
        self.toggleLeftBox.setSizePolicy(sizePolicy)
        self.toggleLeftBox.setMinimumSize(QSize(0, 45))
        self.toggleLeftBox.setFont(font)
        self.toggleLeftBox.setCursor(QCursor(Qt.PointingHandCursor))
        self.toggleLeftBox.setLayoutDirection(Qt.LeftToRight)
        self.toggleLeftBox.setStyleSheet(u"background-image: url(:/icons/images/icons/icon_settings.png);")

        self.verticalLayout_9.addWidget(self.toggleLeftBox)


        self.verticalMenuLayout.addWidget(self.bottomMenu, 0, Qt.AlignBottom)


        self.verticalLayout_3.addWidget(self.leftMenuFrame)


        self.appLayout.addWidget(self.leftMenuBg)

        self.extraLeftBox = QFrame(self.bgApp)
        self.extraLeftBox.setObjectName(u"extraLeftBox")
        self.extraLeftBox.setMinimumSize(QSize(0, 0))
        self.extraLeftBox.setMaximumSize(QSize(0, 16777215))
        self.extraLeftBox.setStyleSheet(u"")
        self.extraLeftBox.setFrameShape(QFrame.NoFrame)
        self.extraLeftBox.setFrameShadow(QFrame.Raised)
        self.verticalLayout_11 = QVBoxLayout(self.extraLeftBox)
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.verticalLayout_11.setContentsMargins(0, 0, 0, 0)
        self.extraTopBg = QFrame(self.extraLeftBox)
        self.extraTopBg.setObjectName(u"extraTopBg")
        self.extraTopBg.setMinimumSize(QSize(0, 50))
        self.extraTopBg.setMaximumSize(QSize(16777215, 50))
        self.extraTopBg.setFrameShape(QFrame.NoFrame)
        self.extraTopBg.setFrameShadow(QFrame.Raised)
        self.verticalLayout_5 = QVBoxLayout(self.extraTopBg)
        self.verticalLayout_5.setSpacing(0)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.extraTopLayout = QGridLayout()
        self.extraTopLayout.setObjectName(u"extraTopLayout")
        self.extraTopLayout.setHorizontalSpacing(10)
        self.extraTopLayout.setVerticalSpacing(0)
        self.extraTopLayout.setContentsMargins(10, -1, 10, -1)
        self.extraIcon = QFrame(self.extraTopBg)
        self.extraIcon.setObjectName(u"extraIcon")
        self.extraIcon.setMinimumSize(QSize(20, 0))
        self.extraIcon.setMaximumSize(QSize(20, 20))
        self.extraIcon.setFrameShape(QFrame.NoFrame)
        self.extraIcon.setFrameShadow(QFrame.Raised)

        self.extraTopLayout.addWidget(self.extraIcon, 0, 0, 1, 1)

        self.extraLabel = QLabel(self.extraTopBg)
        self.extraLabel.setObjectName(u"extraLabel")
        self.extraLabel.setMinimumSize(QSize(150, 0))

        self.extraTopLayout.addWidget(self.extraLabel, 0, 1, 1, 1)

        self.extraCloseColumnBtn = QPushButton(self.extraTopBg)
        self.extraCloseColumnBtn.setObjectName(u"extraCloseColumnBtn")
        self.extraCloseColumnBtn.setMinimumSize(QSize(28, 28))
        self.extraCloseColumnBtn.setMaximumSize(QSize(28, 28))
        self.extraCloseColumnBtn.setCursor(QCursor(Qt.PointingHandCursor))
        icon = QIcon()
        icon.addFile(u":/icons/images/icons/icon_close.png", QSize(), QIcon.Normal, QIcon.Off)
        self.extraCloseColumnBtn.setIcon(icon)
        self.extraCloseColumnBtn.setIconSize(QSize(20, 20))

        self.extraTopLayout.addWidget(self.extraCloseColumnBtn, 0, 2, 1, 1)


        self.verticalLayout_5.addLayout(self.extraTopLayout)


        self.verticalLayout_11.addWidget(self.extraTopBg)

        self.extraContent = QFrame(self.extraLeftBox)
        self.extraContent.setObjectName(u"extraContent")
        self.extraContent.setMinimumSize(QSize(0, 0))
        self.extraContent.setStyleSheet(u"")
        self.extraContent.setFrameShape(QFrame.NoFrame)
        self.extraContent.setFrameShadow(QFrame.Raised)
        self.verticalLayout_10 = QVBoxLayout(self.extraContent)
        self.verticalLayout_10.setSpacing(1)
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.verticalLayout_10.setContentsMargins(1, 1, 1, 1)
        self.scrollArea_2 = QScrollArea(self.extraContent)
        self.scrollArea_2.setObjectName(u"scrollArea_2")
        self.scrollArea_2.setStyleSheet(u"background-color: transparent;")
        self.scrollArea_2.setFrameShape(QFrame.NoFrame)
        self.scrollArea_2.setWidgetResizable(True)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 222, 721))
        self.verticalLayout_41 = QVBoxLayout(self.scrollAreaWidgetContents_2)
        self.verticalLayout_41.setSpacing(0)
        self.verticalLayout_41.setObjectName(u"verticalLayout_41")
        self.verticalLayout_41.setContentsMargins(0, 0, 0, 0)
        self.extraTopMenu = QFrame(self.scrollAreaWidgetContents_2)
        self.extraTopMenu.setObjectName(u"extraTopMenu")
        self.extraTopMenu.setMinimumSize(QSize(0, 0))
        self.extraTopMenu.setStyleSheet(u"#extraTopMenu .QLabel {\n"
"	color: rgb(143, 143, 143);\n"
"}\n"
"\n"
"#extraTopMenu .QComboBox {\n"
"	background-color: rgb(33, 37, 43);\n"
"	color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"#extraTopMenu .QLineEdit {\n"
"	background-color: rgb(33, 37, 43);\n"
"}\n"
"\n"
"#extraTopMenu .QPushButton {\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"\n"
"#extraTopMenu .QPushButton:hover {\n"
"	background-color: rgb(40, 44, 52);\n"
"}")
        self.extraTopMenu.setFrameShape(QFrame.NoFrame)
        self.extraTopMenu.setFrameShadow(QFrame.Raised)
        self.verticalLayout_33 = QVBoxLayout(self.extraTopMenu)
        self.verticalLayout_33.setObjectName(u"verticalLayout_33")
        self.verticalLayout_23 = QVBoxLayout()
        self.verticalLayout_23.setObjectName(u"verticalLayout_23")
        self.label_5 = QLabel(self.extraTopMenu)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setStyleSheet(u"")

        self.verticalLayout_23.addWidget(self.label_5)

        self.comboBox_testline_name = QComboBox(self.extraTopMenu)
        self.comboBox_testline_name.setObjectName(u"comboBox_testline_name")
        self.comboBox_testline_name.setMaximumSize(QSize(16777215, 30))
        self.comboBox_testline_name.setFont(font)
        self.comboBox_testline_name.setAutoFillBackground(False)
        self.comboBox_testline_name.setStyleSheet(u"")
        self.comboBox_testline_name.setEditable(True)
        self.comboBox_testline_name.setIconSize(QSize(16, 16))
        self.comboBox_testline_name.setFrame(True)

        self.verticalLayout_23.addWidget(self.comboBox_testline_name)


        self.verticalLayout_33.addLayout(self.verticalLayout_23)

        self.verticalLayout_12 = QVBoxLayout()
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.label_8 = QLabel(self.extraTopMenu)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_12.addWidget(self.label_8)

        self.comboBox_site_type = QComboBox(self.extraTopMenu)
        self.comboBox_site_type.addItem("")
        self.comboBox_site_type.addItem("")
        self.comboBox_site_type.setObjectName(u"comboBox_site_type")
        self.comboBox_site_type.setMaximumSize(QSize(16777215, 30))
        self.comboBox_site_type.setFont(font)
        self.comboBox_site_type.setAutoFillBackground(False)
        self.comboBox_site_type.setStyleSheet(u"")
        self.comboBox_site_type.setIconSize(QSize(16, 16))
        self.comboBox_site_type.setFrame(True)

        self.verticalLayout_12.addWidget(self.comboBox_site_type)


        self.verticalLayout_33.addLayout(self.verticalLayout_12)

        self.verticalLayout_29 = QVBoxLayout()
        self.verticalLayout_29.setObjectName(u"verticalLayout_29")
        self.verticalLayout_26 = QVBoxLayout()
        self.verticalLayout_26.setObjectName(u"verticalLayout_26")
        self.label_3 = QLabel(self.extraTopMenu)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_26.addWidget(self.label_3)

        self.lineEdit_node_ip = QLineEdit(self.extraTopMenu)
        self.lineEdit_node_ip.setObjectName(u"lineEdit_node_ip")
        self.lineEdit_node_ip.setMinimumSize(QSize(0, 0))
        self.lineEdit_node_ip.setStyleSheet(u"")

        self.verticalLayout_26.addWidget(self.lineEdit_node_ip)


        self.verticalLayout_29.addLayout(self.verticalLayout_26)

        self.verticalLayout_27 = QVBoxLayout()
        self.verticalLayout_27.setObjectName(u"verticalLayout_27")
        self.label_username = QLabel(self.extraTopMenu)
        self.label_username.setObjectName(u"label_username")
        self.label_username.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_27.addWidget(self.label_username)

        self.lineEdit_username = QLineEdit(self.extraTopMenu)
        self.lineEdit_username.setObjectName(u"lineEdit_username")
        self.lineEdit_username.setMinimumSize(QSize(0, 0))
        self.lineEdit_username.setStyleSheet(u"")

        self.verticalLayout_27.addWidget(self.lineEdit_username)


        self.verticalLayout_29.addLayout(self.verticalLayout_27)

        self.verticalLayout_28 = QVBoxLayout()
        self.verticalLayout_28.setObjectName(u"verticalLayout_28")
        self.label_username_6 = QLabel(self.extraTopMenu)
        self.label_username_6.setObjectName(u"label_username_6")
        self.label_username_6.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_28.addWidget(self.label_username_6)

        self.lineEdit_password = QLineEdit(self.extraTopMenu)
        self.lineEdit_password.setObjectName(u"lineEdit_password")
        self.lineEdit_password.setMinimumSize(QSize(0, 0))
        self.lineEdit_password.setStyleSheet(u"")
        self.lineEdit_password.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        self.verticalLayout_28.addWidget(self.lineEdit_password)


        self.verticalLayout_29.addLayout(self.verticalLayout_28)


        self.verticalLayout_33.addLayout(self.verticalLayout_29)

        self.verticalLayout_34 = QVBoxLayout()
        self.verticalLayout_34.setObjectName(u"verticalLayout_34")
        self.verticalLayout_36 = QVBoxLayout()
        self.verticalLayout_36.setObjectName(u"verticalLayout_36")
        self.label_14 = QLabel(self.extraTopMenu)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_36.addWidget(self.label_14)

        self.lineEdit_app_ip = QLineEdit(self.extraTopMenu)
        self.lineEdit_app_ip.setObjectName(u"lineEdit_app_ip")
        self.lineEdit_app_ip.setMinimumSize(QSize(0, 0))
        self.lineEdit_app_ip.setStyleSheet(u"")

        self.verticalLayout_36.addWidget(self.lineEdit_app_ip)


        self.verticalLayout_34.addLayout(self.verticalLayout_36)

        self.verticalLayout_37 = QVBoxLayout()
        self.verticalLayout_37.setObjectName(u"verticalLayout_37")
        self.label_15 = QLabel(self.extraTopMenu)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_37.addWidget(self.label_15)

        self.lineEdit_app_username = QLineEdit(self.extraTopMenu)
        self.lineEdit_app_username.setObjectName(u"lineEdit_app_username")
        self.lineEdit_app_username.setMinimumSize(QSize(0, 0))
        self.lineEdit_app_username.setStyleSheet(u"")

        self.verticalLayout_37.addWidget(self.lineEdit_app_username)


        self.verticalLayout_34.addLayout(self.verticalLayout_37)

        self.verticalLayout_38 = QVBoxLayout()
        self.verticalLayout_38.setObjectName(u"verticalLayout_38")
        self.label_app_username = QLabel(self.extraTopMenu)
        self.label_app_username.setObjectName(u"label_app_username")
        self.label_app_username.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_38.addWidget(self.label_app_username)

        self.lineEdit_app_password = QLineEdit(self.extraTopMenu)
        self.lineEdit_app_password.setObjectName(u"lineEdit_app_password")
        self.lineEdit_app_password.setMinimumSize(QSize(0, 0))
        self.lineEdit_app_password.setStyleSheet(u"")
        self.lineEdit_app_password.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        self.verticalLayout_38.addWidget(self.lineEdit_app_password)


        self.verticalLayout_34.addLayout(self.verticalLayout_38)


        self.verticalLayout_33.addLayout(self.verticalLayout_34)

        self.verticalLayout_24 = QVBoxLayout()
        self.verticalLayout_24.setObjectName(u"verticalLayout_24")
        self.verticalLayout_web_info = QVBoxLayout()
        self.verticalLayout_web_info.setObjectName(u"verticalLayout_web_info")
        self.verticalLayout_web_info.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.verticalLayout_30 = QVBoxLayout()
        self.verticalLayout_30.setObjectName(u"verticalLayout_30")
        self.label_web_ip = QLabel(self.extraTopMenu)
        self.label_web_ip.setObjectName(u"label_web_ip")
        self.label_web_ip.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_30.addWidget(self.label_web_ip)

        self.lineEdit_web_ip = QLineEdit(self.extraTopMenu)
        self.lineEdit_web_ip.setObjectName(u"lineEdit_web_ip")
        self.lineEdit_web_ip.setMinimumSize(QSize(0, 0))
        self.lineEdit_web_ip.setStyleSheet(u"")

        self.verticalLayout_30.addWidget(self.lineEdit_web_ip)


        self.verticalLayout_web_info.addLayout(self.verticalLayout_30)

        self.verticalLayout_31 = QVBoxLayout()
        self.verticalLayout_31.setObjectName(u"verticalLayout_31")
        self.label_web_user = QLabel(self.extraTopMenu)
        self.label_web_user.setObjectName(u"label_web_user")
        self.label_web_user.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_31.addWidget(self.label_web_user)

        self.lineEdit_web_username = QLineEdit(self.extraTopMenu)
        self.lineEdit_web_username.setObjectName(u"lineEdit_web_username")
        self.lineEdit_web_username.setMinimumSize(QSize(0, 0))
        self.lineEdit_web_username.setStyleSheet(u"")

        self.verticalLayout_31.addWidget(self.lineEdit_web_username)


        self.verticalLayout_web_info.addLayout(self.verticalLayout_31)

        self.verticalLayout_32 = QVBoxLayout()
        self.verticalLayout_32.setObjectName(u"verticalLayout_32")
        self.label_web_password = QLabel(self.extraTopMenu)
        self.label_web_password.setObjectName(u"label_web_password")
        self.label_web_password.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_32.addWidget(self.label_web_password)

        self.lineEdit_web_password = QLineEdit(self.extraTopMenu)
        self.lineEdit_web_password.setObjectName(u"lineEdit_web_password")
        self.lineEdit_web_password.setMinimumSize(QSize(0, 0))
        self.lineEdit_web_password.setStyleSheet(u"")
        self.lineEdit_web_password.setEchoMode(QLineEdit.PasswordEchoOnEdit)

        self.verticalLayout_32.addWidget(self.lineEdit_web_password)


        self.verticalLayout_web_info.addLayout(self.verticalLayout_32)


        self.verticalLayout_24.addLayout(self.verticalLayout_web_info)

        self.verticalLayout_42 = QVBoxLayout()
        self.verticalLayout_42.setObjectName(u"verticalLayout_42")
        self.label_Namespace = QLabel(self.extraTopMenu)
        self.label_Namespace.setObjectName(u"label_Namespace")
        self.label_Namespace.setMaximumSize(QSize(16777215, 18))

        self.verticalLayout_42.addWidget(self.label_Namespace)

        self.lineEdit_Namespace = QLineEdit(self.extraTopMenu)
        self.lineEdit_Namespace.setObjectName(u"lineEdit_Namespace")
        self.lineEdit_Namespace.setMinimumSize(QSize(0, 0))
        self.lineEdit_Namespace.setStyleSheet(u"")
        self.lineEdit_Namespace.setInputMethodHints(Qt.ImhNone)
        self.lineEdit_Namespace.setFrame(True)
        self.lineEdit_Namespace.setEchoMode(QLineEdit.Normal)

        self.verticalLayout_42.addWidget(self.lineEdit_Namespace)


        self.verticalLayout_24.addLayout(self.verticalLayout_42)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.pushButton_save = QPushButton(self.extraTopMenu)
        self.pushButton_save.setObjectName(u"pushButton_save")
        self.pushButton_save.setStyleSheet(u"")

        self.horizontalLayout_6.addWidget(self.pushButton_save)

        self.horizontalSpacer = QSpacerItem(30, 17, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_6.addItem(self.horizontalSpacer)


        self.verticalLayout_24.addLayout(self.horizontalLayout_6)

        self.verticalSpacer = QSpacerItem(17, 18, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_24.addItem(self.verticalSpacer)


        self.verticalLayout_33.addLayout(self.verticalLayout_24)


        self.verticalLayout_41.addWidget(self.extraTopMenu)

        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)

        self.verticalLayout_10.addWidget(self.scrollArea_2)


        self.verticalLayout_11.addWidget(self.extraContent)


        self.appLayout.addWidget(self.extraLeftBox)

        self.contentBox = QFrame(self.bgApp)
        self.contentBox.setObjectName(u"contentBox")
        self.contentBox.setFrameShape(QFrame.NoFrame)
        self.contentBox.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.contentBox)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.contentTopBg = QFrame(self.contentBox)
        self.contentTopBg.setObjectName(u"contentTopBg")
        self.contentTopBg.setMinimumSize(QSize(0, 50))
        self.contentTopBg.setMaximumSize(QSize(16777215, 50))
        self.contentTopBg.setFrameShape(QFrame.NoFrame)
        self.contentTopBg.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.contentTopBg)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 10, 0)
        self.leftBox = QFrame(self.contentTopBg)
        self.leftBox.setObjectName(u"leftBox")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.leftBox.sizePolicy().hasHeightForWidth())
        self.leftBox.setSizePolicy(sizePolicy1)
        self.leftBox.setFrameShape(QFrame.NoFrame)
        self.leftBox.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.leftBox)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.titleRightInfo = QLabel(self.leftBox)
        self.titleRightInfo.setObjectName(u"titleRightInfo")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.titleRightInfo.sizePolicy().hasHeightForWidth())
        self.titleRightInfo.setSizePolicy(sizePolicy2)
        self.titleRightInfo.setMaximumSize(QSize(16777215, 45))
        self.titleRightInfo.setFont(font)
        self.titleRightInfo.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.horizontalLayout_3.addWidget(self.titleRightInfo)


        self.horizontalLayout.addWidget(self.leftBox)

        self.rightButtons = QFrame(self.contentTopBg)
        self.rightButtons.setObjectName(u"rightButtons")
        self.rightButtons.setMinimumSize(QSize(0, 28))
        self.rightButtons.setFrameShape(QFrame.NoFrame)
        self.rightButtons.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.rightButtons)
        self.horizontalLayout_2.setSpacing(5)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.settingsTopBtn = QPushButton(self.rightButtons)
        self.settingsTopBtn.setObjectName(u"settingsTopBtn")
        self.settingsTopBtn.setMinimumSize(QSize(28, 28))
        self.settingsTopBtn.setMaximumSize(QSize(28, 28))
        self.settingsTopBtn.setCursor(QCursor(Qt.PointingHandCursor))
        icon1 = QIcon()
        icon1.addFile(u":/icons/images/icons/viruses.png", QSize(), QIcon.Normal, QIcon.Off)
        self.settingsTopBtn.setIcon(icon1)
        self.settingsTopBtn.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.settingsTopBtn)

        self.minimizeAppBtn = QPushButton(self.rightButtons)
        self.minimizeAppBtn.setObjectName(u"minimizeAppBtn")
        self.minimizeAppBtn.setMinimumSize(QSize(28, 28))
        self.minimizeAppBtn.setMaximumSize(QSize(28, 28))
        self.minimizeAppBtn.setCursor(QCursor(Qt.PointingHandCursor))
        icon2 = QIcon()
        icon2.addFile(u":/icons/images/icons/icon_minimize.png", QSize(), QIcon.Normal, QIcon.Off)
        self.minimizeAppBtn.setIcon(icon2)
        self.minimizeAppBtn.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.minimizeAppBtn)

        self.maximizeRestoreAppBtn = QPushButton(self.rightButtons)
        self.maximizeRestoreAppBtn.setObjectName(u"maximizeRestoreAppBtn")
        self.maximizeRestoreAppBtn.setMinimumSize(QSize(28, 28))
        self.maximizeRestoreAppBtn.setMaximumSize(QSize(28, 28))
        font3 = QFont()
        font3.setFamilies([u"Segoe UI"])
        font3.setPointSize(10)
        font3.setBold(False)
        font3.setItalic(False)
        font3.setStyleStrategy(QFont.PreferDefault)
        self.maximizeRestoreAppBtn.setFont(font3)
        self.maximizeRestoreAppBtn.setCursor(QCursor(Qt.PointingHandCursor))
        icon3 = QIcon()
        icon3.addFile(u":/icons/images/icons/icon_maximize.png", QSize(), QIcon.Normal, QIcon.Off)
        self.maximizeRestoreAppBtn.setIcon(icon3)
        self.maximizeRestoreAppBtn.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.maximizeRestoreAppBtn)

        self.closeAppBtn = QPushButton(self.rightButtons)
        self.closeAppBtn.setObjectName(u"closeAppBtn")
        self.closeAppBtn.setMinimumSize(QSize(28, 28))
        self.closeAppBtn.setMaximumSize(QSize(28, 28))
        self.closeAppBtn.setCursor(QCursor(Qt.PointingHandCursor))
        self.closeAppBtn.setIcon(icon)
        self.closeAppBtn.setIconSize(QSize(20, 20))

        self.horizontalLayout_2.addWidget(self.closeAppBtn)


        self.horizontalLayout.addWidget(self.rightButtons, 0, Qt.AlignRight)


        self.verticalLayout_2.addWidget(self.contentTopBg)

        self.contentBottom = QFrame(self.contentBox)
        self.contentBottom.setObjectName(u"contentBottom")
        self.contentBottom.setFrameShape(QFrame.NoFrame)
        self.contentBottom.setFrameShadow(QFrame.Raised)
        self.verticalLayout_6 = QVBoxLayout(self.contentBottom)
        self.verticalLayout_6.setSpacing(0)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(0, 0, 0, 0)
        self.content = QFrame(self.contentBottom)
        self.content.setObjectName(u"content")
        self.content.setFrameShape(QFrame.NoFrame)
        self.content.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_4 = QHBoxLayout(self.content)
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.horizontalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.pagesContainer = QFrame(self.content)
        self.pagesContainer.setObjectName(u"pagesContainer")
        self.pagesContainer.setStyleSheet(u"")
        self.pagesContainer.setFrameShape(QFrame.NoFrame)
        self.pagesContainer.setFrameShadow(QFrame.Raised)
        self.verticalLayout_15 = QVBoxLayout(self.pagesContainer)
        self.verticalLayout_15.setSpacing(0)
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.verticalLayout_15.setContentsMargins(10, 10, 10, 10)
        self.stackedWidget = QStackedWidget(self.pagesContainer)
        self.stackedWidget.setObjectName(u"stackedWidget")
        self.stackedWidget.setStyleSheet(u" background: transparent;\n"
"")
        self.home = QWidget()
        self.home.setObjectName(u"home")
        self.home.setStyleSheet(u"background-image: url(:/images/images/images/nokia-refreshed-logo-3_0.jpg);\n"
"background-position: center;\n"
"background-repeat: no-repeat;")
        self.stackedWidget.addWidget(self.home)
        self.tput_page = QWidget()
        self.tput_page.setObjectName(u"tput_page")
        self.tput_page.setStyleSheet(u"QRadioButton::indicator:checked {\n"
"	background: 3px solid rgb(189, 147, 249);\n"
"	border: 3px solid rgb(52, 59, 72);\n"
"}\n"
"\n"
"QCheckBox::indicator:checked {\n"
"	background: 3px solid rgb(189, 147, 249);\n"
"	border: 3px solid rgb(52, 59, 72);\n"
"}\n"
"\n"
"#frame_tput QFrame:hover {\n"
"    border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"\n"
"\n"
"#frame_tput .QPushButton {\n"
"	color: #ffffff;\n"
"	background-color: rgb(52, 59, 72);\n"
"}\n"
"\n"
"#frame_tput .QPushButton:hover {\n"
"	background-color: rgb(40, 44, 52);\n"
"}\n"
"\n"
"#frame_tput .QPushButton:pressed {\n"
"	background-color: rgb(189, 147, 249);\n"
"	color: rgb(255, 255, 255);\n"
"}\n"
"\n"
"#frame_tput .QComboBox {\n"
"	background-color: rgb(33, 37, 43);\n"
"	color: rgb(255, 121, 198);\n"
"}\n"
"\n"
"#frame_tput QToolTip {\n"
"	color: #ffffff;\n"
"	background-color: rgba(33, 37, 43, 180);\n"
"	border: 1px solid rgb(44, 49, 58);\n"
"	background-image: none;\n"
"	background-position: left center;\n"
"	background-repeat: no-repeat;\n"
"	"
                        "border: none;\n"
"	border-left: 2px solid rgb(255, 121, 198);\n"
"	text-align: left;\n"
"	padding-left: 8px;\n"
"	margin: 0px;\n"
"}\n"
"#frame_tput QLineEdit {\n"
"	color: rgb(255, 121, 198);\n"
"	background-color: rgb(33, 37, 43);\n"
"    border-radius: 5px;\n"
"    border: 2px solid rgb(33, 37, 43);\n"
"    padding-left: 10px;\n"
"}\n"
"\n"
"#frame_tput QLineEdit:hover {\n"
"    border: 2px solid rgb(64, 71, 88);\n"
"}\n"
"\n"
"\n"
"#frame_tput QLineEdit:focus {\n"
"    border: 2px solid rgb(91, 101, 124);\n"
"}\n"
"\n"
"#frame_tput #label_btslog_status {\n"
"	border-radius: 12px;\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"}\n"
"\n"
"#frame_tput #label_btslog_status:hover {\n"
"	border-radius: 12px;\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"}\n"
"\n"
"#frame_tput #label_btslog_status:disabled {\n"
"	border-radius: 12px;\n"
"	border: 2px solid rgb(52, 59, 72);\n"
"	background-color: lawngreen;\n"
"}\n"
"\n"
"#frame_tput .QLabel {\n"
"	color: rgb(143, 143, 143);\n"
"}\n"
"#frame_tput QLabel:hover {\n"
"   "
                        "border: 2px\n"
"}\n"
"\n"
"#splitter .QLabel {\n"
"	color: rgb(143, 143, 143);\n"
"}\n"
"\n"
"#splitter QTextBrowser {\n"
"	background-color: rgb(33, 37, 43);\n"
"}")
        self.verticalLayout_22 = QVBoxLayout(self.tput_page)
        self.verticalLayout_22.setObjectName(u"verticalLayout_22")
        self.frame_tput = QFrame(self.tput_page)
        self.frame_tput.setObjectName(u"frame_tput")
        self.frame_tput.setMaximumSize(QSize(16777215, 150))
        self.frame_tput.setStyleSheet(u"")
        self.frame_tput.setFrameShape(QFrame.StyledPanel)
        self.frame_tput.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_13 = QHBoxLayout(self.frame_tput)
        self.horizontalLayout_13.setSpacing(2)
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.horizontalLayout_13.setContentsMargins(2, 0, 2, 2)
        self.frame_tput_check = QFrame(self.frame_tput)
        self.frame_tput_check.setObjectName(u"frame_tput_check")
        self.frame_tput_check.setMinimumSize(QSize(0, 0))
        self.frame_tput_check.setMaximumSize(QSize(400, 16777215))
        self.frame_tput_check.setFrameShape(QFrame.StyledPanel)
        self.frame_tput_check.setFrameShadow(QFrame.Raised)
        self.verticalLayout_25 = QVBoxLayout(self.frame_tput_check)
        self.verticalLayout_25.setObjectName(u"verticalLayout_25")
        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.pushButton_start = QPushButton(self.frame_tput_check)
        self.pushButton_start.setObjectName(u"pushButton_start")
        self.pushButton_start.setMinimumSize(QSize(75, 30))
        self.pushButton_start.setMaximumSize(QSize(75, 30))
        self.pushButton_start.setSizeIncrement(QSize(70, 30))
        self.pushButton_start.setBaseSize(QSize(0, 0))
        self.pushButton_start.setStyleSheet(u"")

        self.horizontalLayout_16.addWidget(self.pushButton_start)

        self.pushButton_stop = QPushButton(self.frame_tput_check)
        self.pushButton_stop.setObjectName(u"pushButton_stop")
        self.pushButton_stop.setEnabled(True)
        self.pushButton_stop.setMinimumSize(QSize(75, 30))
        self.pushButton_stop.setMaximumSize(QSize(75, 30))
        self.pushButton_stop.setSizeIncrement(QSize(70, 30))
        self.pushButton_stop.setBaseSize(QSize(0, 0))
        self.pushButton_stop.setStyleSheet(u"")

        self.horizontalLayout_16.addWidget(self.pushButton_stop)

        self.pushButton_clear = QPushButton(self.frame_tput_check)
        self.pushButton_clear.setObjectName(u"pushButton_clear")
        self.pushButton_clear.setMinimumSize(QSize(75, 30))
        self.pushButton_clear.setMaximumSize(QSize(75, 30))
        self.pushButton_clear.setSizeIncrement(QSize(70, 30))
        self.pushButton_clear.setBaseSize(QSize(0, 0))

        self.horizontalLayout_16.addWidget(self.pushButton_clear)

        self.pushButton_tput_log_open = QPushButton(self.frame_tput_check)
        self.pushButton_tput_log_open.setObjectName(u"pushButton_tput_log_open")
        self.pushButton_tput_log_open.setMinimumSize(QSize(75, 30))
        self.pushButton_tput_log_open.setMaximumSize(QSize(75, 30))
        self.pushButton_tput_log_open.setSizeIncrement(QSize(70, 30))
        self.pushButton_tput_log_open.setBaseSize(QSize(0, 0))

        self.horizontalLayout_16.addWidget(self.pushButton_tput_log_open)


        self.verticalLayout_25.addLayout(self.horizontalLayout_16)

        self.horizontalLayout_17 = QHBoxLayout()
        self.horizontalLayout_17.setObjectName(u"horizontalLayout_17")
        self.comboBox_check_tput_mode = QComboBox(self.frame_tput_check)
        self.comboBox_check_tput_mode.addItem("")
        self.comboBox_check_tput_mode.addItem("")
        self.comboBox_check_tput_mode.setObjectName(u"comboBox_check_tput_mode")
        self.comboBox_check_tput_mode.setFont(font)
        self.comboBox_check_tput_mode.setAutoFillBackground(False)
        self.comboBox_check_tput_mode.setStyleSheet(u"")
        self.comboBox_check_tput_mode.setIconSize(QSize(16, 16))
        self.comboBox_check_tput_mode.setFrame(True)

        self.horizontalLayout_17.addWidget(self.comboBox_check_tput_mode)

        self.checkBox_debug_mode = QCheckBox(self.frame_tput_check)
        self.checkBox_debug_mode.setObjectName(u"checkBox_debug_mode")
        self.checkBox_debug_mode.setAutoFillBackground(False)
        self.checkBox_debug_mode.setStyleSheet(u"")

        self.horizontalLayout_17.addWidget(self.checkBox_debug_mode)


        self.verticalLayout_25.addLayout(self.horizontalLayout_17)

        self.checkBox_with_btslog = QCheckBox(self.frame_tput_check)
        self.checkBox_with_btslog.setObjectName(u"checkBox_with_btslog")
        self.checkBox_with_btslog.setAutoFillBackground(False)
        self.checkBox_with_btslog.setStyleSheet(u"")

        self.verticalLayout_25.addWidget(self.checkBox_with_btslog)

        self.commandLinkButton_tput_log = QCommandLinkButton(self.frame_tput_check)
        self.commandLinkButton_tput_log.setObjectName(u"commandLinkButton_tput_log")
        self.commandLinkButton_tput_log.setMinimumSize(QSize(0, 0))
        self.commandLinkButton_tput_log.setCursor(QCursor(Qt.PointingHandCursor))
        self.commandLinkButton_tput_log.setLayoutDirection(Qt.RightToLeft)
        self.commandLinkButton_tput_log.setAutoFillBackground(False)
        self.commandLinkButton_tput_log.setStyleSheet(u"")
        icon4 = QIcon()
        icon4.addFile(u":/icons/images/icons/cil-link.png", QSize(), QIcon.Normal, QIcon.Off)
        self.commandLinkButton_tput_log.setIcon(icon4)
        self.commandLinkButton_tput_log.setIconSize(QSize(16, 16))
        self.commandLinkButton_tput_log.setCheckable(False)

        self.verticalLayout_25.addWidget(self.commandLinkButton_tput_log)

        self.verticalSpacer_2 = QSpacerItem(20, 17, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_25.addItem(self.verticalSpacer_2)


        self.horizontalLayout_13.addWidget(self.frame_tput_check)

        self.line = QFrame(self.frame_tput)
        self.line.setObjectName(u"line")
        self.line.setWindowModality(Qt.NonModal)
        self.line.setStyleSheet(u"background-color: rgb(66, 66, 66);")
        self.line.setFrameShadow(QFrame.Sunken)
        self.line.setLineWidth(1)
        self.line.setMidLineWidth(0)
        self.line.setFrameShape(QFrame.VLine)

        self.horizontalLayout_13.addWidget(self.line)

        self.frame_DL_traffic = QFrame(self.frame_tput)
        self.frame_DL_traffic.setObjectName(u"frame_DL_traffic")
        self.frame_DL_traffic.setMinimumSize(QSize(0, 0))
        self.frame_DL_traffic.setMaximumSize(QSize(250, 16777215))
        self.frame_DL_traffic.setFrameShape(QFrame.StyledPanel)
        self.frame_DL_traffic.setFrameShadow(QFrame.Raised)
        self.verticalLayout_35 = QVBoxLayout(self.frame_DL_traffic)
        self.verticalLayout_35.setSpacing(6)
        self.verticalLayout_35.setObjectName(u"verticalLayout_35")
        self.verticalLayout_35.setContentsMargins(6, 6, 6, 0)
        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setSpacing(6)
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_4 = QLabel(self.frame_DL_traffic)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMinimumSize(QSize(0, 0))
        self.label_4.setMaximumSize(QSize(16777215, 16777215))
        self.label_4.setStyleSheet(u"")

        self.horizontalLayout_7.addWidget(self.label_4)

        self.lineEdit_ue_ip = QLineEdit(self.frame_DL_traffic)
        self.lineEdit_ue_ip.setObjectName(u"lineEdit_ue_ip")
        self.lineEdit_ue_ip.setMinimumSize(QSize(0, 0))
        self.lineEdit_ue_ip.setMaximumSize(QSize(16777215, 16777215))
        self.lineEdit_ue_ip.setStyleSheet(u"")

        self.horizontalLayout_7.addWidget(self.lineEdit_ue_ip)

        self.horizontalLayout_7.setStretch(0, 1)
        self.horizontalLayout_7.setStretch(1, 3)

        self.verticalLayout_35.addLayout(self.horizontalLayout_7)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.label_6 = QLabel(self.frame_DL_traffic)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setStyleSheet(u"")
        self.label_6.setFrameShadow(QFrame.Plain)

        self.horizontalLayout_8.addWidget(self.label_6)

        self.lineEdit_bandwidth = QLineEdit(self.frame_DL_traffic)
        self.lineEdit_bandwidth.setObjectName(u"lineEdit_bandwidth")
        self.lineEdit_bandwidth.setStyleSheet(u"")

        self.horizontalLayout_8.addWidget(self.lineEdit_bandwidth)

        self.horizontalLayout_8.setStretch(0, 1)
        self.horizontalLayout_8.setStretch(1, 1)

        self.verticalLayout_35.addLayout(self.horizontalLayout_8)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.label_7 = QLabel(self.frame_DL_traffic)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setStyleSheet(u"")

        self.horizontalLayout_10.addWidget(self.label_7)

        self.lineEdit_port = QLineEdit(self.frame_DL_traffic)
        self.lineEdit_port.setObjectName(u"lineEdit_port")
        self.lineEdit_port.setStyleSheet(u"")

        self.horizontalLayout_10.addWidget(self.lineEdit_port)

        self.horizontalLayout_10.setStretch(0, 1)
        self.horizontalLayout_10.setStretch(1, 1)

        self.verticalLayout_35.addLayout(self.horizontalLayout_10)

        self.pushButton_start_DL = QPushButton(self.frame_DL_traffic)
        self.pushButton_start_DL.setObjectName(u"pushButton_start_DL")
        self.pushButton_start_DL.setEnabled(True)
        self.pushButton_start_DL.setMinimumSize(QSize(75, 30))
        self.pushButton_start_DL.setMaximumSize(QSize(75, 30))
        self.pushButton_start_DL.setSizeIncrement(QSize(0, 0))
        self.pushButton_start_DL.setBaseSize(QSize(0, 0))
        self.pushButton_start_DL.setStyleSheet(u"")

        self.verticalLayout_35.addWidget(self.pushButton_start_DL)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_35.addItem(self.verticalSpacer_4)


        self.horizontalLayout_13.addWidget(self.frame_DL_traffic)

        self.line_2 = QFrame(self.frame_tput)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setWindowModality(Qt.NonModal)
        self.line_2.setStyleSheet(u"background-color: rgb(66, 66, 66);")
        self.line_2.setFrameShadow(QFrame.Sunken)
        self.line_2.setLineWidth(1)
        self.line_2.setMidLineWidth(0)
        self.line_2.setFrameShape(QFrame.VLine)

        self.horizontalLayout_13.addWidget(self.line_2)

        self.frame_curve = QFrame(self.frame_tput)
        self.frame_curve.setObjectName(u"frame_curve")
        self.frame_curve.setMinimumSize(QSize(0, 0))
        self.frame_curve.setMaximumSize(QSize(200, 16777215))
        self.frame_curve.setFrameShape(QFrame.StyledPanel)
        self.frame_curve.setFrameShadow(QFrame.Raised)
        self.verticalLayout_39 = QVBoxLayout(self.frame_curve)
        self.verticalLayout_39.setObjectName(u"verticalLayout_39")
        self.verticalLayout_39.setContentsMargins(6, 6, 6, 0)
        self.lineEdit_tput_log = QLineEdit(self.frame_curve)
        self.lineEdit_tput_log.setObjectName(u"lineEdit_tput_log")

        self.verticalLayout_39.addWidget(self.lineEdit_tput_log)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.label_9 = QLabel(self.frame_curve)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setStyleSheet(u"")

        self.horizontalLayout_14.addWidget(self.label_9)

        self.lineEdit_DL_peak = QLineEdit(self.frame_curve)
        self.lineEdit_DL_peak.setObjectName(u"lineEdit_DL_peak")
        self.lineEdit_DL_peak.setLayoutDirection(Qt.LeftToRight)
        self.lineEdit_DL_peak.setStyleSheet(u"")

        self.horizontalLayout_14.addWidget(self.lineEdit_DL_peak)

        self.label_11 = QLabel(self.frame_curve)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setStyleSheet(u"")

        self.horizontalLayout_14.addWidget(self.label_11)

        self.horizontalLayout_14.setStretch(0, 1)
        self.horizontalLayout_14.setStretch(1, 1)
        self.horizontalLayout_14.setStretch(2, 1)

        self.verticalLayout_39.addLayout(self.horizontalLayout_14)

        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.label_10 = QLabel(self.frame_curve)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setStyleSheet(u"")

        self.horizontalLayout_15.addWidget(self.label_10)

        self.lineEdit_UL_peak = QLineEdit(self.frame_curve)
        self.lineEdit_UL_peak.setObjectName(u"lineEdit_UL_peak")
        self.lineEdit_UL_peak.setStyleSheet(u"")

        self.horizontalLayout_15.addWidget(self.lineEdit_UL_peak)

        self.label_12 = QLabel(self.frame_curve)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setStyleSheet(u"")

        self.horizontalLayout_15.addWidget(self.label_12)

        self.horizontalLayout_15.setStretch(0, 1)
        self.horizontalLayout_15.setStretch(1, 1)
        self.horizontalLayout_15.setStretch(2, 1)

        self.verticalLayout_39.addLayout(self.horizontalLayout_15)

        self.pushButton_curve = QPushButton(self.frame_curve)
        self.pushButton_curve.setObjectName(u"pushButton_curve")
        self.pushButton_curve.setEnabled(True)
        self.pushButton_curve.setMinimumSize(QSize(100, 30))
        self.pushButton_curve.setMaximumSize(QSize(100, 25))
        self.pushButton_curve.setSizeIncrement(QSize(70, 25))
        self.pushButton_curve.setBaseSize(QSize(70, 25))
        self.pushButton_curve.setStyleSheet(u"")

        self.verticalLayout_39.addWidget(self.pushButton_curve)

        self.verticalSpacer_3 = QSpacerItem(20, 13, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_39.addItem(self.verticalSpacer_3)


        self.horizontalLayout_13.addWidget(self.frame_curve)

        self.line_5 = QFrame(self.frame_tput)
        self.line_5.setObjectName(u"line_5")
        self.line_5.setWindowModality(Qt.NonModal)
        self.line_5.setStyleSheet(u"background-color: rgb(66, 66, 66);")
        self.line_5.setFrameShadow(QFrame.Sunken)
        self.line_5.setLineWidth(1)
        self.line_5.setMidLineWidth(0)
        self.line_5.setFrameShape(QFrame.VLine)

        self.horizontalLayout_13.addWidget(self.line_5)

        self.frame_btslog = QFrame(self.frame_tput)
        self.frame_btslog.setObjectName(u"frame_btslog")
        self.frame_btslog.setMaximumSize(QSize(240, 16777215))
        self.frame_btslog.setFrameShape(QFrame.StyledPanel)
        self.frame_btslog.setFrameShadow(QFrame.Raised)
        self.verticalLayout_43 = QVBoxLayout(self.frame_btslog)
        self.verticalLayout_43.setObjectName(u"verticalLayout_43")
        self.horizontalLayout_18 = QHBoxLayout()
        self.horizontalLayout_18.setObjectName(u"horizontalLayout_18")
        self.label_13 = QLabel(self.frame_btslog)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setStyleSheet(u"")

        self.horizontalLayout_18.addWidget(self.label_13)

        self.pushButton_start_btslog = QPushButton(self.frame_btslog)
        self.pushButton_start_btslog.setObjectName(u"pushButton_start_btslog")
        self.pushButton_start_btslog.setMinimumSize(QSize(75, 30))
        self.pushButton_start_btslog.setMaximumSize(QSize(75, 30))
        self.pushButton_start_btslog.setSizeIncrement(QSize(70, 30))
        self.pushButton_start_btslog.setBaseSize(QSize(0, 0))
        self.pushButton_start_btslog.setStyleSheet(u"")

        self.horizontalLayout_18.addWidget(self.pushButton_start_btslog)

        self.pushButton_stop_btslog = QPushButton(self.frame_btslog)
        self.pushButton_stop_btslog.setObjectName(u"pushButton_stop_btslog")
        self.pushButton_stop_btslog.setEnabled(True)
        self.pushButton_stop_btslog.setMinimumSize(QSize(75, 30))
        self.pushButton_stop_btslog.setMaximumSize(QSize(75, 30))
        self.pushButton_stop_btslog.setSizeIncrement(QSize(70, 30))
        self.pushButton_stop_btslog.setBaseSize(QSize(0, 0))
        self.pushButton_stop_btslog.setStyleSheet(u"")

        self.horizontalLayout_18.addWidget(self.pushButton_stop_btslog)

        self.label_btslog_status = QLabel(self.frame_btslog)
        self.label_btslog_status.setObjectName(u"label_btslog_status")
        self.label_btslog_status.setMinimumSize(QSize(24, 24))
        self.label_btslog_status.setMaximumSize(QSize(24, 24))
        self.label_btslog_status.setStyleSheet(u"")
        self.label_btslog_status.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_18.addWidget(self.label_btslog_status)


        self.verticalLayout_43.addLayout(self.horizontalLayout_18)

        self.comboBox_btslog_mode = QComboBox(self.frame_btslog)
        self.comboBox_btslog_mode.addItem("")
        self.comboBox_btslog_mode.addItem("")
        self.comboBox_btslog_mode.setObjectName(u"comboBox_btslog_mode")
        self.comboBox_btslog_mode.setFont(font)
        self.comboBox_btslog_mode.setAutoFillBackground(False)
        self.comboBox_btslog_mode.setStyleSheet(u"")
        self.comboBox_btslog_mode.setIconSize(QSize(16, 16))
        self.comboBox_btslog_mode.setFrame(True)

        self.verticalLayout_43.addWidget(self.comboBox_btslog_mode)

        self.commandLinkButton_open_btslog = QCommandLinkButton(self.frame_btslog)
        self.commandLinkButton_open_btslog.setObjectName(u"commandLinkButton_open_btslog")
        self.commandLinkButton_open_btslog.setCursor(QCursor(Qt.PointingHandCursor))
        self.commandLinkButton_open_btslog.setLayoutDirection(Qt.RightToLeft)
        self.commandLinkButton_open_btslog.setAutoFillBackground(False)
        self.commandLinkButton_open_btslog.setStyleSheet(u"")
        self.commandLinkButton_open_btslog.setIcon(icon4)
        self.commandLinkButton_open_btslog.setIconSize(QSize(16, 16))
        self.commandLinkButton_open_btslog.setCheckable(False)

        self.verticalLayout_43.addWidget(self.commandLinkButton_open_btslog)

        self.verticalSpacer_5 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_43.addItem(self.verticalSpacer_5)


        self.horizontalLayout_13.addWidget(self.frame_btslog)

        self.line_6 = QFrame(self.frame_tput)
        self.line_6.setObjectName(u"line_6")
        self.line_6.setWindowModality(Qt.NonModal)
        self.line_6.setStyleSheet(u"background-color: rgb(66, 66, 66);")
        self.line_6.setFrameShadow(QFrame.Sunken)
        self.line_6.setLineWidth(1)
        self.line_6.setMidLineWidth(0)
        self.line_6.setFrameShape(QFrame.VLine)

        self.horizontalLayout_13.addWidget(self.line_6)

        self.frame_nrdim = QFrame(self.frame_tput)
        self.frame_nrdim.setObjectName(u"frame_nrdim")
        self.frame_nrdim.setMinimumSize(QSize(0, 0))
        self.frame_nrdim.setFrameShape(QFrame.StyledPanel)
        self.frame_nrdim.setFrameShadow(QFrame.Raised)
        self.verticalLayout_40 = QVBoxLayout(self.frame_nrdim)
        self.verticalLayout_40.setSpacing(2)
        self.verticalLayout_40.setObjectName(u"verticalLayout_40")
        self.verticalLayout_40.setContentsMargins(9, 2, -1, 2)
        self.pushButton_getpeak = QPushButton(self.frame_nrdim)
        self.pushButton_getpeak.setObjectName(u"pushButton_getpeak")
        self.pushButton_getpeak.setEnabled(True)
        self.pushButton_getpeak.setMinimumSize(QSize(75, 25))
        self.pushButton_getpeak.setMaximumSize(QSize(75, 30))
        self.pushButton_getpeak.setSizeIncrement(QSize(70, 30))
        self.pushButton_getpeak.setBaseSize(QSize(0, 0))
        self.pushButton_getpeak.setStyleSheet(u"")
        self.pushButton_getpeak.setIconSize(QSize(16, 16))

        self.verticalLayout_40.addWidget(self.pushButton_getpeak)

        self.tableWidget_peak = QTableWidget(self.frame_nrdim)
        self.tableWidget_peak.setObjectName(u"tableWidget_peak")

        self.verticalLayout_40.addWidget(self.tableWidget_peak)


        self.horizontalLayout_13.addWidget(self.frame_nrdim)

        self.horizontalSpacer_2 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_13.addItem(self.horizontalSpacer_2)

        self.horizontalLayout_13.setStretch(0, 1)
        self.horizontalLayout_13.setStretch(1, 1)
        self.horizontalLayout_13.setStretch(2, 1)
        self.horizontalLayout_13.setStretch(3, 1)
        self.horizontalLayout_13.setStretch(4, 1)
        self.horizontalLayout_13.setStretch(5, 1)
        self.horizontalLayout_13.setStretch(6, 1)
        self.horizontalLayout_13.setStretch(7, 1)
        self.horizontalLayout_13.setStretch(8, 1)

        self.verticalLayout_22.addWidget(self.frame_tput)

        self.splitter = QSplitter(self.tput_page)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.layoutWidget = QWidget(self.splitter)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.verticalLayout_20 = QVBoxLayout(self.layoutWidget)
        self.verticalLayout_20.setObjectName(u"verticalLayout_20")
        self.verticalLayout_20.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setLayoutDirection(Qt.LeftToRight)
        self.label.setInputMethodHints(Qt.ImhNoTextHandles)
        self.label.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.verticalLayout_20.addWidget(self.label)

        self.textBrowser_DL_TPUT = QTextBrowser(self.layoutWidget)
        self.textBrowser_DL_TPUT.setObjectName(u"textBrowser_DL_TPUT")
        self.textBrowser_DL_TPUT.setFont(font)
        self.textBrowser_DL_TPUT.setStyleSheet(u"")

        self.verticalLayout_20.addWidget(self.textBrowser_DL_TPUT)

        self.splitter.addWidget(self.layoutWidget)
        self.layoutWidget1 = QWidget(self.splitter)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.verticalLayout_21 = QVBoxLayout(self.layoutWidget1)
        self.verticalLayout_21.setObjectName(u"verticalLayout_21")
        self.verticalLayout_21.setContentsMargins(0, 0, 0, 0)
        self.label_2 = QLabel(self.layoutWidget1)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setStyleSheet(u"")
        self.label_2.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.verticalLayout_21.addWidget(self.label_2)

        self.textBrowser_UL_TPUT = QTextBrowser(self.layoutWidget1)
        self.textBrowser_UL_TPUT.setObjectName(u"textBrowser_UL_TPUT")
        self.textBrowser_UL_TPUT.setFont(font)

        self.verticalLayout_21.addWidget(self.textBrowser_UL_TPUT)

        self.splitter.addWidget(self.layoutWidget1)

        self.verticalLayout_22.addWidget(self.splitter)

        self.stackedWidget.addWidget(self.tput_page)
        self.rf_page = QWidget()
        self.rf_page.setObjectName(u"rf_page")
        self.stackedWidget.addWidget(self.rf_page)
        self.widgets = QWidget()
        self.widgets.setObjectName(u"widgets")
        self.widgets.setStyleSheet(u"b")
        self.verticalLayout = QVBoxLayout(self.widgets)
        self.verticalLayout.setSpacing(10)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(10, 10, 10, 10)
        self.row_1 = QFrame(self.widgets)
        self.row_1.setObjectName(u"row_1")
        self.row_1.setFrameShape(QFrame.StyledPanel)
        self.row_1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_16 = QVBoxLayout(self.row_1)
        self.verticalLayout_16.setSpacing(0)
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.verticalLayout_16.setContentsMargins(0, 0, 0, 0)
        self.frame_div_content_1 = QFrame(self.row_1)
        self.frame_div_content_1.setObjectName(u"frame_div_content_1")
        self.frame_div_content_1.setMinimumSize(QSize(0, 110))
        self.frame_div_content_1.setMaximumSize(QSize(16777215, 110))
        self.frame_div_content_1.setFrameShape(QFrame.NoFrame)
        self.frame_div_content_1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_17 = QVBoxLayout(self.frame_div_content_1)
        self.verticalLayout_17.setSpacing(0)
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.verticalLayout_17.setContentsMargins(0, 0, 0, 0)
        self.frame_title_wid_1 = QFrame(self.frame_div_content_1)
        self.frame_title_wid_1.setObjectName(u"frame_title_wid_1")
        self.frame_title_wid_1.setMaximumSize(QSize(16777215, 35))
        self.frame_title_wid_1.setFrameShape(QFrame.StyledPanel)
        self.frame_title_wid_1.setFrameShadow(QFrame.Raised)
        self.verticalLayout_18 = QVBoxLayout(self.frame_title_wid_1)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.labelBoxBlenderInstalation = QLabel(self.frame_title_wid_1)
        self.labelBoxBlenderInstalation.setObjectName(u"labelBoxBlenderInstalation")
        self.labelBoxBlenderInstalation.setFont(font)
        self.labelBoxBlenderInstalation.setStyleSheet(u"")

        self.verticalLayout_18.addWidget(self.labelBoxBlenderInstalation)


        self.verticalLayout_17.addWidget(self.frame_title_wid_1)

        self.frame_content_wid_1 = QFrame(self.frame_div_content_1)
        self.frame_content_wid_1.setObjectName(u"frame_content_wid_1")
        self.frame_content_wid_1.setFrameShape(QFrame.NoFrame)
        self.frame_content_wid_1.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_9 = QHBoxLayout(self.frame_content_wid_1)
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(-1, -1, -1, 0)
        self.lineEdit = QLineEdit(self.frame_content_wid_1)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setMinimumSize(QSize(0, 30))
        self.lineEdit.setStyleSheet(u"background-color: rgb(33, 37, 43);")

        self.gridLayout.addWidget(self.lineEdit, 0, 0, 1, 1)

        self.pushButton = QPushButton(self.frame_content_wid_1)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setMinimumSize(QSize(150, 30))
        self.pushButton.setFont(font)
        self.pushButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.pushButton.setStyleSheet(u"background-color: rgb(52, 59, 72);")
        icon5 = QIcon()
        icon5.addFile(u":/icons/images/icons/cil-folder-open.png", QSize(), QIcon.Normal, QIcon.Off)
        self.pushButton.setIcon(icon5)

        self.gridLayout.addWidget(self.pushButton, 0, 1, 1, 1)

        self.labelVersion_3 = QLabel(self.frame_content_wid_1)
        self.labelVersion_3.setObjectName(u"labelVersion_3")
        self.labelVersion_3.setStyleSheet(u"color: rgb(113, 126, 149);")
        self.labelVersion_3.setLineWidth(1)
        self.labelVersion_3.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.labelVersion_3, 1, 0, 1, 2)


        self.horizontalLayout_9.addLayout(self.gridLayout)


        self.verticalLayout_17.addWidget(self.frame_content_wid_1)


        self.verticalLayout_16.addWidget(self.frame_div_content_1)


        self.verticalLayout.addWidget(self.row_1)

        self.row_2 = QFrame(self.widgets)
        self.row_2.setObjectName(u"row_2")
        self.row_2.setMinimumSize(QSize(0, 150))
        self.row_2.setFrameShape(QFrame.StyledPanel)
        self.row_2.setFrameShadow(QFrame.Raised)
        self.verticalLayout_19 = QVBoxLayout(self.row_2)
        self.verticalLayout_19.setObjectName(u"verticalLayout_19")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.checkBox = QCheckBox(self.row_2)
        self.checkBox.setObjectName(u"checkBox")
        self.checkBox.setAutoFillBackground(False)
        self.checkBox.setStyleSheet(u"")

        self.gridLayout_2.addWidget(self.checkBox, 0, 0, 1, 1)

        self.radioButton = QRadioButton(self.row_2)
        self.radioButton.setObjectName(u"radioButton")
        self.radioButton.setStyleSheet(u"")

        self.gridLayout_2.addWidget(self.radioButton, 0, 1, 1, 1)

        self.verticalSlider = QSlider(self.row_2)
        self.verticalSlider.setObjectName(u"verticalSlider")
        self.verticalSlider.setStyleSheet(u"")
        self.verticalSlider.setOrientation(Qt.Vertical)

        self.gridLayout_2.addWidget(self.verticalSlider, 0, 2, 3, 1)

        self.verticalScrollBar = QScrollBar(self.row_2)
        self.verticalScrollBar.setObjectName(u"verticalScrollBar")
        self.verticalScrollBar.setStyleSheet(u" QScrollBar:vertical { background: rgb(52, 59, 72); }\n"
" QScrollBar:horizontal { background: rgb(52, 59, 72); }")
        self.verticalScrollBar.setOrientation(Qt.Vertical)

        self.gridLayout_2.addWidget(self.verticalScrollBar, 0, 4, 3, 1)

        self.scrollArea = QScrollArea(self.row_2)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setStyleSheet(u" QScrollBar:vertical {\n"
"    background: rgb(52, 59, 72);\n"
" }\n"
" QScrollBar:horizontal {\n"
"    background: rgb(52, 59, 72);\n"
" }")
        self.scrollArea.setFrameShape(QFrame.NoFrame)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 218, 218))
        self.scrollAreaWidgetContents.setStyleSheet(u" QScrollBar:vertical {\n"
"	border: none;\n"
"    background: rgb(52, 59, 72);\n"
"    width: 14px;\n"
"    margin: 21px 0 21px 0;\n"
"	border-radius: 0px;\n"
" }")
        self.horizontalLayout_11 = QHBoxLayout(self.scrollAreaWidgetContents)
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.plainTextEdit = QPlainTextEdit(self.scrollAreaWidgetContents)
        self.plainTextEdit.setObjectName(u"plainTextEdit")
        self.plainTextEdit.setMinimumSize(QSize(200, 200))
        self.plainTextEdit.setStyleSheet(u"background-color: rgb(33, 37, 43);")

        self.horizontalLayout_11.addWidget(self.plainTextEdit)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayout_2.addWidget(self.scrollArea, 0, 5, 3, 1)

        self.comboBox = QComboBox(self.row_2)
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.setObjectName(u"comboBox")
        self.comboBox.setFont(font)
        self.comboBox.setAutoFillBackground(False)
        self.comboBox.setStyleSheet(u"background-color: rgb(33, 37, 43);")
        self.comboBox.setIconSize(QSize(16, 16))
        self.comboBox.setFrame(True)

        self.gridLayout_2.addWidget(self.comboBox, 1, 0, 1, 2)

        self.horizontalScrollBar = QScrollBar(self.row_2)
        self.horizontalScrollBar.setObjectName(u"horizontalScrollBar")
        sizePolicy.setHeightForWidth(self.horizontalScrollBar.sizePolicy().hasHeightForWidth())
        self.horizontalScrollBar.setSizePolicy(sizePolicy)
        self.horizontalScrollBar.setStyleSheet(u" QScrollBar:vertical { background: rgb(52, 59, 72); }\n"
" QScrollBar:horizontal { background: rgb(52, 59, 72); }")
        self.horizontalScrollBar.setOrientation(Qt.Horizontal)

        self.gridLayout_2.addWidget(self.horizontalScrollBar, 1, 3, 1, 1)

        self.commandLinkButton = QCommandLinkButton(self.row_2)
        self.commandLinkButton.setObjectName(u"commandLinkButton")
        self.commandLinkButton.setCursor(QCursor(Qt.PointingHandCursor))
        self.commandLinkButton.setStyleSheet(u"")
        self.commandLinkButton.setIcon(icon4)

        self.gridLayout_2.addWidget(self.commandLinkButton, 1, 6, 1, 1)

        self.horizontalSlider = QSlider(self.row_2)
        self.horizontalSlider.setObjectName(u"horizontalSlider")
        self.horizontalSlider.setStyleSheet(u"")
        self.horizontalSlider.setOrientation(Qt.Horizontal)

        self.gridLayout_2.addWidget(self.horizontalSlider, 2, 0, 1, 2)


        self.verticalLayout_19.addLayout(self.gridLayout_2)


        self.verticalLayout.addWidget(self.row_2)

        self.row_3 = QFrame(self.widgets)
        self.row_3.setObjectName(u"row_3")
        self.row_3.setMinimumSize(QSize(0, 150))
        self.row_3.setFrameShape(QFrame.StyledPanel)
        self.row_3.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_12 = QHBoxLayout(self.row_3)
        self.horizontalLayout_12.setSpacing(0)
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.horizontalLayout_12.setContentsMargins(0, 0, 0, 0)
        self.tableWidget = QTableWidget(self.row_3)
        if (self.tableWidget.columnCount() < 4):
            self.tableWidget.setColumnCount(4)
        __qtablewidgetitem = QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        if (self.tableWidget.rowCount() < 16):
            self.tableWidget.setRowCount(16)
        font4 = QFont()
        font4.setFamilies([u"Segoe UI"])
        __qtablewidgetitem4 = QTableWidgetItem()
        __qtablewidgetitem4.setFont(font4);
        self.tableWidget.setVerticalHeaderItem(0, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(3, __qtablewidgetitem7)
        __qtablewidgetitem8 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(4, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(5, __qtablewidgetitem9)
        __qtablewidgetitem10 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(6, __qtablewidgetitem10)
        __qtablewidgetitem11 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(7, __qtablewidgetitem11)
        __qtablewidgetitem12 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(8, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(9, __qtablewidgetitem13)
        __qtablewidgetitem14 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(10, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(11, __qtablewidgetitem15)
        __qtablewidgetitem16 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(12, __qtablewidgetitem16)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(13, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(14, __qtablewidgetitem18)
        __qtablewidgetitem19 = QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(15, __qtablewidgetitem19)
        __qtablewidgetitem20 = QTableWidgetItem()
        self.tableWidget.setItem(0, 0, __qtablewidgetitem20)
        __qtablewidgetitem21 = QTableWidgetItem()
        self.tableWidget.setItem(0, 1, __qtablewidgetitem21)
        __qtablewidgetitem22 = QTableWidgetItem()
        self.tableWidget.setItem(0, 2, __qtablewidgetitem22)
        __qtablewidgetitem23 = QTableWidgetItem()
        self.tableWidget.setItem(0, 3, __qtablewidgetitem23)
        self.tableWidget.setObjectName(u"tableWidget")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.tableWidget.sizePolicy().hasHeightForWidth())
        self.tableWidget.setSizePolicy(sizePolicy3)
        palette = QPalette()
        brush = QBrush(QColor(221, 221, 221, 255))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.WindowText, brush)
        brush1 = QBrush(QColor(0, 0, 0, 0))
        brush1.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Button, brush1)
        palette.setBrush(QPalette.Active, QPalette.Text, brush)
        palette.setBrush(QPalette.Active, QPalette.ButtonText, brush)
        brush2 = QBrush(QColor(0, 0, 0, 255))
        brush2.setStyle(Qt.NoBrush)
        palette.setBrush(QPalette.Active, QPalette.Base, brush2)
        palette.setBrush(QPalette.Active, QPalette.Window, brush1)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Active, QPalette.PlaceholderText, brush)
#endif
        palette.setBrush(QPalette.Inactive, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Button, brush1)
        palette.setBrush(QPalette.Inactive, QPalette.Text, brush)
        palette.setBrush(QPalette.Inactive, QPalette.ButtonText, brush)
        brush3 = QBrush(QColor(0, 0, 0, 255))
        brush3.setStyle(Qt.NoBrush)
        palette.setBrush(QPalette.Inactive, QPalette.Base, brush3)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush1)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Inactive, QPalette.PlaceholderText, brush)
#endif
        palette.setBrush(QPalette.Disabled, QPalette.WindowText, brush)
        palette.setBrush(QPalette.Disabled, QPalette.Button, brush1)
        palette.setBrush(QPalette.Disabled, QPalette.Text, brush)
        palette.setBrush(QPalette.Disabled, QPalette.ButtonText, brush)
        brush4 = QBrush(QColor(0, 0, 0, 255))
        brush4.setStyle(Qt.NoBrush)
        palette.setBrush(QPalette.Disabled, QPalette.Base, brush4)
        palette.setBrush(QPalette.Disabled, QPalette.Window, brush1)
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette.Disabled, QPalette.PlaceholderText, brush)
#endif
        self.tableWidget.setPalette(palette)
        self.tableWidget.setFrameShape(QFrame.NoFrame)
        self.tableWidget.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.tableWidget.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tableWidget.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableWidget.setShowGrid(True)
        self.tableWidget.setGridStyle(Qt.SolidLine)
        self.tableWidget.setSortingEnabled(False)
        self.tableWidget.horizontalHeader().setVisible(False)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(True)
        self.tableWidget.horizontalHeader().setDefaultSectionSize(200)
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.verticalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setCascadingSectionResizes(False)
        self.tableWidget.verticalHeader().setHighlightSections(False)
        self.tableWidget.verticalHeader().setStretchLastSection(True)

        self.horizontalLayout_12.addWidget(self.tableWidget)


        self.verticalLayout.addWidget(self.row_3)

        self.stackedWidget.addWidget(self.widgets)

        self.verticalLayout_15.addWidget(self.stackedWidget)


        self.horizontalLayout_4.addWidget(self.pagesContainer)

        self.extraRightBox = QFrame(self.content)
        self.extraRightBox.setObjectName(u"extraRightBox")
        self.extraRightBox.setMinimumSize(QSize(0, 0))
        self.extraRightBox.setMaximumSize(QSize(0, 16777215))
        self.extraRightBox.setFrameShape(QFrame.NoFrame)
        self.extraRightBox.setFrameShadow(QFrame.Raised)
        self.verticalLayout_7 = QVBoxLayout(self.extraRightBox)
        self.verticalLayout_7.setSpacing(0)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.themeSettingsTopDetail = QFrame(self.extraRightBox)
        self.themeSettingsTopDetail.setObjectName(u"themeSettingsTopDetail")
        self.themeSettingsTopDetail.setMaximumSize(QSize(16777215, 3))
        self.themeSettingsTopDetail.setFrameShape(QFrame.NoFrame)
        self.themeSettingsTopDetail.setFrameShadow(QFrame.Raised)

        self.verticalLayout_7.addWidget(self.themeSettingsTopDetail)

        self.contentSettings = QFrame(self.extraRightBox)
        self.contentSettings.setObjectName(u"contentSettings")
        self.contentSettings.setFrameShape(QFrame.NoFrame)
        self.contentSettings.setFrameShadow(QFrame.Raised)
        self.verticalLayout_13 = QVBoxLayout(self.contentSettings)
        self.verticalLayout_13.setSpacing(0)
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.verticalLayout_13.setContentsMargins(0, 0, 0, 0)
        self.btn_upgrade = QPushButton(self.contentSettings)
        self.btn_upgrade.setObjectName(u"btn_upgrade")
        sizePolicy.setHeightForWidth(self.btn_upgrade.sizePolicy().hasHeightForWidth())
        self.btn_upgrade.setSizePolicy(sizePolicy)
        self.btn_upgrade.setMinimumSize(QSize(0, 45))
        self.btn_upgrade.setFont(font)
        self.btn_upgrade.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_upgrade.setLayoutDirection(Qt.LeftToRight)
        self.btn_upgrade.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-level-up.png);")

        self.verticalLayout_13.addWidget(self.btn_upgrade)

        self.btn_change_theme = QPushButton(self.contentSettings)
        self.btn_change_theme.setObjectName(u"btn_change_theme")
        sizePolicy.setHeightForWidth(self.btn_change_theme.sizePolicy().hasHeightForWidth())
        self.btn_change_theme.setSizePolicy(sizePolicy)
        self.btn_change_theme.setMinimumSize(QSize(0, 45))
        self.btn_change_theme.setFont(font)
        self.btn_change_theme.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_change_theme.setLayoutDirection(Qt.LeftToRight)
        self.btn_change_theme.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-loop-circular.png);")

        self.verticalLayout_13.addWidget(self.btn_change_theme)

        self.btn_help = QPushButton(self.contentSettings)
        self.btn_help.setObjectName(u"btn_help")
        sizePolicy.setHeightForWidth(self.btn_help.sizePolicy().hasHeightForWidth())
        self.btn_help.setSizePolicy(sizePolicy)
        self.btn_help.setMinimumSize(QSize(0, 45))
        self.btn_help.setFont(font)
        self.btn_help.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_help.setLayoutDirection(Qt.LeftToRight)
        self.btn_help.setStyleSheet(u"background-image: url(:/icons/images/icons/help.png);")

        self.verticalLayout_13.addWidget(self.btn_help)

        self.btn_about = QPushButton(self.contentSettings)
        self.btn_about.setObjectName(u"btn_about")
        sizePolicy.setHeightForWidth(self.btn_about.sizePolicy().hasHeightForWidth())
        self.btn_about.setSizePolicy(sizePolicy)
        self.btn_about.setMinimumSize(QSize(0, 45))
        self.btn_about.setFont(font)
        self.btn_about.setCursor(QCursor(Qt.PointingHandCursor))
        self.btn_about.setLayoutDirection(Qt.LeftToRight)
        self.btn_about.setStyleSheet(u"background-image: url(:/icons/images/icons/cil-envelope-letter.png);")

        self.verticalLayout_13.addWidget(self.btn_about)

        self.topMenus = QFrame(self.contentSettings)
        self.topMenus.setObjectName(u"topMenus")
        self.topMenus.setFrameShape(QFrame.NoFrame)
        self.topMenus.setFrameShadow(QFrame.Raised)
        self.verticalLayout_14 = QVBoxLayout(self.topMenus)
        self.verticalLayout_14.setSpacing(0)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.verticalLayout_14.setContentsMargins(0, 0, 0, 0)

        self.verticalLayout_13.addWidget(self.topMenus, 0, Qt.AlignTop)


        self.verticalLayout_7.addWidget(self.contentSettings)


        self.horizontalLayout_4.addWidget(self.extraRightBox)


        self.verticalLayout_6.addWidget(self.content)

        self.bottomBar = QFrame(self.contentBottom)
        self.bottomBar.setObjectName(u"bottomBar")
        self.bottomBar.setMinimumSize(QSize(0, 22))
        self.bottomBar.setMaximumSize(QSize(16777215, 22))
        self.bottomBar.setFrameShape(QFrame.NoFrame)
        self.bottomBar.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_5 = QHBoxLayout(self.bottomBar)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.horizontalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.creditsLabel = QLabel(self.bottomBar)
        self.creditsLabel.setObjectName(u"creditsLabel")
        self.creditsLabel.setMaximumSize(QSize(16777215, 16))
        font5 = QFont()
        font5.setFamilies([u"Segoe UI"])
        font5.setBold(False)
        font5.setItalic(False)
        self.creditsLabel.setFont(font5)
        self.creditsLabel.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.horizontalLayout_5.addWidget(self.creditsLabel)

        self.Label_import_progress = QLabel(self.bottomBar)
        self.Label_import_progress.setObjectName(u"Label_import_progress")
        self.Label_import_progress.setMaximumSize(QSize(16777215, 16))
        self.Label_import_progress.setFont(font5)
        self.Label_import_progress.setAlignment(Qt.AlignLeading|Qt.AlignLeft|Qt.AlignVCenter)

        self.horizontalLayout_5.addWidget(self.Label_import_progress)

        self.version = QLabel(self.bottomBar)
        self.version.setObjectName(u"version")
        self.version.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout_5.addWidget(self.version)

        self.frame_size_grip = QFrame(self.bottomBar)
        self.frame_size_grip.setObjectName(u"frame_size_grip")
        self.frame_size_grip.setMinimumSize(QSize(20, 0))
        self.frame_size_grip.setMaximumSize(QSize(20, 16777215))
        self.frame_size_grip.setFrameShape(QFrame.NoFrame)
        self.frame_size_grip.setFrameShadow(QFrame.Raised)

        self.horizontalLayout_5.addWidget(self.frame_size_grip)


        self.verticalLayout_6.addWidget(self.bottomBar)


        self.verticalLayout_2.addWidget(self.contentBottom)


        self.appLayout.addWidget(self.contentBox)


        self.appMargins.addWidget(self.bgApp)

        MainWindow.setCentralWidget(self.styleSheet)

        self.retranslateUi(MainWindow)

        self.stackedWidget.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.titleLeftApp.setText(QCoreApplication.translate("MainWindow", u"Dracula", None))
        self.titleLeftDescription.setText(QCoreApplication.translate("MainWindow", u"5GRAN", None))
        self.toggleButton.setText(QCoreApplication.translate("MainWindow", u"Hide", None))
        self.btn_home.setText(QCoreApplication.translate("MainWindow", u"Home", None))
        self.btn_tput.setText(QCoreApplication.translate("MainWindow", u"Throughput", None))
        self.btn_rf.setText(QCoreApplication.translate("MainWindow", u"RF", None))
        self.btn_save.setText(QCoreApplication.translate("MainWindow", u"Save", None))
        self.btn_exit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
#if QT_CONFIG(tooltip)
        self.toggleLeftBox.setToolTip(QCoreApplication.translate("MainWindow", u"config", None))
#endif // QT_CONFIG(tooltip)
        self.toggleLeftBox.setText(QCoreApplication.translate("MainWindow", u"Config", None))
        self.extraLabel.setText(QCoreApplication.translate("MainWindow", u"Testline Config", None))
#if QT_CONFIG(tooltip)
        self.extraCloseColumnBtn.setToolTip(QCoreApplication.translate("MainWindow", u"Close left box", None))
#endif // QT_CONFIG(tooltip)
        self.extraCloseColumnBtn.setText("")
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Testline Name", None))
        self.comboBox_testline_name.setCurrentText("")
        self.comboBox_testline_name.setPlaceholderText("")
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"Choose site type", None))
        self.comboBox_site_type.setItemText(0, QCoreApplication.translate("MainWindow", u"classical", None))
        self.comboBox_site_type.setItemText(1, QCoreApplication.translate("MainWindow", u"cloud", None))

        self.comboBox_site_type.setCurrentText(QCoreApplication.translate("MainWindow", u"classical", None))
        self.comboBox_site_type.setPlaceholderText("")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Node ip", None))
        self.lineEdit_node_ip.setText("")
        self.lineEdit_node_ip.setPlaceholderText(QCoreApplication.translate("MainWindow", u"btsip", None))
        self.label_username.setText(QCoreApplication.translate("MainWindow", u"Username (toor4nsn or core)", None))
        self.lineEdit_username.setText("")
        self.lineEdit_username.setPlaceholderText(QCoreApplication.translate("MainWindow", u"username", None))
        self.label_username_6.setText(QCoreApplication.translate("MainWindow", u"Password", None))
        self.lineEdit_password.setText("")
        self.lineEdit_password.setPlaceholderText(QCoreApplication.translate("MainWindow", u"password", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"app server ip", None))
        self.lineEdit_app_ip.setText("")
        self.lineEdit_app_ip.setPlaceholderText(QCoreApplication.translate("MainWindow", u"app server ip", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"app server username", None))
        self.lineEdit_app_username.setText("")
        self.lineEdit_app_username.setPlaceholderText(QCoreApplication.translate("MainWindow", u"app server username", None))
        self.label_app_username.setText(QCoreApplication.translate("MainWindow", u"app server password", None))
        self.lineEdit_app_password.setText("")
        self.lineEdit_app_password.setPlaceholderText(QCoreApplication.translate("MainWindow", u"app server password", None))
        self.label_web_ip.setText(QCoreApplication.translate("MainWindow", u"Web ip (if cloud site. vCU or VDU)", None))
        self.lineEdit_web_ip.setText("")
        self.lineEdit_web_ip.setPlaceholderText(QCoreApplication.translate("MainWindow", u"web ip", None))
        self.label_web_user.setText(QCoreApplication.translate("MainWindow", u"Web Username (Nemuadmin)", None))
        self.lineEdit_web_username.setText("")
        self.lineEdit_web_username.setPlaceholderText(QCoreApplication.translate("MainWindow", u"web username", None))
        self.label_web_password.setText(QCoreApplication.translate("MainWindow", u"Web password", None))
        self.lineEdit_web_password.setText("")
        self.lineEdit_web_password.setPlaceholderText(QCoreApplication.translate("MainWindow", u"web password", None))
        self.label_Namespace.setText(QCoreApplication.translate("MainWindow", u"Namespace", None))
        self.lineEdit_Namespace.setText("")
        self.lineEdit_Namespace.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Name Space", None))
        self.pushButton_save.setText(QCoreApplication.translate("MainWindow", u"save", None))
        self.titleRightInfo.setText(QCoreApplication.translate("MainWindow", u"5GRAN Dracula", None))
#if QT_CONFIG(tooltip)
        self.settingsTopBtn.setToolTip(QCoreApplication.translate("MainWindow", u"Settings", None))
#endif // QT_CONFIG(tooltip)
        self.settingsTopBtn.setText("")
#if QT_CONFIG(tooltip)
        self.minimizeAppBtn.setToolTip(QCoreApplication.translate("MainWindow", u"Minimize", None))
#endif // QT_CONFIG(tooltip)
        self.minimizeAppBtn.setText("")
#if QT_CONFIG(tooltip)
        self.maximizeRestoreAppBtn.setToolTip(QCoreApplication.translate("MainWindow", u"Maximize", None))
#endif // QT_CONFIG(tooltip)
        self.maximizeRestoreAppBtn.setText("")
#if QT_CONFIG(tooltip)
        self.closeAppBtn.setToolTip(QCoreApplication.translate("MainWindow", u"Close", None))
#endif // QT_CONFIG(tooltip)
        self.closeAppBtn.setText("")
#if QT_CONFIG(tooltip)
        self.pushButton_start.setToolTip(QCoreApplication.translate("MainWindow", u"start check", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_start.setText(QCoreApplication.translate("MainWindow", u"start", None))
#if QT_CONFIG(tooltip)
        self.pushButton_stop.setToolTip(QCoreApplication.translate("MainWindow", u"stop check", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_stop.setText(QCoreApplication.translate("MainWindow", u"stop", None))
#if QT_CONFIG(tooltip)
        self.pushButton_clear.setToolTip(QCoreApplication.translate("MainWindow", u"clear screen", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_clear.setText(QCoreApplication.translate("MainWindow", u"clear", None))
#if QT_CONFIG(tooltip)
        self.pushButton_tput_log_open.setToolTip(QCoreApplication.translate("MainWindow", u"open log", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_tput_log_open.setText(QCoreApplication.translate("MainWindow", u"log", None))
        self.comboBox_check_tput_mode.setItemText(0, QCoreApplication.translate("MainWindow", u"NC mode", None))
        self.comboBox_check_tput_mode.setItemText(1, QCoreApplication.translate("MainWindow", u"TCP dump mode", None))

        self.checkBox_debug_mode.setText(QCoreApplication.translate("MainWindow", u"Debug Mode", None))
        self.checkBox_with_btslog.setText(QCoreApplication.translate("MainWindow", u"With BTS log", None))
        self.commandLinkButton_tput_log.setText(QCoreApplication.translate("MainWindow", u"log link", None))
        self.commandLinkButton_tput_log.setDescription("")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"UE IP", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"BandWidth(MHz)", None))
        self.lineEdit_bandwidth.setText(QCoreApplication.translate("MainWindow", u"1500", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"PORT", None))
        self.lineEdit_port.setText(QCoreApplication.translate("MainWindow", u"542250", None))
#if QT_CONFIG(tooltip)
        self.pushButton_start_DL.setToolTip(QCoreApplication.translate("MainWindow", u"start dl TPUT", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_start_DL.setText(QCoreApplication.translate("MainWindow", u"Start DL", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"DL peak value", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"MHz", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"UL peak value", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"MHz", None))
#if QT_CONFIG(tooltip)
        self.pushButton_curve.setToolTip(QCoreApplication.translate("MainWindow", u"drow curve", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_curve.setText(QCoreApplication.translate("MainWindow", u"Draw Curve", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"btslog", None))
#if QT_CONFIG(tooltip)
        self.pushButton_start_btslog.setToolTip(QCoreApplication.translate("MainWindow", u"start btslog", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_start_btslog.setText(QCoreApplication.translate("MainWindow", u"start", None))
#if QT_CONFIG(tooltip)
        self.pushButton_stop_btslog.setToolTip(QCoreApplication.translate("MainWindow", u"stop btslog", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_stop_btslog.setText(QCoreApplication.translate("MainWindow", u"stop", None))
        self.label_btslog_status.setText("")
        self.comboBox_btslog_mode.setItemText(0, QCoreApplication.translate("MainWindow", u"TCP dump mode", None))
        self.comboBox_btslog_mode.setItemText(1, QCoreApplication.translate("MainWindow", u"NC mode", None))

        self.commandLinkButton_open_btslog.setText(QCoreApplication.translate("MainWindow", u"log link", None))
        self.commandLinkButton_open_btslog.setDescription("")
#if QT_CONFIG(tooltip)
        self.pushButton_getpeak.setToolTip(QCoreApplication.translate("MainWindow", u"stop btslog", None))
#endif // QT_CONFIG(tooltip)
        self.pushButton_getpeak.setText(QCoreApplication.translate("MainWindow", u"Get peak", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"DL Throughput", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"UL Throughput", None))
        self.labelBoxBlenderInstalation.setText(QCoreApplication.translate("MainWindow", u"FILE BOX", None))
        self.lineEdit.setText("")
        self.lineEdit.setPlaceholderText(QCoreApplication.translate("MainWindow", u"Type here", None))
        self.pushButton.setText(QCoreApplication.translate("MainWindow", u"Open", None))
        self.labelVersion_3.setText(QCoreApplication.translate("MainWindow", u"Label description", None))
        self.checkBox.setText(QCoreApplication.translate("MainWindow", u"CheckBox", None))
        self.radioButton.setText(QCoreApplication.translate("MainWindow", u"RadioButton", None))
        self.comboBox.setItemText(0, QCoreApplication.translate("MainWindow", u"Test 1", None))
        self.comboBox.setItemText(1, QCoreApplication.translate("MainWindow", u"Test 2", None))
        self.comboBox.setItemText(2, QCoreApplication.translate("MainWindow", u"Test 3", None))

        self.commandLinkButton.setText(QCoreApplication.translate("MainWindow", u"Link Button", None))
        self.commandLinkButton.setDescription(QCoreApplication.translate("MainWindow", u"Link description", None))
        ___qtablewidgetitem = self.tableWidget.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("MainWindow", u"0", None));
        ___qtablewidgetitem1 = self.tableWidget.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("MainWindow", u"1", None));
        ___qtablewidgetitem2 = self.tableWidget.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("MainWindow", u"2", None));
        ___qtablewidgetitem3 = self.tableWidget.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("MainWindow", u"3", None));
        ___qtablewidgetitem4 = self.tableWidget.verticalHeaderItem(0)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem5 = self.tableWidget.verticalHeaderItem(1)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem6 = self.tableWidget.verticalHeaderItem(2)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem7 = self.tableWidget.verticalHeaderItem(3)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem8 = self.tableWidget.verticalHeaderItem(4)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem9 = self.tableWidget.verticalHeaderItem(5)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem10 = self.tableWidget.verticalHeaderItem(6)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem11 = self.tableWidget.verticalHeaderItem(7)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem12 = self.tableWidget.verticalHeaderItem(8)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem13 = self.tableWidget.verticalHeaderItem(9)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem14 = self.tableWidget.verticalHeaderItem(10)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem15 = self.tableWidget.verticalHeaderItem(11)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem16 = self.tableWidget.verticalHeaderItem(12)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem17 = self.tableWidget.verticalHeaderItem(13)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem18 = self.tableWidget.verticalHeaderItem(14)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("MainWindow", u"New Row", None));
        ___qtablewidgetitem19 = self.tableWidget.verticalHeaderItem(15)
        ___qtablewidgetitem19.setText(QCoreApplication.translate("MainWindow", u"New Row", None));

        __sortingEnabled = self.tableWidget.isSortingEnabled()
        self.tableWidget.setSortingEnabled(False)
        ___qtablewidgetitem20 = self.tableWidget.item(0, 0)
        ___qtablewidgetitem20.setText(QCoreApplication.translate("MainWindow", u"Test", None));
        ___qtablewidgetitem21 = self.tableWidget.item(0, 1)
        ___qtablewidgetitem21.setText(QCoreApplication.translate("MainWindow", u"Text", None));
        ___qtablewidgetitem22 = self.tableWidget.item(0, 2)
        ___qtablewidgetitem22.setText(QCoreApplication.translate("MainWindow", u"Cell", None));
        ___qtablewidgetitem23 = self.tableWidget.item(0, 3)
        ___qtablewidgetitem23.setText(QCoreApplication.translate("MainWindow", u"Line", None));
        self.tableWidget.setSortingEnabled(__sortingEnabled)

        self.btn_upgrade.setText(QCoreApplication.translate("MainWindow", u"Upgrade", None))
        self.btn_change_theme.setText(QCoreApplication.translate("MainWindow", u"Change_Theme", None))
        self.btn_help.setText(QCoreApplication.translate("MainWindow", u"Help", None))
        self.btn_about.setText(QCoreApplication.translate("MainWindow", u"About", None))
        self.creditsLabel.setText(QCoreApplication.translate("MainWindow", u"By: Jie Li", None))
        self.Label_import_progress.setText("")
        self.version.setText(QCoreApplication.translate("MainWindow", u"v1.0.3", None))
    # retranslateUi

