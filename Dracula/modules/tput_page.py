import queue
import loguru
from modules.my_thread.mod_nrdim_web import NrDimWeb
from modules.my_thread.thread_get_peak_nrdim import GetPeakNrdim
from ui.TputLog import Tputlog_windows
from modules.my_thread.thread_btslog_collect import CollectBtsLogThread
from modules.myown_funtions import MyOwnFuntions
from modules.debug import DebugUI
from modules.my_thread.thread_tput_curve import Tput_curve_Thread
from modules.public_thread import check_ip, color_text, stop_thread
from modules.my_global_dict import MY_GLOBAL_DICT
from main import MainWindow
from PySide6.QtWidgets import (
    QLineEdit,
    QFileDialog,
    QMessageBox,
    QComboBox,
    QHeaderView,
    QTableWidgetItem,
    QLabel,
    QTableView,
)
from PySide6.QtGui import QAction, QIcon, QDesktopServices
from PySide6.QtCore import QThread, Signal, QObject, QProcess, QUrl, Qt
from modules.my_thread.thread_check_tput import CheckTput, CheckTput_show
from PySide6.QtWebEngineWidgets import QWebEngineView


class TputPage(MainWindow):
    def init_setup(self):
        TputPage.lineEdit_tput_log_action(self)
        TputPage.pushButton(self)
        TputPage.comboBox(self)

    def pushButton(self):
        self.ui.pushButton_start.setCheckable(True)
        self.ui.pushButton_start.clicked.connect(
            lambda: TputPage._start_check_tput(self)
        )
        self.ui.pushButton_stop.clicked.connect(lambda: TputPage._stop_check_tput(self))
        self.ui.pushButton_clear.clicked.connect(
            lambda: TputPage._clear_textBrowser(self)
        )

        self.ui.pushButton_tput_log_open.clicked.connect(
            lambda: TputPage._slot_open_tputlog(self)
        )

        self.ui.pushButton_start_DL.clicked.connect(
            lambda: TputPage._start_DL_tput(self)
        )

        self.ui.pushButton_curve.clicked.connect(
            lambda: TputPage._slot_tput_curve(self)
        )

        self.ui.checkBox_debug_mode.clicked.connect(
            lambda: TputPage._checkBox_debug_mode(self)
        )

        self.ui.pushButton_start_btslog.setCheckable(True)
        self.ui.pushButton_start_btslog.clicked.connect(
            lambda: TputPage._start_btslog_collect(self)
        )
        self.ui.pushButton_stop_btslog.clicked.connect(
            lambda: TputPage._slot_btslog_finished(self)
        )
        self.ui.commandLinkButton_open_btslog.clicked.connect(
            lambda: QDesktopServices.openUrl(QUrl.fromLocalFile("./logs/btslog"))
        )
        self.ui.commandLinkButton_tput_log.clicked.connect(
            lambda: QDesktopServices.openUrl(QUrl.fromLocalFile("./logs/tputlog"))
        )

        self.ui.pushButton_getpeak.setCheckable(True)
        self.ui.pushButton_getpeak.clicked.connect(
            lambda: TputPage._get_peak_tput(self)
        )
        # self.ui.pushButton_getpeak.clicked.connect(
        #     lambda: TputPage._Show_NrDim_Web(self)
        # )

    def _Show_NrDim_Web(self):
        self.browser = QWebEngineView()
        self.browser.resize(1200, 600)
        self.browser.setWindowTitle("NR Dim")
        self.browser.load(
            QUrl("http://5gtables.eecloud.dynamic.nsn-net.net/?tool=SCFImport")
        )
        self.browser.show()
        # self.ShowNrDimWeb_thread = NrDimWeb(scf_file="SCF.xml")
        # self.ShowNrDimWeb_thread.start()

    def _get_peak_tput(self):
        self.ui.pushButton_getpeak.setChecked(True)
        self.ui.pushButton_getpeak.setEnabled(False)
        self.getpeaktput_thread = GetPeakNrdim(scf_file_path="SCF.xml")
        self.getpeaktput_thread.signal_create_table.connect(
            lambda x, y, z: TputPage._slot_create_table(self, x, y, z)
        )
        self.getpeaktput_thread.signal_message.connect(
            lambda x: TputPage._slot_peak_tput_show(self, x)
        )
        self.getpeaktput_thread.signal_print.connect(
            lambda x: TputPage._slot_peak_tput_print(self, x)
        )
        self.getpeaktput_thread.finished.connect(
            lambda: TputPage._slot_peak_tput_finished(self)
        )

        self.getpeaktput_thread.signal_messagebox_error.connect(
            lambda x: TputPage._slot_message_err(self, x)
        )

        self.getpeaktput_thread.start()

    def _slot_message_err(self, x):
        QMessageBox.warning(self, "warning", x)

    def _slot_peak_tput_print(self, x):
        pass

    def _slot_create_table(self, x: int, y: bool, z: str):
        if y:
            self.ui.tableWidget_peak.setColumnCount(4)
            self.ui.tableWidget_peak.setRowCount(x)
            self.ui.tableWidget_peak.setHorizontalHeaderLabels(
                ["cellid", "DLtput", "ULtput", "link"]
            )
        else:
            self.ui.tableWidget_peak.setColumnCount(1)
            self.ui.tableWidget_peak.setRowCount(1)
            self.ui.tableWidget_peak.setHorizontalHeaderLabels(["error"])

            err = QTableWidgetItem(z)
            self.ui.tableWidget_peak.setItem(0, 0, err)
            # err.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        self.ui.tableWidget_peak.verticalHeader().setVisible(False)  # 隐藏Header

        self.ui.tableWidget_peak.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch
        )  # 设置等距平铺效果，与自适应冲突，根据需求选择

        # self.ui.tableWidget_peak.resizeColumnsToContents()  # 设置列宽高按照内容自适应
        # self.ui.tableWidget_peak.resizeRowsToContents()  # 设置行宽和高按照内容自适应

        self.ui.tableWidget_peak.setEditTriggers(QTableView.NoEditTriggers)  # 禁止编辑

    def _slot_peak_tput_show(self, x: dict):
        """_summary_

        Args:
            x (dict): _description_
            {"cell_id": "loading","downlink": "loading","uplink": "loading","link": "loading","id": 0}
        """
        id = x.get("id")

        cellid = QTableWidgetItem(x.get("cell_id"))
        self.ui.tableWidget_peak.setItem(id, 0, cellid)
        cellid.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        DLtput = QTableWidgetItem(str(x.get("downlink")))
        self.ui.tableWidget_peak.setItem(id, 1, DLtput)
        DLtput.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        ULtput = QTableWidgetItem(str(x.get("uplink")))
        self.ui.tableWidget_peak.setItem(id, 2, ULtput)
        ULtput.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)

        link = x.get("link")
        if link != "loading":
            temp_link = "<a href='{}' style='color:green'>link</a>".format(
                x.get("link")
            )
            temp_qlab = QLabel()
            temp_qlab.setText(temp_link)
            temp_qlab.setObjectName("fuck")
            temp_qlab.setOpenExternalLinks(True)
            temp_qlab.setToolTip(x.get("link"))
            temp_qlab.setAlignment(Qt.AlignCenter)
            self.ui.tableWidget_peak.setCellWidget(id, 3, temp_qlab)

        self.ui.tableWidget_peak.verticalHeader().setVisible(False)  # 隐藏Header

        self.ui.tableWidget_peak.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch
        )  # 设置等距平铺效果，与自适应冲突，根据需求选择

        # self.ui.tableWidget_peak.resizeColumnsToContents()  # 设置列宽高按照内容自适应
        # self.ui.tableWidget_peak.resizeRowsToContents()  # 设置行宽和高按照内容自适应

        self.ui.tableWidget_peak.setEditTriggers(QTableView.NoEditTriggers)  # 禁止编辑

    def _slot_peak_tput_finished(self):
        self.ui.pushButton_getpeak.setChecked(False)
        self.ui.pushButton_getpeak.setEnabled(True)

    def _slot_open_tputlog(self):
        FileName, _ = QFileDialog.getOpenFileName(
            self, "choose file", "", "log Files (*);;all Files (*.txt)"
        )

        if not FileName:
            return

        self.Tputlog_window = Tputlog_windows(FileName=FileName)
        self.Tputlog_window.show()

    def _start_btslog_collect(self):
        """
        收集btslog
        """
        self.ui.pushButton_start_btslog.setChecked(True)
        self.ui.pushButton_start_btslog.setEnabled(False)
        self.ui.comboBox_check_tput_mode.setEnabled(False)
        self.ui.comboBox_btslog_mode.setEnabled(False)
        MY_GLOBAL_DICT.update(
            {"btslog_mode": self.ui.comboBox_btslog_mode.currentText()}
        )

        self.worker_btslog = CollectBtsLogThread()
        self.worker_btslog.signal_message.connect(
            lambda x: TputPage._slot_btslog_MessageBox(self, x)
        )
        self.worker_btslog.finished.connect(
            lambda: TputPage._slot_btslog_finished(self)
        )
        self.worker_btslog.signal_light_status.connect(
            lambda: TputPage._slot_signal_light_status(self)
        )

        self.worker_btslog.start()

    def _slot_signal_light_status(self):
        """_summary_
        btslog信号指示灯状态,False为正在收集log,灯为绿色
        """
        self.ui.label_btslog_status.setEnabled(False)

    def _slot_btslog_finished(self):
        """_summary_
        btslog结束后的一些操作
        """
        self.ui.label_btslog_status.setEnabled(True)
        self.ui.pushButton_start_btslog.setChecked(False)
        self.ui.pushButton_start_btslog.setEnabled(True)

        if not self.ui.pushButton_start.isChecked():
            self.ui.comboBox_check_tput_mode.setEnabled(True)
            self.ui.comboBox_btslog_mode.setEnabled(True)

        state = False
        try:
            state = self.worker_btslog.is_alive()
        except Exception as _:
            pass

        if state:
            try:
                self.worker_btslog.stop()
                stop_thread(self.worker_btslog)
            except Exception as _:
                pass

    def _slot_btslog_MessageBox(self, x):
        """_summary_
        取btslog时,出现错误,弹出消息提示框
        Args:
            x (_type_): _description_
        """
        QMessageBox.warning(self, "warning", x)

    def comboBox(self):
        """_summary_
        由于btslog和tput的NC mode和TCP dump mode功能是互斥的,做了一个互斥关系
        """
        comboBox_check_tput_mode = self.ui.comboBox_check_tput_mode
        comboBox_btslog_mode = self.ui.comboBox_btslog_mode

        comboBox_check_tput_mode.currentTextChanged.connect(
            lambda: TputPage._slot_comboBox_setup(
                comboBox_check_tput_mode, comboBox_btslog_mode
            )
        )

        comboBox_btslog_mode.currentTextChanged.connect(
            lambda: TputPage._slot_comboBox_setup(
                comboBox_btslog_mode, comboBox_check_tput_mode
            )
        )

    def _slot_comboBox_setup(comboBox1: QComboBox, comboBox2: QComboBox):
        """_summary_
        comboBox函数的槽函数
        Args:
            comboBox1 (QComboBox): _description_
            comboBox2 (QComboBox): _description_
        """
        CurrentText = comboBox1.currentText()

        if CurrentText == "NC mode":
            comboBox2.setCurrentText("TCP dump mode")
        else:
            comboBox2.setCurrentText("NC mode")

    def _checkBox_debug_mode(self):
        """_summary_
        check tput开启debug模式的功能:弹出debug mode配置界面
        """

        def accept(debug_window: DebugUI):
            debug_btslog_substring = debug_window.textEdit_substring.toPlainText()
            MY_GLOBAL_DICT.update({"debug_btslog_substring": debug_btslog_substring})
            MyOwnFuntions.update_json(
                {"debug_btslog_substring": debug_btslog_substring}
            )

        def reject():
            self.ui.checkBox_debug_mode.setChecked(False)

        if self.ui.checkBox_debug_mode.isChecked():
            debug_window = DebugUI()
            debug_window.textEdit_substring.setText(
                MY_GLOBAL_DICT.get("debug_btslog_substring")
            )
            debug_window.buttonBox.accepted.connect(lambda: accept(debug_window))
            debug_window.buttonBox.rejected.connect(lambda: reject())
            debug_window.rejected.connect(lambda: reject())
            debug_window.exec()

    def _slot_tput_curve(self):
        """_summary_
        点击按钮，绘制曲线图功能
        """
        draw_curve = Tput_curve_Thread()
        draw_curve._signal_tput_list.connect(lambda x: TputPage._tputlog_curve(self, x))
        draw_curve.start()

    def _tputlog_curve(self, x: list = None):
        """_summary_
        _slot_tput_curve 的槽函数，绘制曲线图
        Args:
            x (list, optional): _description_. Defaults to None.
        """
        plt = MY_GLOBAL_DICT.get("plt")
        pd = MY_GLOBAL_DICT.get("pd")
        if plt is None:
            QMessageBox.information(
                self, "Waiting", "matplotlib is loading. please hold on"
            )
            return
        if pd is None:
            QMessageBox.information(
                self, "Waiting", "pandas is loading. please hold on"
            )
            return
        fig = plt.figure(figsize=(10, 6.18))
        ax = fig.add_subplot(1, 1, 1)  # 通过fig添加子图，参数：行数，列数，第几个
        ax.set_title("Throughput Curve", fontsize=14, fontweight="bold")  # 添加子图的标题
        ax.set_xlabel("Time (Y/m/d H:M:S.xxx)")  # 设置x轴的名称
        ax.set_ylabel("Throughput (Mbps)")  # 设置y轴的名称

        df = pd.DataFrame(
            x, columns=["type", "Stime", "time", "cellid", "tput"]
        )  # 格式化列表

        cellidlist = list(set(df["cellid"].astype("int64")))  # 查找小区号，并去重，排序

        # 画出peak值标线
        try:
            DL_peak = float(self.ui.lineEdit_DL_peak.text().strip())
            ax.plot(
                [min(df["time"]), max(df["time"])],
                [DL_peak, DL_peak],
                ls="-",
                lw=4,
                label="DLpeakTput:{}Mbps".format(DL_peak),
            )
            ax.text(
                min(df["time"]),
                DL_peak,
                (DL_peak),
                ha="center",
                va="center",
                fontsize=10,
            )
            ax.text(
                max(df["time"]),
                DL_peak,
                (DL_peak),
                ha="center",
                va="center",
                fontsize=10,
            )
        except Exception as _:
            pass

        try:
            UL_peak = float(self.ui.lineEdit_UL_peak.text().strip())
            ax.plot(
                [min(df["time"]), max(df["time"])],
                [UL_peak, UL_peak],
                ls="-",
                lw=4,
                label="ULpeakTput:{}Mbps".format(UL_peak),
            )
            ax.text(
                min(df["time"]),
                UL_peak,
                (UL_peak),
                ha="center",
                va="center",
                fontsize=10,
            )
            ax.text(
                max(df["time"]),
                UL_peak,
                (UL_peak),
                ha="center",
                va="center",
                fontsize=10,
            )
        except Exception as _:
            pass

        # 画实际的小区throughput
        for cellid in cellidlist:
            df_dl = df[(df["type"] == "DL") & (df["cellid"] == str(cellid))]
            df_ul = df[(df["type"] == "UL") & (df["cellid"] == str(cellid))]

            # 判断是否全为0，如果全为0则不画该曲线
            if all(df_dl["tput"] == 0):
                pass
            else:
                ax.plot(
                    df_dl["time"],
                    df_dl["tput"],
                    ls="--",
                    marker=".",
                    label="DL:{}".format(str(cellid)),
                )
            if all(df_ul["tput"] == 0):
                pass
            else:
                ax.plot(
                    df_ul["time"],
                    df_ul["tput"],
                    ls="--",
                    marker=".",
                    label="UL:{}".format(str(cellid)),
                )

        ax.set_ylim(0, max(df["tput"]) + 100)
        plt.legend()  # 显示图例，即每条线对应 label 中的内容
        plt.grid(True)
        fig.autofmt_xdate()  # 自动旋转xlabel
        try:
            plt.tight_layout()
        except Exception as _:
            pass

        plt.show()

    def _start_DL_tput(self):
        """_summary_
        打下行tput,调用putty.exe
        """
        process = QProcess()
        program = "./plugin/putty.exe"

        UEip = self.ui.lineEdit_ue_ip.text()
        cbw = self.ui.lineEdit_bandwidth.text()
        port = self.ui.lineEdit_port.text()
        APP_ip = self.ui.lineEdit_app_ip.text()
        APP_username = self.ui.lineEdit_app_username.text()
        APP_passwd = self.ui.lineEdit_app_password.text()

        if all([UEip, cbw, port]):
            if check_ip(UEip):
                with open("./plugin/iperf", "w") as f:
                    f.write(
                        "iperf -c {} -u -b {}M -i 1 -l 1400 -t 36000 -p {}".format(
                            UEip, cbw, port
                        )
                    )

                arguments = [
                    "-ssh",
                    "-t",
                    "-l",
                    APP_username,
                    "-pw",
                    APP_passwd,
                    APP_ip,
                    "-m",
                    "./plugin/iperf",
                ]
                process.startDetached(program, arguments)
            else:
                QMessageBox.information(
                    self, "check", "ip format wrong\n Please check!"
                )
        else:
            QMessageBox.information(
                self, "check", "Incomplete parameters,Please check!"
            )

    def _clear_textBrowser(self, num_line=1000):
        """_summary_
        清空textBrowser_DL_TPUT和textBrowser_UL_TPUT的显示内容
        """
        self.ui.textBrowser_DL_TPUT.document().setMaximumBlockCount(num_line)
        self.ui.textBrowser_UL_TPUT.document().setMaximumBlockCount(num_line)
        self.ui.textBrowser_DL_TPUT.clear()
        self.ui.textBrowser_UL_TPUT.clear()

    def _stop_check_tput(self):
        """_summary_
        停止check tput
        """
        tool_log: loguru.logger = MY_GLOBAL_DICT.get("tool_log")
        MY_GLOBAL_DICT.update({"tput_flag": False})

        state = False
        try:
            # state = self.worker_check_tput.isRunning()
            state = self.worker_check_tput.is_alive()
        except Exception as e:
            tool_log.warning(e)

        if state:
            try:
                self.worker_check_tput.stop()
                stop_thread(self.worker_check_tput)
            except Exception as e:
                tool_log.warning(e)

            TputPage._finished_check_tput(self)

        state = False
        try:
            state = self.worker_check_tput_show.is_alive()
        except Exception as _:
            pass
        if state:
            stop_thread(self.worker_check_tput_show)

        TputPage._slot_update_textBrowser(self, color_text("Stopped", color="green"))
        TputPage._slot_update_textBrowser(self, "")

    def _start_check_tput(self):
        """_summary_
        开始运行检查tput
        """
        self.ui.textBrowser_DL_TPUT.document().setMaximumBlockCount(1000)
        self.ui.textBrowser_UL_TPUT.document().setMaximumBlockCount(1000)

        self.ui.pushButton_start.setChecked(True)
        self.ui.pushButton_start.setEnabled(False)
        self.ui.checkBox_debug_mode.setEnabled(False)
        self.ui.checkBox_with_btslog.setEnabled(False)
        self.ui.comboBox_check_tput_mode.setEnabled(False)
        self.ui.comboBox_btslog_mode.setEnabled(False)

        self.queue_tput_log = queue.Queue()

        MY_GLOBAL_DICT.update(
            {
                "check_tput_mode": self.ui.comboBox_check_tput_mode.currentText(),
                "debug_mode": self.ui.checkBox_debug_mode.isChecked(),
                "queue_tput_log": self.queue_tput_log,
                "tput_flag": True,
                "WithBtsLog": self.ui.checkBox_with_btslog.isChecked(),
            }
        )

        self.worker_check_tput = CheckTput()
        self.worker_check_tput.finished.connect(
            lambda: TputPage._finished_check_tput(self)
        )

        self.worker_check_tput.signal_message.connect(
            lambda x: TputPage._slot_update_textBrowser(self, x)
        )
        self.worker_check_tput.start()

        self.worker_check_tput_show = CheckTput_show()
        self.worker_check_tput_show.signal_textBextBrowser_DL.connect(
            lambda x: TputPage._slot_update_textBrowser_DL(self, x)
        )
        self.worker_check_tput_show.signal_textBextBrowser_UL.connect(
            lambda x: TputPage._slot_update_textBrowser_UL(self, x)
        )
        self.worker_check_tput_show.signal_message.connect(
            lambda x: TputPage._slot_update_textBrowser(self, x)
        )
        self.worker_check_tput_show.start()

    def _finished_check_tput(self):
        MY_GLOBAL_DICT.update({"tput_flag": False})
        self.ui.pushButton_start.setChecked(False)
        self.ui.pushButton_start.setEnabled(True)
        self.ui.checkBox_debug_mode.setEnabled(True)
        self.ui.checkBox_with_btslog.setEnabled(True)

        if not self.ui.pushButton_start_btslog.isChecked():
            self.ui.comboBox_check_tput_mode.setEnabled(True)
            self.ui.comboBox_btslog_mode.setEnabled(True)

    def _slot_update_textBrowser(self, x: str):
        self.ui.textBrowser_DL_TPUT.append(x)
        self.ui.textBrowser_UL_TPUT.append(x)

    def _slot_update_textBrowser_DL(self, x: str):
        self.ui.textBrowser_DL_TPUT.append(x)

    def _slot_update_textBrowser_UL(self, x: str):
        self.ui.textBrowser_UL_TPUT.append(x)

    def lineEdit_tput_log_action(self):
        # def Open_tputlog():
        #     FileNames, _ = QFileDialog.getOpenFileNames(
        #         self, "choose file", "", "log Files (*);;all Files (*.txt)"
        #     )
        #     if FileNames:
        #         self.ui.lineEdit_tput_log.setText(str(FileNames))
        #         MY_GLOBAL_DICT.update({"Tputlog_FileNames": FileNames})
        #         QCoreApplication.processEvents()

        action = QAction(
            QIcon(":/icons/images/icons/cil-folder.png"), "chose log file", self
        )
        action.triggered.connect(lambda: TputPage._onTriggered_lineEdit_tput_log(self))
        # action.triggered.connect(Open_tputlog)
        self.ui.lineEdit_tput_log.addAction(action, QLineEdit.TrailingPosition)

    def _onTriggered_lineEdit_tput_log(self):
        self.thread_lineEdit_tput_log = QThread()
        self.worker_lineEdit_tput_log = OpenLogs()
        self.worker_lineEdit_tput_log.moveToThread(self.thread_lineEdit_tput_log)
        self.worker_lineEdit_tput_log.signal_filename.connect(
            lambda x: TputPage._updateUI_lineEdit_tput_log(self, x)
        )
        self.worker_lineEdit_tput_log.signal_finish.connect(
            self.thread_lineEdit_tput_log.quit
        )
        self.thread_lineEdit_tput_log.started.connect(
            lambda: self.worker_lineEdit_tput_log.run(self)
        )
        self.thread_lineEdit_tput_log.start()

    def _updateUI_lineEdit_tput_log(self, FileNames):
        self.ui.lineEdit_tput_log.setText(str(FileNames))


class OpenLogs(QObject):
    signal_filename = Signal(list)
    signal_finish = Signal()

    def run(self, UI):
        FileNames, _ = QFileDialog.getOpenFileNames(
            UI, "choose file", "", "log Files (*);;all Files (*.txt)"
        )
        if FileNames:
            self.signal_filename.emit(FileNames)
            MY_GLOBAL_DICT.update({"Tputlog_FileNames": FileNames})

        self.signal_finish.emit()
