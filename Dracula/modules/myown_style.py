from main import MainWindow
from PySide6.QtCore import QFile, QTextStream


class MyOwnStyle(MainWindow):
    def light_style(self):
        # with open("themes\my_light.qss", "r") as f:
        #     sty = f.read()
        sty = MyOwnStyle.styleLoader(self, qss_file="themes\my_light.qss")
        self.setStyleSheet(sty)
        self.ui.tput_page.setStyleSheet(sty)
        self.ui.extraTopMenu.setStyleSheet(sty)

    def dark_style(self):
        # with open("themes\my_dark.qss", "r") as f:
        #     sty = f.read()
        sty = MyOwnStyle.styleLoader(self, qss_file="themes\my_dark.qss")
        self.setStyleSheet(sty)
        self.ui.tput_page.setStyleSheet(sty)
        self.ui.extraTopMenu.setStyleSheet(sty)

    def styleLoader(self, qss_file):
        file = QFile(qss_file)
        file.open(QFile.ReadOnly | QFile.Text)
        stream = QTextStream(file)
        style = stream.readAll()
        return style
