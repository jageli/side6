import ctypes
import inspect
from pathlib import Path
import re
import sys
import threading
import time
from PySide6.QtCore import Signal, QObject
from loguru import logger
from modules import MY_GLOBAL_DICT


class ModuleFilter:
    def __init__(self, module):
        self.module = module
        self.enabled = True

    def __call__(self, record):
        if self.enabled:
            return record["extra"]["module"] == self.module
        else:
            return False


def timer(func):
    def wrapper(*args, **kwargs):
        mes = f"{func.__name__} ..."
        print(mes)
        MY_GLOBAL_DICT["import_mode_signal"].emit(mes)

        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()

        mes = f"{func.__name__} success. took {end_time - start_time:.2f} s"
        print(mes)
        MY_GLOBAL_DICT["import_mode_signal"].emit(mes)

        return result

    return wrapper


class PublicThreadStartUp(threading.Thread, QObject):
    import_mode_signal = Signal(str)

    def __init__(self):
        threading.Thread.__init__(self)
        QObject.__init__(self)
        MY_GLOBAL_DICT.update({"import_mode_signal": self.import_mode_signal})

    def run(self):
        self.prepare_tool_log()
        self.ImportModules()

    def ImportModules(self):
        self._import_matplotlib()
        self._import_pandas()
        self._import_admin()
        self._import_vdu()
        self._import_node()
        self.import_mode_signal.emit("")

    @timer
    def _import_node(self):
        import taf.internal.node

    @timer
    def _import_vdu(self):
        from taf.gnb.oam.coam.vdu import vdu

        MY_GLOBAL_DICT.update({"vdu": vdu})

    @timer
    def _import_admin(self):
        from taf.sbts.oam.coam.admin import admin

        MY_GLOBAL_DICT.update({"admin": admin})

    @timer
    def _import_matplotlib(self):
        from matplotlib import pyplot as plt

        plt.rcParams["font.sans-serif"] = ["SimHei"]
        plt.rcParams["axes.unicode_minus"] = False
        MY_GLOBAL_DICT.update({"plt": plt})

    @timer
    def _import_pandas(self):
        import pandas as pd

        MY_GLOBAL_DICT.update({"pd": pd})

    def prepare_tool_log(self):
        tool_log_file = "tool.log"
        log_file = Path(tool_log_file)

        if log_file.exists():
            with log_file.open("r+") as f:
                f.truncate(0)

        file_handler_params = {
            # "sink": tool_log_file,
            "filter": lambda record: record["extra"]["module"] == "main",
            "enqueue": True,
        }
        logger.remove(handler_id=None)
        self.tool_log = logger.bind(module="main", configure=False)
        self.tool_log.add(sink=tool_log_file, **file_handler_params)

        # 注: pyintsaller -w 时一定要把这个代码注释掉，否则程序运行不了
        # self.tool_log.add(sink=sys.stdout, **file_handler_params)

        MY_GLOBAL_DICT.update({"tool_log": self.tool_log})

        # tool_log_handler_id = self.tool_log.add(tool_log_file,**file_handler_params)
        # logger.stop(tool_log_handler_id)
        # logger.remove(tool_log_handler_id)

        # tool_log = logger.bind(module="main", configure=False)
        # tool_log.add(sink=tool_log_file, filter=ModuleFilter(module="main"))

        # use logging
        # self.tool_log = logging.Logger("tool_log")
        # self.tool_log_handler = logging.FileHandler(tool_log_file)
        # formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        # self.tool_log_handler.setFormatter(formatter)
        # self.tool_log.addHandler(self.tool_log_handler)
        # MY_GLOBAL_DICT.update({"tool_log": self.tool_log})


class Thread_monitoring(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        file_handler_params = {
            "sink": sys.stdout,
            "filter": lambda record: record["extra"]["module"] == "monitoring",
            "enqueue": True,
        }
        self.monitoring = logger.bind(module="monitoring", configure=False)
        logger.add(**file_handler_params)

    def run(self):
        while True:
            shtd_list = threading.enumerate()
            self.monitoring.info(f"{shtd_list}\n")
            time.sleep(1)


def Thread_monitor():
    _Thread_monitoring = Thread_monitoring()
    _Thread_monitoring.start()
    MY_GLOBAL_DICT.update({"_Thread_monitoring": _Thread_monitoring})


def _async_raise(tid, exctype):
    if not inspect.isclass(exctype):
        raise TypeError("Only types can be raised (not instances)")
    res = ctypes.pythonapi.PyThreadState_SetAsyncExc(
        ctypes.c_long(tid), ctypes.py_object(exctype)
    )
    if res == 0:
        raise ValueError("invalid thread id")
    elif res != 1:
        ctypes.pythonapi.PyThreadState_SetAsyncExc(tid, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


def stop_thread(thread: threading.Thread):
    # print(f"stop {thread}")
    _async_raise(thread.ident, SystemExit)


def timeS() -> str:
    """_summary_

    Returns:
        str:e.g 2023-11-05_22-37-60
    """
    time_strf = time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())
    return time_strf


def color_text(text: str, color="red", font_weight="bold"):
    return "<span style='color:{};font-weight:{}'>{}</span>".format(
        color, font_weight, text
    )


def check_ip(str) -> bool:
    if str.strip():
        if re.match(
            r"^(25[0-5]|2[0-4][0-9]|[1][0-9]{2}|[1-9][0-9]|[1-9])(\.(25[0-5]|2[0-4][0-9]|[1][0-9]{2}|[1-9][0-9]|[0-9])){3}$",
            str,
        ):
            return True
        else:
            return False


class ValidateTestLineConfig:
    @staticmethod
    def validate_node_ip():
        node_ip = MY_GLOBAL_DICT.get("current_testline").get("node_ip")
        if not check_ip(node_ip):
            raise Exception("Node ip format incorrect!")

    @staticmethod
    def validate_node_username():
        node_username = MY_GLOBAL_DICT.get("current_testline").get("node_username")
        if not node_username:
            raise Exception("Node username is None")

    @staticmethod
    def validate_node_password():
        node_password = MY_GLOBAL_DICT.get("current_testline").get("node_password")
        if not node_password:
            raise Exception("Node password is None")

    @staticmethod
    def validate_app_ip():
        app_ip = MY_GLOBAL_DICT.get("current_testline").get("app_ip")
        if not check_ip(app_ip):
            raise Exception("APP server ip format incorrect!")

    @staticmethod
    def validate_app_username():
        app_username = MY_GLOBAL_DICT.get("current_testline").get("app_username")
        if not app_username:
            raise Exception("APP server username is None")

    @staticmethod
    def validate_app_password():
        app_password = MY_GLOBAL_DICT.get("current_testline").get("app_password")
        if not app_password:
            raise Exception("APP server password is None")

    @staticmethod
    def validate_web_ip():
        web_ip = MY_GLOBAL_DICT.get("current_testline").get("web_ip")
        if not check_ip(web_ip):
            raise Exception("Web ip format incorrect!")

    @staticmethod
    def validate_web_username():
        web_username = MY_GLOBAL_DICT.get("current_testline").get("web_username")
        if not web_username:
            raise Exception("Web username is None")

    @staticmethod
    def validate_web_password():
        web_password = MY_GLOBAL_DICT.get("current_testline").get("web_password")
        if not web_password:
            raise Exception("Web password is None")

    @staticmethod
    def validate_Namespace():
        Namespace = MY_GLOBAL_DICT.get("current_testline").get("Namespace")
        if not Namespace:
            raise Exception("Namespace is None")


def prepare_case_log_dir_child(parent_dir: Path) -> (Path, str):
    log_dir = Path(parent_dir)
    if not log_dir.exists():
        log_dir.mkdir()
    return log_dir
