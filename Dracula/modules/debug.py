from PySide6.QtWidgets import QDialog
from PySide6.QtGui import QIcon
from modules.ui_debug import Ui_Dialog


class DebugUI(QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        super(DebugUI, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(":/images/images/images/PyDracula.png"))
        self.setWindowTitle("setup")
