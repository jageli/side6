import json
import os
from pathlib import Path
from modules.my_global_dict import MY_GLOBAL_DICT
from modules.myown_style import MyOwnStyle
from modules.app_functions import AppFunctions
from modules.ui_functions import UIFunctions
from main import MainWindow
from PySide6.QtWidgets import QMenu
from PySide6.QtGui import QCursor


class MyOwnFuntions(MainWindow):
    def init_setup_config(self):
        config_file = "config/config.json"
        MY_GLOBAL_DICT.update({"config_file": config_file})
        file = Path(config_file)
        if not file.exists():
            file.parent.mkdir(parents=True, exist_ok=True)
            init_data = {
                "theme": False,
                "current_testline": {},
                "testline_list": [],
            }
            setup_conf = json.dumps(init_data, indent=4, separators=(",", ": "))
            file.write_text(setup_conf)
        MyOwnFuntions.load_json()

        MyOwnFuntions.init_testline_config(self)
        MyOwnFuntions.init_comboBox_testline_name_list(self)

    def init_testline_config(self):
        self.ui.comboBox_testline_name.setCurrentText(
            MY_GLOBAL_DICT.get("current_testline").get("testline_name")
        )
        self.ui.comboBox_site_type.setCurrentText(
            MY_GLOBAL_DICT.get("current_testline").get("site_type")
        )

        MyOwnFuntions.web_information_show(
            self, self.ui.comboBox_site_type.currentText()
        )

        self.ui.lineEdit_node_ip.setText(
            MY_GLOBAL_DICT.get("current_testline").get("node_ip")
        )
        self.ui.lineEdit_username.setText(
            MY_GLOBAL_DICT.get("current_testline").get("node_username")
        )
        self.ui.lineEdit_password.setText(
            MY_GLOBAL_DICT.get("current_testline").get("node_password")
        )

        self.ui.lineEdit_app_ip.setText(
            MY_GLOBAL_DICT.get("current_testline").get("app_ip")
        )
        self.ui.lineEdit_app_username.setText(
            MY_GLOBAL_DICT.get("current_testline").get("app_username")
        )
        self.ui.lineEdit_app_password.setText(
            MY_GLOBAL_DICT.get("current_testline").get("app_password")
        )

        self.ui.lineEdit_web_ip.setText(
            MY_GLOBAL_DICT.get("current_testline").get("web_ip")
        )
        self.ui.lineEdit_web_username.setText(
            MY_GLOBAL_DICT.get("current_testline").get("web_username")
        )
        self.ui.lineEdit_web_password.setText(
            MY_GLOBAL_DICT.get("current_testline").get("web_password")
        )

        self.ui.lineEdit_Namespace.setText(
            MY_GLOBAL_DICT.get("current_testline").get("Namespace")
        )

        MyOwnFuntions.update_creditsLabel(self)

    @staticmethod
    def load_json():
        config_file = MY_GLOBAL_DICT.get("config_file")
        with open(config_file, encoding="UTF-8") as jsonfile:
            MY_GLOBAL_DICT.update(**json.load(jsonfile))
        return MY_GLOBAL_DICT

    @staticmethod
    def update_json(update_dict: dict):
        file = MY_GLOBAL_DICT.get("config_file")
        with open(file, "r+", encoding="UTF-8") as jsonfile:
            data: dict = json.load(jsonfile)
            data.update(update_dict)
            jsonfile.seek(0)
            json.dump(data, jsonfile, indent=4, ensure_ascii=False)
            jsonfile.truncate()

    def change_theme_fun(self):
        if self.useCustomTheme:
            themeFile = os.path.abspath(
                os.path.join(self.absPath, "themes/py_dracula_dark.qss")
            )
            UIFunctions.theme(self, themeFile, True)
            AppFunctions.setThemeHack_dark(self)
            MyOwnStyle.dark_style(self)
            self.useCustomTheme = False
        else:
            themeFile = os.path.abspath(
                os.path.join(self.absPath, "themes/py_dracula_light.qss")
            )
            UIFunctions.theme(self, themeFile, True)
            AppFunctions.setThemeHack(self)
            MyOwnStyle.light_style(self)
            self.useCustomTheme = True
        update_dict = {"theme": self.useCustomTheme}
        MyOwnFuntions.update_json(update_dict=update_dict)

    def pushButton_save_passed_slot(self):
        testline_name = self.ui.comboBox_testline_name.currentText()
        if testline_name is None:
            testline_name = "default"
        site_type = self.ui.comboBox_site_type.currentText()
        node_ip = self.ui.lineEdit_node_ip.text()
        node_username = self.ui.lineEdit_username.text()
        node_password = self.ui.lineEdit_password.text()

        app_ip = self.ui.lineEdit_app_ip.text()
        app_username = self.ui.lineEdit_app_username.text()
        app_password = self.ui.lineEdit_app_password.text()

        web_ip = self.ui.lineEdit_web_ip.text()
        web_username = self.ui.lineEdit_web_username.text()
        web_password = self.ui.lineEdit_web_password.text()

        Namespace = self.ui.lineEdit_Namespace.text()

        update_current_testline = {
            "current_testline": {
                "testline_name": testline_name,
                "site_type": site_type,
                "node_ip": node_ip,
                "node_username": node_username,
                "node_password": node_password,
                "app_ip": app_ip,
                "app_username": app_username,
                "app_password": app_password,
                "web_ip": web_ip,
                "web_username": web_username,
                "web_password": web_password,
                "Namespace": Namespace,
            }
        }
        MyOwnFuntions.update_json(update_dict=update_current_testline)

        update_testline_list = {
            "testline_name": testline_name,
            "site_type": site_type,
            "node_ip": node_ip,
            "node_username": node_username,
            "node_password": node_password,
            "app_ip": app_ip,
            "app_username": app_username,
            "app_password": app_password,
            "web_ip": web_ip,
            "web_username": web_username,
            "web_password": web_password,
            "Namespace": Namespace,
        }

        testline_list: list = MyOwnFuntions.load_json().get("testline_list")
        for n, i in enumerate(testline_list):
            if update_testline_list.get("testline_name") == i.get("testline_name"):
                testline_list.pop(n)
        testline_list.append(update_testline_list)

        MyOwnFuntions.update_json({"testline_list": testline_list})
        MyOwnFuntions.init_comboBox_testline_name_list(self)

    def init_comboBox_testline_name_list(self):
        base_config = MyOwnFuntions.load_json()
        testline_list = base_config.get("testline_list")
        if testline_list:
            testline_name_list = [i.get("testline_name") for i in testline_list]
            testline_name_list.sort()
        else:
            testline_name_list = []
        self.ui.comboBox_testline_name.clear()
        self.ui.comboBox_testline_name.addItems(testline_name_list)

        current_testline: dict = base_config.get("current_testline")
        if current_testline:
            self.ui.comboBox_testline_name.setCurrentText(
                current_testline.get("testline_name")
            )

    def update_comboBox_testline_name_list(self, current_testline):
        testline_list = MyOwnFuntions.load_json().get("testline_list")
        for testline in testline_list:
            if current_testline == testline.get("testline_name"):
                MyOwnFuntions.update_json({"current_testline": testline})
                MY_GLOBAL_DICT.update({"current_testline": testline})
                MyOwnFuntions.init_testline_config(self)
        
        MyOwnFuntions.update_creditsLabel(self)

    def update_creditsLabel(self):
        testline_name = MY_GLOBAL_DICT.get("current_testline").get("testline_name")
        node_ip = MY_GLOBAL_DICT.get("current_testline").get("node_ip")
        text = f"{testline_name} {node_ip}"
        self.ui.creditsLabel.setText(text)

    def web_information_show(self, current_type):
        if current_type == "classical":
            self.ui.label_web_ip.setVisible(False)
            self.ui.lineEdit_web_ip.setVisible(False)
            self.ui.label_web_user.setVisible(False)
            self.ui.lineEdit_web_username.setVisible(False)
            self.ui.label_web_password.setVisible(False)
            self.ui.lineEdit_web_password.setVisible(False)
            self.ui.label_Namespace.setVisible(False)
            self.ui.lineEdit_Namespace.setVisible(False)

            self.ui.comboBox_check_tput_mode.setVisible(True)
            self.ui.comboBox_btslog_mode.setVisible(True)

        elif current_type == "cloud":
            self.ui.label_web_ip.setVisible(True)
            self.ui.lineEdit_web_ip.setVisible(True)
            self.ui.label_web_user.setVisible(True)
            self.ui.lineEdit_web_username.setVisible(True)
            self.ui.label_web_password.setVisible(True)
            self.ui.lineEdit_web_password.setVisible(True)
            self.ui.label_Namespace.setVisible(True)
            self.ui.lineEdit_Namespace.setVisible(True)

            self.ui.comboBox_check_tput_mode.setVisible(False)
            self.ui.comboBox_btslog_mode.setVisible(False)

    def comboBox_testline_name_right_event(self):
        def DeleteThis():
            json_dict = MyOwnFuntions.load_json()
            testline_list: list = json_dict.get("testline_list")
            current_testline_name = json_dict.get("current_testline").get(
                "testline_name"
            )
            for i, testline in enumerate(testline_list):
                if current_testline_name == testline.get("testline_name"):
                    testline_list.pop(i)

            MyOwnFuntions.update_json({"testline_list": testline_list})
            MyOwnFuntions.update_json({"current_testline": {}})

            MyOwnFuntions.load_json()
            MyOwnFuntions.init_comboBox_testline_name_list(self)
            MyOwnFuntions.init_testline_config(self)

        def DeleteAll():
            MyOwnFuntions.update_json({"testline_list": []})
            MyOwnFuntions.update_json({"current_testline": {}})
            MyOwnFuntions.load_json()
            MyOwnFuntions.init_comboBox_testline_name_list(self)
            MyOwnFuntions.init_testline_config(self)

        menu = QMenu()
        delete_this = menu.addAction("Delete this")
        delete_all = menu.addAction("Delete All")
        delete_this.triggered.connect(DeleteThis)
        delete_all.triggered.connect(DeleteAll)
        menu.exec(QCursor.pos())
