from requests import get
from threading import Thread
from PySide6.QtCore import Signal, QObject, QProcess
from PySide6.QtWidgets import QMessageBox

from modules.my_global_dict import MY_GLOBAL_DICT


def question_message(ui, x):
    reply = QMessageBox.question(
        ui,
        "update",
        x,
        QMessageBox.Yes | QMessageBox.No,
        QMessageBox.Yes,
    )
    if reply == QMessageBox.Yes:
        my_upgrade = UpgradeAPP()
        my_upgrade.start()


def information_message(ui, x):
    QMessageBox.information(ui, "message", x)


def warning_message(ui, x):
    QMessageBox.warning(ui, "warning", x)


def upgrade_app(ui):
    t = CheckVersionForUpgrade()
    t.message_information.connect(lambda x: information_message(ui, x))
    t.message_question.connect(lambda x: question_message(ui, x))
    t.message_warning.connect(lambda x: warning_message(ui, x))
    t.start()


class UpgradeAPP(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.upgrade_name = "upgrade.exe"
        self.upgrade_bat = "upgrade.bat"

    def run(self):
        self.__create_upgrade_bat()
        self.__upgrade()

    def __create_upgrade_bat(self):
        text = f'''set "title=title -- upgrade --" 
set "wget=.\\plugin\\wget.exe https://gitlabe1.ext.net.nokia.com/j7li/tools/-/raw/master/upgrade_dracula.exe -O {self.upgrade_name}"
set "upgrade={self.upgrade_name}"
set "parameters=-N Dracula -L https://gitlabe1.ext.net.nokia.com/j7li/dracula_5gran/-/raw/main/Dracula.zip -W ./plugin/wget.exe"
set "pause=pause"
start cmd.exe /c "%title% && %wget% && %upgrade% %parameters% && %pause%"'''
        with open(self.upgrade_bat, "w", encoding="utf-8") as f:
            f.write(text)

    def __upgrade(self):
        process = QProcess()
        program = self.upgrade_bat
        process.startDetached(program)


class CheckVersionForUpgrade(Thread, QObject):
    message_warning = Signal(str)
    message_question = Signal(str)
    message_information = Signal(str)

    def __init__(self):
        Thread.__init__(self)
        QObject.__init__(self)

        self.current_version = MY_GLOBAL_DICT.get("current_version")

    def run(self):
        res: dict = get_version()
        if not res:
            self.message_warning.emit(
                "Can't find version information\nplease check your network"
            )
            return

        version = res.get("version")
        if version == self.current_version:
            self.message_information.emit("The version is the latest")
            return

        self.message_question.emit(
            f"Find new version:{version}\nAre you sure to upgrade?"
        )


class CheckVersion(Thread, QObject):
    message = Signal(str)

    def __init__(self, current_version):
        Thread.__init__(self)
        QObject.__init__(self)

        self.current_version = current_version

    def run(self):
        res: dict = get_version()
        if not res:
            return
        version = res.get("version")
        if version == self.current_version:
            return
        self.message.emit(
            f"Current version:{self.current_version}. Find new version:{version}. Please upgrade"
        )


def get_version() -> dict:
    """_summary_
    curl https://gitlabe1.ext.net.nokia.com/j7li/tools/-/raw/master/DraculaVersion.json?private_token=AfdbmZHRJ-AJ5TcSh55g
    Returns:
        dict: _description_
        {'version': 'V.20231119', 'curl': 'https://gitlabe1.ext.net.nokia.com/j7li/5gran-toolkit'}
        or
        None

    """
    url = "https://gitlabe1.ext.net.nokia.com/j7li/tools/-/raw/master/DraculaVersion.json?private_token=AfdbmZHRJ-AJ5TcSh55g"
    proxies = {
        # 'https': 'http://10.144.1.10:8080',
        # 'http': 'http://10.144.1.10:8080'
    }
    response = get(url, proxies=proxies)
    if response.status_code == 200:
        try:
            _json = response.json()
            return _json
        except Exception as _:
            return None

    else:
        return None
